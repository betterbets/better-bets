#!/bin/bash
set -e
set -o errexit
set -o pipefail

# Ensure correct mapping of user/group from host machine to container
bash /permissions.sh || true
find /home/betterbets \! -user $APP_USER -execdir chown -R $APP_USER:$APP_USER {} \;
# The Celery containers may not have /models or /static - don't break on error
chown -R $APP_USER:$APP_USER /models || true
chown -R $APP_USER:$APP_USER /static || true


# Ensure there is a connection to the database
function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$POSTRES_DB", user="$POSTGRES_USER", password="$POSTGRES_PASSWORD", host="db")
except psycopg2.OperationalError as e:
    sys.exit(-1)
sys.exit(0)
END
}
until postgres_ready; do
  >&2 echo "Postgres is unavailable..."
  sleep 1
done
>&2 echo "Postgres is up - continuing..."


show_help() {
    echo """
Usage: docker run <imagename> COMMAND
Commands
dev     : Start a normal Django development server.
bash    : Start a bash shell
manage  : Start manage.py
lint    : Run pylint
test    : Run Django tests
python  : Run a python command
shell   : Start a Django Python shell.
uwsgi   : Run uwsgi server.
help    : Show this message
"""
}

# Run
case "$1" in
    dev)
        cd /home/betterbets && su - $APP_USER -p -c "python configure.py"
        cd /home/betterbets && su - $APP_USER -p -c "bash betterbets/setup.sh"
        echo "Starting development server"
        cd /home/betterbets && su - $APP_USER -p -c "python betterbets/manage.py runserver 0.0.0.0:8080"
    ;;
    bash)
        /bin/bash "${@:2}"
    ;;
    manage)
        python /home/betterbets/betterbets/manage.py "${@:2}"
    ;;
    test)
        echo Running Django tests
        cd /home/betterbets && coverage run betterbets/manage.py test betterbets
        cd /home/betterbets && coverage report -m
        echo Django tests finished
    ;;
    lint)
        echo Running Pylint
        cd /home/betterbets && pylint betterbets/betterbets betterbets/main betterbets/plugins
        echo Pylint finished
        echo Running Isort
        cd /home/betterbets && isort -rc --check-only .
        echo Isort finished
    ;;
    python)
        python "${@:2}"
    ;;
    shell)
        python /home/betterbets/betterbets/manage.py shell_plus
    ;;
    notebook)
        pip install jupyter
        cd /home/betterbets/betterbets && python manage.py shell_plus --notebook
    ;;
    uwsgi)
        cd /home/betterbets && su - $APP_USER -p -c "python configure.py"
        cd /home/betterbets && su - $APP_USER -p -c "bash betterbets/setup.sh"
        echo "Starting uwsgi server"
        su - $APP_USER -p -c "uwsgi --ini /home/betterbets/configs/uwsgi/uwsgi.ini"
    ;;
    *)
        show_help
    ;;
esac
