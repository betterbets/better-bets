import logging
import os
import pathlib
from subprocess import call, run

import boto3
import psycopg2

logging.basicConfig(format='%(message)s', level=logging.INFO)


BACKUP_FILENAME = 'latest.sql'
BACKUPS_BUCKET_NAME = 'betterbets-dashboard-backups'
POSTGRES_HOST = 'db'
POSTGRES_DB = os.environ.get('POSTGRES_DB')
POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
PG_PASS_PATH = os.path.join('/', 'home', 'betterbets', '.pgpass')

DATA_BUCKET_NAME = 'betterbets-dashboard-data'
MODELS_PATH = os.path.join('/', 'models')
CSGO_MODELS_PATH = os.path.join(MODELS_PATH, 'csgo')


def ensure_pgpass_exists(func):
    """Create pgpass file for pg_restore.

    Postgres commands cannot have password passed as an argument and
    requires a .pgpass file to be present in the home directory of the user
    running the command.
    """
    def wrapper():
        if not os.path.isfile(PG_PASS_PATH):
            with open(PG_PASS_PATH, 'w') as pass_file:
                # hostname:port:database:username:password
                pass_file.write('{}:{}:{}:{}:{}'.format(
                    POSTGRES_HOST,
                    5432,
                    POSTGRES_DB,
                    POSTGRES_USER,
                    POSTGRES_PASSWORD
                ))
            os.chmod(PG_PASS_PATH, 0o600)
        func()
    return wrapper


@ensure_pgpass_exists
def restore():
    """Restore from latest production database.

    Check to see if there are any database tables present, if not,
    restore the latest database from S3.
    """
    conn = psycopg2.connect(
        host=POSTGRES_HOST,
        dbname=POSTGRES_DB,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD
    )
    cursor = conn.cursor()
    cursor.execute("SELECT relname FROM pg_class WHERE relkind='r' AND relname !~ '^(pg_|sql_)';")
    tables = cursor.fetchall()
    if not tables:
        logging.info('Restoring Database: Started')
        s3 = boto3.client('s3')
        # 1. Write the compressed SQL file
        with open('{}.bz2'.format(BACKUP_FILENAME), 'wb') as backup_file:
            s3.download_fileobj(BACKUPS_BUCKET_NAME, '{}.bz2'.format(BACKUP_FILENAME), backup_file)
        # 2. Unzip the compressed file
        call(['bunzip2', '{}.bz2'.format(BACKUP_FILENAME)])
        # 3. Read the SQL file into psql
        with open(BACKUP_FILENAME) as backup_file:
            with open(os.devnull, 'w') as devnull:  # Suppress output of restore
                run(
                    ['psql', '-h', POSTGRES_HOST, '-U', POSTGRES_USER, POSTGRES_DB],
                    input=backup_file.read().encode(),
                    stdout=devnull,
                    stderr=devnull
                )
        call(['rm', 'latest.sql'])
        logging.info('Restoring Database: Finished')
    else:
        logging.info('Restoring Database: Database already exists')


def get_model_files():
    """Download latest model files if they don't already exist."""
    csgo_models_path = pathlib.Path(CSGO_MODELS_PATH)
    if list(csgo_models_path.glob('*')):
        logging.info('Downloading Model Files: CSGO model files already exist')
    else:
        logging.info('Downloading Model Files: Started')
        csgo_models_path.mkdir(parents=True, exist_ok=True)
        s3 = boto3.client('s3')
        s3_objects = s3.list_objects(Bucket=DATA_BUCKET_NAME, Prefix='models/csgo/')['Contents']

        for s3_object in s3_objects:
            if s3_object['Size'] > 0:
                # s3 has a flat structure meaning the top level key
                # 'models/csgo' will also be listed in s3_objects. However, it
                # has a size of zero and can be ignored.
                s3_object_key = s3_object['Key']
                file_path = os.path.join('/', s3_object_key)
                with open(file_path, 'wb') as model_file:
                    logging.info('Downloading Model Files: Downloading "{}"'.format(s3_object_key))
                    s3.download_fileobj(DATA_BUCKET_NAME, s3_object_key, model_file)
        logging.info('Downloading Model Files: Finished')


if __name__ == '__main__':
    restore()
    get_model_files()
