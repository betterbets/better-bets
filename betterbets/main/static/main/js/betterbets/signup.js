let button = document.getElementById("show-password");
let password = document.getElementById("id_password");
let eye = document.getElementById("eye");
button.onmousedown = function () {
  password.type = "text";
  eye.className = "fas fa-eye-slash";
};
button.onmouseup = function () {
  password.type = "password";
  eye.className = "fas fa-eye";
};
button.onmouseleave = function () {
  password.type = "password";
  eye.className = "fas fa-eye";
};
