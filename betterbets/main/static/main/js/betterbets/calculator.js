const risks = {
    'Low': {'fraction': 6, 'max_percent': 3},
    'Medium': {'fraction': 4, 'max_percent': 5},
    'High': {'fraction': 2, 'max_percent': 8}
};

function suggest_bets(id){
    // Suggest bet size for every game for a given game
    let risk = 'Medium';

    let team_1_percent = document.getElementById(`${id}_team_1_probability`).innerText;
    let team_2_percent = document.getElementById(`${id}_team_2_probability`).innerText;
    let team_1_probability = parseFloat(team_1_percent.replace('%', '')) / 100;
    let team_2_probability = parseFloat(team_2_percent.replace('%', '')) / 100;
    let team_1_odds = document.getElementById(`${id}_team_1_odds`).value;
    let team_2_odds = document.getElementById(`${id}_team_2_odds`).value;

    if (team_1_odds > 0 && team_2_odds > 0) {
        // Only allow positive numbers
        let calc_result = calc(
            team_1_odds, team_2_odds,
            team_1_probability, team_2_probability,
            risk
        );
        document.getElementById(`${id}_team_1_suggestion`).innerHTML = calc_result.team_1 + '%';
        document.getElementById(`${id}_team_2_suggestion`).innerHTML = calc_result.team_2 + '%';
    } else {
        // Clear any existing odds on the page if not valid
        document.getElementById(`${id}_team_1_suggestion`).innerHTML = "-";
        document.getElementById(`${id}_team_2_suggestion`).innerHTML = "-";
    }
}

function calc(t1odd, t2odd, t1prob, t2prob, risk){
    // Calculate suggested bet for team1 and team2
    let t1bet = Math.min(
        (((t1prob * t1odd - 1) / (t1odd-1)) / risks[risk]['fraction']) * 100,
        risks[risk]['max_percent']
    );
    let t2bet = Math.min(
        (((t2prob * t2odd - 1) / (t2odd-1)) / risks[risk]['fraction']) * 100,
        risks[risk]['max_percent']
    );
    return {
        'team_1': t1bet > 0.0 ? t1bet.toFixed(2) : 0,
        'team_2': t2bet > 0.0 ? t2bet.toFixed(2) : 0
    }
}
