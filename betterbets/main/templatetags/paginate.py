from django import template

register = template.Library()


@register.inclusion_tag('templatetags/pagination.html', takes_context=True)
def paginate(context):
    """Get list of pages for pagination."""
    page_obj = context.get('page_obj')
    paginator = context.get('paginator')
    num_pages = paginator.num_pages
    page_no = page_obj.number

    if num_pages <= 4:
        # Show all the pages
        pages = [x for x in range(1, num_pages + 1)]
    elif page_no < 4:
        # Show all pages from start + 1 and the last page
        pages = [x for x in range(1, page_no + 2)] + ['...', num_pages]
    elif page_no >= num_pages - 2:
        # Show first page, current page - 1 to the end
        pages = [1, '...'] + [x for x in range(page_no - 1, num_pages + 1)]
    else:
        # Show first, last and one either side of the current page
        pages = [1, '...', page_no - 1, page_no, page_no + 1, '...', num_pages]

    pagination = {
        'pages': pages,
        'page_obj': page_obj,
        'paginator': paginator,
    }

    # Handle other filters such as dates and tournaments
    query_params = {key: value for key, value in context['request'].GET.items() if key != 'page'}
    if query_params:
        # Query parameters are set, construct query params
        query_string = '&'.join(f'{key}={value}' for key, value in query_params.items())
        pagination['filters'] = f'&{query_string}'

    return pagination
