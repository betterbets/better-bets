from django import template

from ..models import BannerNotification

register = template.Library()


@register.inclusion_tag('templatetags/banner_notification.html')
def banner_notification():
    """Display notification banner if a notification is live."""
    try:
        notification = BannerNotification.objects.get(live=True)
        return {
            'live_notification': True,
            'color': notification.color,
            'text': notification.text
        }
    except BannerNotification.DoesNotExist:
        return {
            'live_notification': False
        }
