RISK_LEVELS = [
    {'display': 'Very Low', 'threshold': 0.011, 'class': 'badge-success'},
    {'display': 'Low', 'threshold': 0.051, 'class': 'badge-success'},
    {'display': 'Medium', 'threshold': 0.11, 'class': 'badge-warning'},
    {'display': 'High', 'threshold': 0.151, 'class': 'badge-danger'}
]


def calculate_risk_level(std_dev):
    for risk_level in RISK_LEVELS:
        if std_dev < risk_level['threshold']:
            return risk_level
    return {'display': 'Very High', 'class': 'badge-danger'}
