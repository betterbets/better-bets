from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from betterbets.celery import app
from betterbets.settings import SERVER_EMAIL, SITE_URL

from .models import Profile

EXPIRY_WARNING_TEMPLATE = 'registration/email/email_expiry_warning.{}'
ACCOUNT_DELETED_TEMPLATE = 'registration/email/account_deleted.{}'


@app.task
def email_verification_warning():
    """Warn users of email verification expiring soon.

    The email verification expiry link expires after 3 days. Warn users
    after two days that the link will expire tomorrow.
    """
    profiles_to_warn = Profile.objects.email_confirmation_expires_soon()
    for profile in profiles_to_warn:
        message_context = {
            'profile': profile,
            'domain': SITE_URL,
            'uid': urlsafe_base64_encode(force_bytes(profile.user.pk)).decode(),
            'token': profile.email_verification_token
        }
        message = EmailMultiAlternatives(
            subject='Sportsflare Email Verification Expiry Warning',
            from_email=SERVER_EMAIL,
            to=[profile.user.email],
            body=get_template(EXPIRY_WARNING_TEMPLATE.format('txt')).render(
                message_context
            )
        )
        message.attach_alternative(
            get_template(
                EXPIRY_WARNING_TEMPLATE.format('html')
            ).render(message_context),
            'text/html'
        )
        message.send()


@app.task
def email_verification_expired():
    """Notify users that their email verification has expired.

    The email verification expiry link expires after 3 days. The user's
    account gets deleted and they will need to sign up again to re-verify
    their email. This avoids junk emails being collected but allows any real
    users to forgot to verify to be informed of what has happened.
    """
    profiles = Profile.objects.email_confirmation_expired()
    for profile in profiles:
        message = EmailMultiAlternatives(
            subject='Sportsflare Account Removed',
            from_email=SERVER_EMAIL,
            to=[profile.user.email],
            body=get_template(ACCOUNT_DELETED_TEMPLATE.format('txt')).render(
                {
                    'profile': profile
                }
            )
        )
        message.attach_alternative(
            get_template(
                ACCOUNT_DELETED_TEMPLATE.format('html')
            ).render({'profile': profile}),
            'text/html'
        )
        message.send()
        profile.user.delete()
        profile.delete()
