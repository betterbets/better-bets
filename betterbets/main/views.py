import csv

from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.core.mail import EmailMultiAlternatives
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.loader import get_template
from django.utils import timezone
from django.utils.encoding import (
    DjangoUnicodeDecodeError, force_bytes, force_text
)
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views.generic import ListView

from betterbets.settings import SERVER_EMAIL
from plugins.csgo.models import CsgoSeries

from .forms import ProfileUpdateForm, SignUpForm
from .token import ACCOUNT_ACTIVATION_TOKEN

VERIFICATION_SUBJECT = 'Sportsflare Email Verification'
VERIFICATION_TEMPLATE = 'registration/email/email_verification.{}'


class SeriesListView(ListView):
    """List the series for a particular CsgoTournament."""
    model = CsgoSeries
    paginate_by = 10
    _errors = {'invalid_date_filter': 'Invalid date filter'}

    def generate_filters(self):
        """Filters for get_queryset

        :return: Filters to add to a list view queryset
        :rtype: list
        """
        raise NotImplementedError

    def validate_date_filter(self):
        valid_filters = [date_filter['value'] for date_filter in self.generate_filters()]
        if self.request.GET.get('date') not in valid_filters:
            raise ValidationError(self._errors['invalid_date_filter'])


@login_required(login_url='/login/')
def index(request):
    """Landing page for authenticated users."""
    series_list = CsgoSeries.objects.filter(
        start_time__gte=timezone.now()
    ).prefetch_related(
        'csgoseriesteam_set',
        'csgoseriesteam_set__team',
        'tournament',
        'teams'
    )[:3]
    return render(request, 'index.html', {'series_list': series_list})


def beta(request):
    """Outlining that this service is still in Beta."""
    return render(request, 'beta.html')


@transaction.atomic
def signup(request):
    """User sign ups.

    Upon signing up a user is created and a verification email is sent out.
    The user is not able to log in until the email verification link has
    been clicked.

    The 'username' field is set to be the users email, this is done because
    a username is never actually needed and an email can then be used to
    log the user in.
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = User.objects.create(
                username=form.cleaned_data['email'],
                email=form.cleaned_data['email'],
            )
            user.set_password(form.cleaned_data['password'])

            token = ACCOUNT_ACTIVATION_TOKEN.make_token(user)
            user.profile.email_verification_token = token
            user.profile.save()
            user.save()

            message_context = {
                'domain': get_current_site(request).domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': token
            }
            message = EmailMultiAlternatives(
                subject=VERIFICATION_SUBJECT,
                from_email=SERVER_EMAIL,
                to=[user.email],
                body=get_template(VERIFICATION_TEMPLATE.format('txt')).render(
                    message_context
                )
            )
            message.attach_alternative(
                get_template(
                    VERIFICATION_TEMPLATE.format('html')
                ).render(message_context),
                'text/html'
            )
            message.send()
            return redirect('verify_email')
    else:
        if request.user.is_authenticated:
            return redirect('index')
        form = SignUpForm()
    return render(
        request,
        'registration/signup.html',
        {'form': form}
    )


def verify_email(request):
    """Verification email sent."""
    return render(
        request,
        'registration/verify_email.html',
    )


def confirm_email(request, uidb64, token):
    """Activate user account."""
    try:
        user_id = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        user = None
    except (ValueError, DjangoUnicodeDecodeError):
        return render(
            request,
            '400.html',
            context={
                'title': 'Invalid Email Verification Link',
                'message': 'It appears that you have entered an invalid email '
                           'verification link. This is most likely because '
                           'you have entered the link with the in correct case. '
                           'Please ensure you use the exact link we have sent you.'
            }
        )

    token_is_valid = ACCOUNT_ACTIVATION_TOKEN.check_token(user, token)
    if user is not None and token_is_valid:
        user.profile.email_confirmed = True

        user.save()
        login(request, user)
        return redirect('index')
    return render(
        request,
        '400.html',
        context={
            'title': 'Invalid Verification Token',
            'message': 'You have used an invalid token. '
                       'This is most likely because your email link has'
                       'expired. Please please sign up again.'
        }
    )


@login_required
def profile(request):
    """User profile page."""
    return render(request, 'main/profile.html')


@login_required
def profile_update(request):
    """User profile page."""
    if request.method == 'POST':
        form = ProfileUpdateForm(request.POST)
        if form.is_valid():
            user = request.user
            user.first_name = form.cleaned_data['first_name'].strip().title()
            user.last_name = form.cleaned_data['last_name'].strip().title()
            user.profile.date_of_birth = form.cleaned_data['date_of_birth']
            user.save()
            user.profile.save()
            return redirect('profile')
    else:
        form = ProfileUpdateForm()
        form.initial = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'date_of_birth': request.user.profile.date_of_birth
        }
    return render(
        request,
        'main/profile_update.html',
        context={'user': request.user, 'form': form}
    )


@login_required
def download_user_csv(request):
    """User CSV export for mailing lists."""
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="user_details.csv"'
    fieldnames = ['Email', 'First Name', 'Last Name']
    writer = csv.DictWriter(response, fieldnames=fieldnames)
    writer.writeheader()
    for user in User.objects.all():
        writer.writerow({
            'Email': user.email,
            'First Name': user.first_name,
            'Last Name': user.last_name
        })
    return response
