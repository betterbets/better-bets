from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.html import format_html

from . import models


@admin.register(models.BannerNotification)
class BannerNotificationAdmin(admin.ModelAdmin):
    list_display = ('text', 'color', 'live')


@admin.register(models.Proxy)
class ProxyAdmin(admin.ModelAdmin):
    list_display = ('ip_address', 'port', 'https')


@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('email', 'email_confirmed', 'date_of_birth', 'user_link')
    list_filter = ('email_confirmed',)
    search_fields = ('user__first_name', 'user__last_name', 'user__email', 'date_of_birth')
    raw_id_fields = ('user',)

    def email(self, obj):
        return obj.user.email

    def user_link(self, obj):
        return format_html('<a href="{}">{}</a>'.format(
            reverse('admin:auth_user_change', args=(obj.user.id,)),
            obj.user.email
        ))

    email.short_description = 'Email'
    user_link.short_description = 'User'


class BetterbetsUserAdmin(UserAdmin):
    list_display = ('email', 'first_name', 'last_name', 'profile_link')

    def profile_link(self, obj):
        return format_html('<a href="{}">{}</a>'.format(
            reverse('admin:main_profile_change', args=(obj.profile.id,)),
            obj.email
        ))

    profile_link.short_description = 'Profile'


@admin.register(models.ScrapeDetails)
class ScrapeDetailsAdmin(admin.ModelAdmin):
    list_display = ('celery_task', 'created', 'sent', 'has_errors')
    list_filter = ('celery_task', 'created', 'start_time')
    readonly_fields = (
        'celery_task', 'created', 'start_time', 'end_time',
        'update_details', 'create_details', 'errors', 'sent'
    )

    def has_errors(self, obj):
        return bool(obj.errors)

    has_errors.boolean = True
    has_errors.short_description = 'Errors'


admin.site.register(models.BettingSite)

admin.site.unregister(User)
admin.site.register(User, BetterbetsUserAdmin)
