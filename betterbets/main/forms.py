from django import forms
from django.contrib.auth import authenticate, password_validation
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordResetForm, SetPasswordForm
)
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class DateInput(forms.DateInput):
    input_type = 'date'


class ProfileUpdateForm(forms.Form):
    first_name = forms.CharField(
        label='First name:',
        max_length=30,
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': 'John'
        })
    )
    last_name = forms.CharField(
        label='Last name:',
        max_length=30,
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': 'Smith'
        })
    )
    date_of_birth = forms.DateField(
        label='Date of birth:',
        required=False,
        widget=forms.TextInput(attrs={
            'type': 'date'
        })
    )


class SignUpForm(forms.Form):
    """General user sign up form."""
    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(attrs={
            'placeholder': 'Email Address',
            'type': 'email'
        })
    )
    password = forms.CharField(
        label='Password',
        max_length=30,
        required=True,
        widget=forms.PasswordInput(attrs={
            'class':  'form-control',
            'placeholder': 'Password'
        })
    )

    def clean(self):
        """Validate signup form.

         * Ensure that the email address is unique as this address will be used
           to uniquely identify the users
         * store their names as title

         :return: Cleaned data
         :rtype: dict
        """
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if User.objects.filter(email=email):
            self.add_error('email', 'Email "{}" is already in use'.format(
                email
            ))

        try:
            password_validation.validate_password(password)
        except forms.ValidationError as error:
            self.add_error('password', error)
        return self.cleaned_data


class LoginForm(AuthenticationForm):
    """Handle username as an email address."""
    username = forms.CharField(
        label='Email',
        max_length=254,
        widget=forms.TextInput(attrs={
            'placeholder': 'Email Address',
            'type': 'email'
        })
    )
    password = forms.CharField(
        max_length=30,
        required=True,
        widget=forms.PasswordInput(attrs={
            'placeholder': 'Password',
        })
    )

    error_messages = {
        'suspended_account': _('This account has been suspended.'),
        'not_verified': _('This email address has not been verified')
    }

    class Meta:
        model = User
        fields = (
            'username',
            'password'
        )

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                msg = 'Invalid credentials. Note these fields are case sensitive'
                self.add_error('username', msg)
                self.add_error('password', '')
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """Check user's email has been verified.

        :param user: User logging in that needs to be checked
        :raises: forms.ValidationError
        """
        if not user.is_active:
            self.add_error('username', self.error_messages['suspended_account'])
            self.add_error('password', '')
        if not user.profile.email_confirmed:
            self.add_error('username', self.error_messages['not_verified'])
            self.add_error('password', '')


class BetterbetsPasswordResetForm(PasswordResetForm):
    """Add bootstrap attributes to email field."""
    email = forms.EmailField(
        label=_("Email"),
        max_length=254,
        widget=forms.TextInput(attrs={
            'class': 'form-control input-md',
            'placeholder': 'Insert your email',
            'type': 'email'
        })
    )


class BetterbetsSetPasswordForm(SetPasswordForm):
    """Create new password."""
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput(attrs={
            'class': 'form-control input-md',
            'placeholder': 'Insert new password'
        }),
        strip=False,
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        widget=forms.PasswordInput(attrs={
            'class': 'form-control input-md',
            'placeholder': 'Retype password'
        }),
        strip=False,
    )
