from django.contrib.auth import views as auth_views
from django.urls import path, re_path

from . import views
from .forms import (
    BetterbetsPasswordResetForm, BetterbetsSetPasswordForm, LoginForm
)

urlpatterns = [
    # Main
    path('', views.index, name='index'),
    path('beta/', views.beta, name='beta'),
    path('profile/', views.profile, name='profile'),
    path('profile/update/', views.profile_update, name='profile_update'),

    # Login/Logout
    path('login/', auth_views.LoginView.as_view(redirect_authenticated_user=True, form_class=LoginForm), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    # Registration
    path('signup/', views.signup, name='signup'),
    path('verify-email/', views.verify_email, name='verify_email'),
    re_path(
        r'^confirm-email/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.confirm_email,
        name='confirm_email'
    ),

    # Password Reset
    path(
        'password-reset/',
        auth_views.PasswordResetView.as_view(
            form_class=BetterbetsPasswordResetForm,
            template_name='password_reset/password_reset_form.html',
            html_email_template_name='password_reset/password_reset_email.html',
            subject_template_name='password_reset/password_reset_subject.txt'
        ),
        name='password_reset'
    ),
    path(
        'password-reset/done/',
        auth_views.PasswordResetDoneView.as_view(
            template_name='password_reset/password_reset_done.html'
        ),
        name='password_reset_done'
    ),
    re_path(
        r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
            form_class=BetterbetsSetPasswordForm,
            template_name='password_reset/password_reset_confirm.html'
        ),
        name='password_reset_confirm'
    ),
    path(
        'reset/done/',
        auth_views.PasswordResetCompleteView.as_view(
            template_name='password_reset/password_reset_complete.html'
        ),
        name='password_reset_complete'
    ),

    # Admin
    path('user/download/', views.download_user_csv, name='download_user_csv'),
]
