import json
import logging

import boto3

from betterbets.settings import LAMBDA_GET_FUNCTION_NAME

logger = logging.getLogger(__name__)


def lambda_get(url):
    """Use AWS lambda to get

    :param url: Url to request
    :type url: str
    :return: Content of the request
    :rtype: str
    """
    client = boto3.client('lambda', region_name='us-west-2')
    logger.info('Requesting url: "%s"', url)
    response = client.invoke(
        FunctionName=LAMBDA_GET_FUNCTION_NAME,
        InvocationType='RequestResponse',
        Payload=json.dumps({'url': url}).encode()
    )
    payload = json.loads(response['Payload'].read())
    if payload.get('errorMessage') is None:
        logger.info('Response: "%s"', payload['statusCode'])
        return payload['body']
    else:
        logger.info('Response: "%s"', payload['errorMessage'])
