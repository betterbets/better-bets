from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

from .models import Profile


class TestEmailVerification(TestCase):
    """Email verification cleanup tests."""
    def setUp(self):
        """Create required profiles for testing."""
        now = datetime.now()
        today = datetime(
            year=now.year,
            month=now.month,
            day=now.day,
            tzinfo=timezone.get_default_timezone()
        )

        self.user_between_one_and_two_days_ago = User.objects.create(
            username='chris@example.com',
            email='chris@example.com',
            date_joined=today - timedelta(days=1)
        )
        self.user_two_days_ago = User.objects.create(
            username='max@example.com',
            email='max@example.com',
            date_joined=today - timedelta(days=2)
        )
        self.user_between_two_and_three_days_ago = User.objects.create(
            username='john@example.com',
            email='john@example.com',
            date_joined=today - timedelta(days=2, hours=12)
        )
        self.user_three_days_ago = User.objects.create(
            username='virgil@example.com',
            email='virgil@example.com',
            date_joined=today - timedelta(days=3)
        )
        self.user_between_three_and_four_days_ago = User.objects.create(
            username='bob@example.com',
            email='bob@example.com',
            date_joined=today - timedelta(days=3, hours=12)
        )
        self.user_four_days_ago = User.objects.create(
            username='kenny@example.com',
            email='kenny@example.com',
            date_joined=today - timedelta(days=4)
        )

    def test_email_confirmation_expires_soon(self):
        """Test that the correct profiles are being warned."""
        profiles = Profile.objects.email_confirmation_expires_soon().order_by(
            'user__date_joined'
        )
        self.assertEqual(profiles[0].user.email, 'virgil@example.com')
        self.assertEqual(profiles[1].user.email, 'john@example.com')
        self.assertEqual(len(profiles), 2)

    def test_email_confirmation_expired(self):
        """Test that the correct profiles are being deleted."""
        profiles = Profile.objects.email_confirmation_expired().order_by(
            'user__date_joined'
        )
        self.assertEqual(profiles[0].user.email, 'kenny@example.com')
        self.assertEqual(profiles[1].user.email, 'bob@example.com')
        self.assertEqual(len(profiles), 2)
