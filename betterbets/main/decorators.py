from django.core.mail import mail_admins
from django.template.loader import get_template
from django.utils import timezone

from .models import ScrapeDetails


def send_email_notification(model_name):
    """Decorator to send email notification for update tasks.

    - Calculates the task's elapsed time from the start/end times
    - Send an email including the details of the task that was run
    - If there were no updates or creates, the scrape details is deleted

    Note: The convention with email templates.
      - update_<model_name_lower_case>s.html

    :param model_name: Name of the model being updated
    :return: Email notification decorator
    """
    def real_decorator(func):
        def wrapper(*args, **kwargs):
            start = timezone.now()
            scrape_details_pk = func(*args, **kwargs)
            end = timezone.now()

            if scrape_details_pk is None:
                return None

            scrape_details = ScrapeDetails.objects.get(pk=scrape_details_pk)
            updated = scrape_details.update_details
            created = scrape_details.create_details
            no_updated = len(updated)
            no_created = len(created)
            elapsed_time = end - start
            context = {
                'created': created,
                'updated': updated,
                'no_created': '{} Team{} Created'.format(
                    no_created, no_created > 1 and 's' or ''
                ),
                'no_updated': '{} Team{} Updated'.format(
                    no_updated, no_updated > 1 and 's' or ''
                ),
                'start': start.strftime('%x - %X'),
                'end': end.strftime('%x - %X'),
                'elapsed_time': elapsed_time,
            }
            mail_admins(
                subject='[Celery] Update {}'.format(model_name),
                message=get_template(
                    'email/update_{}s.txt'.format(model_name.lower())
                ).render(context),
                html_message=get_template(
                    'email/update_{}s.html'.format(model_name.lower())
                ).render(context)
            )
            scrape_details.sent = True
            scrape_details.start_time = start
            scrape_details.end_time = end
            scrape_details.save()
            return scrape_details_pk
        return wrapper
    return real_decorator
