import logging

import requests
from bs4 import BeautifulSoup
from django.db import models

PROXY_URL = 'https://free-proxy-list.net/anonymous-proxy.html'


logger = logging.getLogger(__name__)


class ProxyManager(models.Manager):
    def ip_addresses(self):
        """List all the ip_addresses.

        :return: All the proxy ip addresses
        :rtype: list
        """
        return [proxy.ip_address for proxy in self.all()]

    def update(self, number=5, delete_existing=True):
        """Updates proxies and checks that they work.

        Proxies are scrapped from a free proxy website.

        The idea is to have a list of proxies that get updated each time a
        new scraping task is run.

        :return: All of the proxies created
        :rtype: list
        """
        logger.info('Updating all proxies')
        if delete_existing:
            self.all().delete()

        new_proxies = []
        proxies_request = requests.get(PROXY_URL, timeout=5)
        proxies_html = BeautifulSoup(
            proxies_request.content, 'html.parser'
        ).select('tbody tr')

        for proxy_html in proxies_html:
            if self.all().count() == number:
                break

            proxy_details_html = proxy_html.select('td')

            new_ip_address = proxy_details_html[0].text
            if new_ip_address not in self.ip_addresses():
                new_proxy = Proxy(
                    ip_address=new_ip_address,
                    port=proxy_details_html[1].text,
                    https=proxy_details_html[6].text == 'yes' and True or False,
                )
                if new_proxy.works():
                    logger.info('Proxy added: %s', new_proxy)
                    new_proxy.save()
                    new_proxies.append(new_proxy)

        logger.info('Proxies Updated: %s', new_proxies)
        return new_proxies


class Proxy(models.Model):
    """Proxies to be used when making requests.

    As well as limiting request rate, proxies are used to avoid having our
    IP banned.
    """
    https = models.BooleanField()
    ip_address = models.CharField(max_length=45)
    port = models.IntegerField()

    objects = ProxyManager()

    class Meta:
        verbose_name_plural = 'Proxies'

    @classmethod
    def add_proxy(cls):
        """Add one new proxy.

        If a proxy ever stops, this can be called to add a singular new one
        after the broken one is deleted.

        :return: A new proxy or None
        """
        try:
            proxies_request = requests.get(PROXY_URL, timeout=2)
        except requests.exceptions.ReadTimeout:
            proxies_request = requests.get(PROXY_URL, timeout=10)
        proxies_html = BeautifulSoup(
            proxies_request.content, 'html.parser'
        ).select('tbody tr')

        for proxy_html in proxies_html:
            proxy_details_html = proxy_html.select('td')

            new_ip_address = proxy_details_html[0].text
            if new_ip_address not in cls.objects.ip_addresses():
                new_proxy = cls(
                    ip_address=new_ip_address,
                    port=proxy_details_html[1].text,
                    https=proxy_details_html[6].text == 'yes' and True or False,
                )
                if new_proxy.works():
                    # Only need to create one new proxy
                    new_proxy.save()
                    return new_proxy
        return None

    def works(self, url=PROXY_URL):
        """Check the proxy can make a successful connection.

        :param url: Url to test connection on
        :return: Returns if a proxy works
        :rtype: bool
        """
        try:
            request = requests.get(
                url,
                proxies={
                    'https': 'https://{}:{}'.format(
                        self.ip_address,
                        self.port
                    )
                },
                timeout=5
            )
            if request.status_code == 200:
                return True
            return False
        except (
                requests.exceptions.ChunkedEncodingError,
                requests.exceptions.ConnectionError,
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ProxyError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.SSLError,
                requests.exceptions.Timeout
        ):
            return False

    def __str__(self):
        return '{}://{}:{}'.format(
            self.https and 'https' or 'http',
            self.ip_address,
            self.port
        )
