from django.core.exceptions import ValidationError
from django.db import models

from .base import BaseModel


class Player(BaseModel):
    """Player base class."""
    # People with only one name, will have the last name filled and the
    # first name left blank.
    first_name = models.CharField(max_length=64, blank=True, null=True)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    country = models.CharField(max_length=64, blank=True)

    class Meta:
        abstract = True

    def clean(self):
        """Default model field validation.

        Either first_name or last_name must be defined.
        """
        if self.first_name == '' and self.last_name == '':
            raise ValidationError('Either first_name or last_name need to be defined.')

    def save(self, *args, **kwargs):
        """Call model's full_clean method before saving."""
        self.full_clean()
        return super().save(*args, **kwargs)


class PlayerAlias(models.Model):
    alias = models.CharField(max_length=128)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name_plural = 'Player aliases'
