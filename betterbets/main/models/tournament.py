from django.db import models
from django.template.defaultfilters import slugify

from .base import BaseModel


class Tournament(BaseModel):
    """Tournament base class."""
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    prize_money = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        blank=True,
        null=True
    )
    prize_money_currency = models.CharField(
        max_length=3,
        blank=True,
        null=True
    )
    offline = models.BooleanField()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return '{}: {} - {}'.format(self.name, self.start_date, self.end_date)
