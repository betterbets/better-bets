from django.core.exceptions import ValidationError
from django.db import models


class BannerNotification(models.Model):
    """Configurable notification under the navbar.

    Only one message can be live at a time.
    """
    text = models.CharField(max_length=150, help_text='Message to be displayed')
    color = models.CharField(
        max_length=7,
        choices=[('#E53B3B', 'Red'), ('#7AC70C', 'Green'), ('#1CB0F6', 'Blue')],
        help_text='Color of message background'
    )
    live = models.BooleanField(help_text='Display message')

    @classmethod
    def live_message(cls):
        """Return the live message if any."""
        return cls.objects.get(live=True)

    def clean(self):
        """Only allow one live message at a time."""
        try:
            live_notification = BannerNotification.objects.get(live=True)
            if self.live and self.pk != live_notification.pk:
                raise ValidationError('There can be only one live message at a time.')
        except BannerNotification.DoesNotExist:
            pass
        return super().clean()

    def __str__(self):
        return '{}: {}'.format(
            self.text,
            '{}'.format(self.live and 'Enabled' or 'Disabled')
        )
