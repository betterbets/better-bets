from django.db import models

from .base import BaseModel


class Series(BaseModel):
    start_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name_plural = 'series'


class SeriesTeam(models.Model):
    probability = models.FloatField(
        blank=True,
        null=True
    )

    class Meta:
        abstract = True

    @property
    def probability_percentage(self):
        try:
            return round(self.probability * 100, 2)
        except TypeError:
            pass
