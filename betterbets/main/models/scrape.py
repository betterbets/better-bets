from django.contrib.postgres.fields import JSONField
from django.db import models


class ScrapeDetails(models.Model):
    """Details of a scrape task being run.

    This will contain serialised JSON of the models that were created as
    well as any fields that have been updated.
    """
    celery_task = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    create_details = JSONField(blank=True, null=True)
    update_details = JSONField(blank=True, null=True)
    errors = JSONField(blank=True, null=True)
    sent = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Scrape details'

    def __str__(self):
        return '{}: {} - {}'.format(
            self.celery_task,
            self.start_time,
            self.end_time
        )
