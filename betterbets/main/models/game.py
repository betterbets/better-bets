from django.db import models
from django.utils.functional import cached_property

from .base import BaseModel


class Game(BaseModel):
    start_time = models.DateTimeField(blank=True, null=True)
    standard_deviation = models.FloatField(
        blank=True,
        null=True
    )

    class Meta:
        abstract = True


class GamePlayerTeam(BaseModel):

    class Meta:
        abstract = True


class GameTeam(models.Model):
    probability = models.FloatField(
        blank=True,
        null=True
    )

    class Meta:
        abstract = True

    @cached_property
    def probability_percentage(self):
        """Helper method for displaying probabilities in the UI."""
        try:
            return round(self.probability * 100, 2)
        except TypeError:
            pass
