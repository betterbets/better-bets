from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone


class ProfileManager(models.Manager):
    def email_confirmation_expires_soon(self):
        """Users who's emails will expire soon.

        "Expiring soon" means that the age of the account is between 2 and
        3 days old.
        """
        now = timezone.now()
        today = datetime(
            year=now.year,
            month=now.month,
            day=now.day,
            tzinfo=timezone.get_default_timezone()
        )
        two_days_ago = today - timedelta(days=2)
        three_days_ago = today - timedelta(days=3)
        return self.filter(
            user__date_joined__lt=two_days_ago,
            user__date_joined__gte=three_days_ago,
            email_confirmed=False
        )

    def email_confirmation_expired(self):
        """After the verification link expires the account is expired.

        Any account greater than 3 days old cannot be verified and is
        'stale'.
        """
        now = timezone.now()
        today = datetime(
            year=now.year,
            month=now.month,
            day=now.day,
            tzinfo=timezone.get_default_timezone()
        )
        three_days_ago = today - timedelta(days=3)
        return self.filter(
            user__date_joined__lt=three_days_ago,
            email_confirmed=False
        )


class Profile(models.Model):
    """Additional information about users."""
    user = models.OneToOneField(
        User,
        related_name='profile',
        on_delete=models.CASCADE
    )
    date_of_birth = models.DateField(null=True, blank=True)
    email_verification_token = models.CharField(max_length=24, blank=True, null=True)
    email_confirmed = models.BooleanField(default=False)

    objects = ProfileManager()

    @property
    def has_complete_profile(self):
        if all([self.date_of_birth, self.user.first_name, self.user.last_name]):
            return True
        return False

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
