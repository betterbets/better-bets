from django.db import models


class BettingSite(models.Model):
    name = models.CharField(max_length=64, unique=True)
    url = models.URLField()

    def save(self, *args, **kwargs):
        """Enforce correct formatting."""
        self.name = self.name.title()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Odds(models.Model):
    amount = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True
    )
    save_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
