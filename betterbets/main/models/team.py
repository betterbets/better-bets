from django.db import models

from .base import BaseModel


class Team(BaseModel):
    name = models.CharField(max_length=128)
    country = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ['name']


class TeamName(models.Model):
    name = models.CharField(max_length=128)
    start_date = models.DateField()
    end_date = models.DateField(default=None, null=True)

    class Meta:
        abstract = True


class TeamRegion(models.Model):
    name = models.CharField(max_length=128)
    start_date = models.DateField()
    end_date = models.DateField(default=None, null=True)

    class Meta:
        abstract = True
