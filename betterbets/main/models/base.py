from datetime import datetime

from django.db import models


class BaseModel(models.Model):
    """Add update functionality.

    When celery tasks scrapes websites, humans need to validate any models
    that have changed. So, the notification email needs to contain any
    fields that have changed, the old value and the new value.
    """
    class Meta:
        abstract = True

    def _update_many_to_many(self, field, new_value, old_value):
        """Update many to many field.

        :param field: Field that is being updated
        :param new_value: New value for the field
        :param old_value: Previous value of the field
        """
        # If the field is a many to many, a list of objects is
        # expected
        if not isinstance(new_value, list):
            raise TypeError(
                'New values for related objects must be a "list" of objects'
            )
        old_value = old_value.all()  # Get the QuerySet
        if list(old_value) != new_value:
            getattr(self, field.name).set(new_value)
            # Use representation as this is for simply display
            return {
                'old': [repr(model) for model in old_value],
                'new': [repr(model) for model in new_value]
            }
        return {}

    def _update_relation(self, field, new_value, old_value):
        """Update relation field (ForeignKey).

        :param field: Field that is being updated
        :param new_value: New value for the field
        :param old_value: Previous value of the field
        """
        if old_value != new_value:
            try:
                setattr(self, field.name, new_value)
            except ValueError:
                raise 'Field must be of type {}'.format(field.related_model)
            # Use representation as this is for simply display
            return {
                'old': repr(old_value),
                'new': repr(new_value)
            }
        return {}

    def _update_field(self, field, new_value, old_value):
        """Update non-relation field.

        :param field: Field that is being updated
        :param new_value: New value for the field
        :param old_value: Previous value of the field
        """
        if old_value != new_value:
            setattr(self, field.name, new_value)
            if isinstance(new_value, datetime):
                new_value = new_value.isoformat()
            if isinstance(old_value, datetime):
                old_value = old_value.isoformat()
            return {'old': old_value, 'new': new_value}
        return {}

    def update(self, **kwargs):
        """Show changes on a model.

        :param kwargs: Keyword arguments of fields to be updated
        :return: The instance and the fields that changed
        :rtype: dict
        """
        updated_fields = {}
        for field in self._meta.get_fields():
            try:
                new_value = kwargs[field.name]
            except KeyError:
                continue
            old_value = getattr(self, field.name)

            if field.many_to_many:
                changes = self._update_many_to_many(field, new_value, old_value)
            elif field.is_relation:
                changes = self._update_relation(field, new_value, old_value)
            else:
                changes = self._update_field(field, new_value, old_value)
            if changes:
                updated_fields[field.name] = changes
        if updated_fields:
            self.save()
        return updated_fields
