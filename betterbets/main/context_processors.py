from betterbets.settings import GOOGLE_ANALYTICS_KEY


def google_analytics(request):
    """Add Google analytics key to context.

    :param request: Django request objects
    :return: A dictionary containing the GOOGLE_ANALYTICS_KEY
    """
    return {'google_analytics_key': GOOGLE_ANALYTICS_KEY}
