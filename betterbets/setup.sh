#!/bin/bash

python /home/betterbets/betterbets/manage.py migrate
python /home/betterbets/betterbets/manage.py collectstatic --noinput --clear
