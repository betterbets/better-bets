from django.urls import path

from . import views

urlpatterns = [
    path('series/', views.CsgoSeriesListView.as_view(), name='csgo_series_list'),
    path('series/<int:pk>', views.CsgoSeriesDetailView.as_view(), name='csgo_series_details'),
    path('performance/', views.CsgoSeriesResultListView.as_view(), name='csgo_model_performance'),
]
