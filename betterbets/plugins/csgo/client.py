import psycopg2
from pandas import read_sql, to_datetime

from betterbets.settings import DATABASES
from plugins.csgo.models import CsgoPlayer

DATABASE_CREDS = DATABASES['default']


class CsgoPandasClient:
    """Csgo database client using Pandas.

    Specifically for queries that need to return pandas data frames used in
    the predict function.
    """
    def __init__(self):
        self.conn = psycopg2.connect(
            dbname=DATABASE_CREDS['NAME'],
            user=DATABASE_CREDS['USER'],
            password=DATABASE_CREDS['PASSWORD'],
            host=DATABASE_CREDS['HOST'],
            port=DATABASE_CREDS['PORT']
        )

    def close(self):
        self.conn.close()

    def get_roster(self, game_id, team_id):
        """Get the players in each team.

        :param game_id: ID of  CsgoGame
        :param team_id: ID of a CsgoTeam
        :return: Pandas data from of query results
        """
        return read_sql(
            """
            SELECT game_id, kills, deaths, player_id, hltv_external_id, team_id, alias
            FROM csgo_csgogameplayerteam gam
            INNER JOIN csgo_csgoplayer player
            ON player_id = player.id
            where team_id = %s
            and game_id = %s
            """ % (str(team_id), str(game_id)),
            self.conn
        ).hltv_external_id.values

    def get_player_game_history(self, player_hltv_id, start_time, num_games=50):
        """Get players performance results before certain series start time.

        :param player_hltv_id: HLTV ID of a CsgoPlayer
        :param start_time: Series start time
        :param num_games: Number of historic matches
        :return: Pandas data frame of query results
        """
        # Get player id from hltv one
        player_id = CsgoPlayer.objects.get(hltv_external_id=player_hltv_id).id
        query = """
                select kills, deaths, mirage_, cobblestone_, cache_, inferno_,
                nuke_, overpass_, train_ from 
                    (select distinct round(kills/100.,2) kills,
                    round(deaths/100.,2) deaths,
                    case when map_.short_code = 'mrg' then 1 else 0 end mirage_,
                    case when map_.short_code = 'cbl' then 1 else 0 end cobblestone_,
                    case when map_.short_code = 'cch' then 1 else 0 end cache_,
                    case when map_.short_code = 'inf' then 1 else 0 end inferno_,
                    case when map_.short_code = 'nuke' then 1 else 0 end nuke_,
                    case when map_.short_code = 'ovp' then 1 else 0 end overpass_,
                    case when map_.short_code = 'trn' then 1 else 0 end train_,
                    series.start_time

                    from csgo_csgogameplayerteam gpt
                    inner join csgo_csgogameteam gt
                    on gpt.team_id = gt.team_id
                    inner join csgo_csgogame game
                    on game.id = gpt.game_id
                    inner join csgo_csgomap map_
                    on map_.id = map_id
                    inner join csgo_csgoseries series
                    on series.id = game.series_id

                    where player_id = %d
                    and series.start_time < '%s'
                    limit %d) as T
                order by start_time desc
                """ % (player_id, to_datetime(start_time), num_games)

        return read_sql(query, self.conn).fillna(0., inplace=False)

    def get_predicting_series_games(self, series_id):
        """Get the games for an upcoming series.

        Games are not duplicated, like in the get_series_games method, which
        is done in order to training the model fairly, so the model
        is invariant to the side the team is on.

        :param series_id: Id of a csgo series
        """

        query = """
            select game_id, map_id, series_id, winner_id, team_id, opponent_id,
            t1win, start_time, year, best_of, prize_money, lan

            from
            (
                select tab1.game_id, map_id, series_id, tab1.winner_id, team_id, opponent_id,
                    case
                    when tab1.winner_id = team_id
                    then 1
                    else 0
                    end as
                        t1win,
                        series.start_time,
                        extract(year from series.start_time) as year,
                        series.length as best_of,
                    case
                    when prize_money is not null
                    then prize_money
                    else 0
                    end as prize_money,
                    case when offline is false then 0 else 1 end as lan,
                    ROW_NUMBER() OVER (PARTITION BY tab1.game_id ORDER BY team_id) rn
                    from
                    (
                        select *
                        from csgo_csgogame a
                        inner join csgo_csgogameteam b
                        on a.id = b.game_id
                    ) tab1
                    inner join (
                        select game_id, team_id as opponent_id
                        from csgo_csgogame a
                        inner join csgo_csgogameteam b
                        on a.id = b.game_id
                    ) tab2
                    on tab1.game_id = tab2.game_id and team_id <> opponent_id
                    inner join csgo_csgoseries series on series.id = series_id
                    inner join csgo_csgotournament tourn on tournament_id = tourn.id
                where series_id = {}
            ) games
            where rn = 1
            """.format(series_id)

        return read_sql(query, self.conn)

    def get_training_series_games(self, series_id, winner=None, map_required=False):
        """Get a series' games when training the model.

        :param series_id: Id of a csgo series
        :param winner: Winner of the series
        :param map_required: Whether a map filter is needed
        :return: Pandas dataframe
        """
        query = """
            select tab1.game_id, map_id, series_id, tab1.winner_id, team_id, opponent_id,
            case
              when tab1.winner_id = team_id
              then 1
              else 0
              end as
                t1win,
                series.start_time,
                extract(year from series.start_time) as year,
                series.length as best_of,
            case
              when prize_money is not null
              then prize_money
              else 0
              end as prize_money,
            case when offline is false then 0 else 1 end as lan
            from
              (
                select *
                from csgo_csgogame a
                inner join csgo_csgogameteam b
                on a.id = b.game_id
              ) tab1
              inner join (
                select game_id, team_id as opponent_id
                from csgo_csgogame a
                inner join csgo_csgogameteam b
                on a.id = b.game_id
              ) tab2
              on tab1.game_id = tab2.game_id and team_id <> opponent_id
            inner join csgo_csgoseries series on series.id = series_id
            inner join csgo_csgotournament tourn on tournament_id = tourn.id
            """

        if map_required:
            query += ' where map_id is not null and series_id = {}'.format(series_id)
        else:
            query += ' where series_id = {}'.format(series_id)

        if winner:
            query += ' and tab1.winner_id is not null'

        return read_sql(query, self.conn)
