"""
Scrape hltv for upcoming, current and past games

The expectation is to gather the hltv_url_paths either via series that have
already been scraped and have the hltv_url_path parameter set, OR by calling
get_series_url_paths

Games need to be updated regularly as having live data will be useful to
display on the front end. This is broken into 3 parts
 - past
 - current
 - upcoming

Past games are displayed on hltv as 'results' while current and upcoming
games are scraped in one go.
"""

import html
import logging
from datetime import datetime, timedelta

import pytz
from bs4 import BeautifulSoup
from django.utils import timezone

from main.lambda_get import lambda_get

from .exceptions import ParsingException

BASE_URL = 'https://www.hltv.org'


logger = logging.getLogger(__name__)


def parse_series_result_url_paths(request_content):
    """Parse result series list page.

    :param request_content: Request content from resulted series list view
    :type request_content: bytes or str
    :return: Upcoming hltv series urls
    :rtype: list
    """
    game_results_html = BeautifulSoup(
        request_content,
        'html.parser'
    )

    hltv_url_paths = []
    for game_result_html in game_results_html.select('div.result-con a'):
        url_path = game_result_html.get('href')
        if url_path is not None:
            # HLTV occasionally has a top section of 'featured results' which
            # are from an ongoing tournament, usually a Major. Sometimes
            # these elements do not have an href associated with them, and
            # they are not even from the filtered date range and therefore
            # should be ignored
            hltv_url_paths.append(url_path)

    return hltv_url_paths


def get_series_result_url_paths(start_date=None, end_date=None):
    """Scrape URL paths for finished games (i.e. games with results).

    :param start_date: Filter if 'include_results' is selected ('%Y-%m-%d')
        Celery tasks can not have DateTime objects passed as they are not
        serializable
    :type start_date: str
    :param end_date: Filter if 'include_results' is selected ('%Y-%m-%d')
        Celery tasks can not have DateTime objects passed as they are not
        serializable
    :type end_date: str
    :return: List of hltv URLs
    :rtype: list
    """
    if end_date is None:
        end_date = timezone.now().strftime('%Y-%m-%d')
    if start_date is None:
        start_date = timezone.now() - timedelta(days=30)
        start_date = start_date.strftime('%Y-%m-%d')
    logger.info('Scraper: Series include results (%s-%s)', start_date, end_date)

    date_filter = 'startDate={}&endDate={}'.format(start_date, end_date)
    game_results_url = '{base}/results?{date_filter}'.format(
        base=BASE_URL,
        date_filter=date_filter
    )

    hltv_url_paths = []
    last_page = False
    offset = 0
    # Pages show 100 results at a time, given in the 'offset' query parameter
    while not last_page:
        game_results_request = lambda_get(game_results_url + '&offset={}'.format(offset))
        if game_results_request is None:
            continue
        next_url_paths = parse_series_result_url_paths(game_results_request)
        hltv_url_paths += next_url_paths
        if len(next_url_paths) < 100:
            last_page = True
        offset += 100

    return hltv_url_paths


def parse_upcoming_series_url_paths(request_content):
    """Parse upcoming series list page.

    :param request_content: Upcoming series list view request content
    :type request_content: bytes or str
    :return: Upcoming hltv series urls
    :rtype: list
    """
    hltv_urls = []
    matches_html = BeautifulSoup(request_content, 'html.parser').select(
        'div.match-day a.upcoming-match'
    )
    for match_html in matches_html:
        hltv_urls.append(match_html.get('href'))
    return hltv_urls


def get_series_url_paths(start_date=None, end_date=None, include_results=False):
    """Scrape the URL paths for game details pages.

    Archived games can be filtered with a start and end date. This is
    useful when wanting to only update a small subset of historic games.

    When a tournament is taking place, including the archived games will not
    be necessary as only the current games are of interest to keep the UI up
    to date.

    :param start_date: Filter if 'include_results' is selected ('%Y-%m-%d')
        Celery tasks can not have DateTime objects passed as they are not
        serializable
    :type start_date: str
    :param end_date: Filter if 'include_results' is selected ('%Y-%m-%d')
        Celery tasks can not have DateTime objects passed as they are not
        serializable
    :type end_date: str
    :param include_results: Include past games
    :type include_results: bool
    :return: List of hltv URLs
    :rtype: list
    """
    logger.info('Scraper: Getting series urls')
    hltv_url_paths = []

    if include_results:
        hltv_url_paths += get_series_result_url_paths(start_date=start_date, end_date=end_date)

    upcoming_matches_request = lambda_get('{}/matches'.format(BASE_URL))
    if upcoming_matches_request is not None:
        hltv_url_paths += parse_upcoming_series_url_paths(upcoming_matches_request)
    return hltv_url_paths


def parse_series_teams(series_details_html=None):
    """Hltv series team parser.

    :param series_details_html: Parsed HTML of the series HTML page
    :type series_details_html: bs4.BeautifulSoup
    :return: Team details for a series
    :rtype: dict
    """
    teams_details = {
        'teams': [],
    }
    # If the teams have not yet been decided (i.e. tournament final),
    # there is no div.team instead it is div.noteam
    teams_html = series_details_html.select('div.teamsBox div.team')
    for team_html in teams_html:
        try:
            team_name = html.unescape(team_html.find('a').text.strip())
        except AttributeError:
            # If the team is not yet decided, it will not have an <a> tag
            continue
        hltv_url_path = html.unescape(team_html.find('a').get('href'))
        team_details = {
            'name': team_name,
            'hltv_url_path': hltv_url_path,
            'hltv_external_id': int(hltv_url_path.split('/')[2])
        }
        teams_details['teams'].append(team_details)

        # The winning team's score will have the class 'won' to indicate
        # they are the winner, making their score green. The losing team
        # will have 'lost' as their score.
        # TODO: Figure out the case for a 'draw' if they can happen
        if bool(team_html.find_next('div').find('div', {'class': 'won'})):
            teams_details['winning_team_details'] = team_details

    return teams_details


def parse_series_start_time(series_details_html):
    """Extract the start time of a series.

    Series that are starting soon show a countdown instead of a date.
    :param series_details_html: Series details HTML content
    :type series_details_html: bs4.BeautifulSoup
    :return timestamp
    """
    try:
        timestamp = int(series_details_html.find(
            'div', {'class': 'countdown'}
        )['data-unix'][:-3])  # Ignore miliseconds
    except KeyError:
        timestamp = int(series_details_html.select(
            'div.timeAndEvent div.date'
        )[0]['data-unix'][:-3])  # Ignore miliseconds

    return timestamp


def parse_series_game(map_html, order, series_details):
    """Parse game for a particular map.

    :param map_html: HTML for a given map
    :param order: Order of the game (game 1, game 2 etc)
    :param series_details: Details of the overall series for team lookup
    :return: Details of a particular game in a series
    :rtype: dict
    """
    game_details = {
        'order': order,
        'players': []
    }

    # Games where the map has not been decided show the "map" as "TBA"
    map_name = map_html.find('div', {'class': 'mapname'}).text
    if map_name == 'TBA':
        map_name = None
    game_details['map_name'] = map_name

    # Winning team is not clearly specified for each game. The
    # easiest way I can work out, given no draws can happen on a
    # particular game, is to look at the value of the first score's
    # class. If it is won, then the first team to display in the
    # 'teamsBox' will be the winner.
    map_results_html = map_html.find(
        'div', {'class': 'results'}
    )
    if map_results_html is not None:
        # If a map is not played, the results div will be None. This
        # will be the case for up coming games where the map is
        # 'TBA' and series that were won before all games were
        # played (i.e. one team wins 2-0 in a best of 3)
        first_team_win = True if map_results_html.find_next('span')['class'][0] == 'won' else False

        if first_team_win:
            game_details['winning_team_details'] = series_details['teams'][0]
        else:
            game_details['winning_team_details'] = series_details['teams'][1]
    return game_details


def parse_result_game_player(player_details_html, player_count, series_details):
    """Parse game players for a result game.

    Games that have been played (i.e. a game result) will have a table
    showing the players, and their kills, deaths and team.

    :param player_details_html: Player details for a particular game
    :param player_count: the Nth player as players are grouped by teams and
        hence their order matters
    :param series_details: Details of the overall series for team lookup
    :return: Details of a player for a particular resulted game
    :rtype: dict
    """
    player_details = {}
    # Players 1 - 5 correspond to the first team, and 6-10 to the
    # second team
    if player_count < 5:
        player_details['team_details'] = series_details['teams'][0]
    else:
        player_details['team_details'] = series_details['teams'][1]
    # Get player's name and alias. Player names and aliases are in the
    # following form: Kenny 'kennyS ' Schrub
    player_names = html.unescape(player_details_html.find(
        'td', {'class', 'players'}
    ).find('div', {'class': 'gtSmartphone-only'}).text).split('\'')
    try:
        player_details['first_name'] = player_names[0].rstrip()
        player_details['alias'] = player_names[1].strip().strip('\'')
        player_details['last_name'] = player_names[2].lstrip()
    except IndexError:
        player_details['alias'] = ', '.join(
            [name.strip() for name in player_names]
        )

    # Get player's kills and deaths
    kills, deaths = player_details_html.find(
        'td', {'class', 'kd text-center'}
    ).text.split('-')
    player_details['kills'] = int(kills)
    player_details['deaths'] = int(deaths)

    # Get player player url path and hltv external id
    hltv_url_path = player_details_html.find('a').get('href')
    # Convert hltv_url_path to stats_url_path:
    #     /player/9255/draken -> /stats/players/9255/draken
    player_details['hltv_url_path'] = '/stats/players/{}'.format(
        # Only need the hltv ID and player name
        '/'.join(hltv_url_path.split('/')[2:])
    )
    player_details['hltv_external_id'] = int(hltv_url_path.split('/')[2])

    return player_details


def parse_upcoming_game_player(player_details_html, team_num, series_details):
    """Parse game players for an upcoming game.

    Upcoming games will get their player details from the 'line up' section,
    which displays all the players expected to play in the game.

    :param player_details_html: Player details for a particular game
    :param team_num: The number of the team, corresponding to the order they
        show up in the HTML and hence in the series_details['teams']
    :param series_details: Details of the overall series for team lookup
    :return: Details of a player for a particular resulted game
    :rtype: dict
    """
    player_details = {
        'team_details': series_details['teams'][team_num]
    }
    player_names = player_details_html.find('img').get('title').split('\'')
    try:
        player_details['first_name'] = html.unescape(player_names[0].strip())
        player_details['alias'] = player_names[1].strip().strip('\'')
        player_details['last_name'] = html.unescape(player_names[2].strip())
    except IndexError:
        player_details['alias'] = ', '.join(
            [name.strip() for name in player_names]
        )

    # Get player hltv_url_path and hltv_external_id
    hltv_url_path = player_details_html.find('a').get('href')
    # Convert player url path to stats url path:
    #     /player/9255/draken -> /stats/players/9255/draken
    try:
        player_details['hltv_url_path'] = '/stats/players/{}'.format(
            # Only need the hltv ID and player name
            '/'.join(hltv_url_path.split('/')[2:])
        )
    except AttributeError:
        # Cannot find player player stats url on "defaulted" games
        raise ParsingException

    player_details['hltv_external_id'] = int(hltv_url_path.split('/')[2])

    return player_details


def parse_series(request_content):
    """Hltv series parser.

    Tournaments have series played between two teams. A series can have one
    or many games. A series with only one game is a best of one, three games
    is a best of three etc.

     Gets the following details
     - Hltv URL
     - Team names
     - Winning team nam
     - Tournament name
     - Start time
     - Games
          - Order
          - Teams (same as series teams)
          - Winner
          - Map
          - Players

    :param request_content: Series details page request content
    :type request_content: bytes or str
    :return: A series and details of its games
    :rtype: dict
    """
    series_details = {
        'is_valid': True,
        'teams': [],
    }

    series_details_html = BeautifulSoup(
        request_content,
        'html.parser'
    )

    series_details.update(parse_series_teams(series_details_html=series_details_html))

    series_details['tournament_name'] = html.unescape(series_details_html.select(
        'div.teamsBox div.timeAndEvent div.event a'
    )[0].text)

    series_details['start_time'] = datetime.fromtimestamp(
        parse_series_start_time(series_details_html=series_details_html),
        tz=pytz.utc
    )

    # Get game map details
    maps_html = series_details_html.select('div.flexbox-column div.mapholder')
    # The length of the series is determined by the number of maps
    # (games) played
    series_details['length'] = len(maps_html)
    games = []
    order = 0
    for map_html in maps_html:
        games.append(parse_series_game(
            map_html=map_html,
            order=order,
            series_details=series_details
        ))
        order += 1
    series_details['games'] = games

    # Get game player details per game
    maps_details_html = series_details_html.select(
        'div.spoiler div.matchstats div.stats-content'
    )
    if maps_details_html:
        # Map details show the players kills/deaths for each of the maps
        # that is played. This is only shown when a game has already been
        # played.
        for map_num, map_details_html in enumerate(maps_details_html[1:]):
            # First map details html is the 'totals'
            corresponding_game = games[map_num]
            players_details_html = map_details_html.findAll('tr', {'class': ''})
            players = []
            for player_count, player_details_html in enumerate(players_details_html):
                players.append(
                    parse_result_game_player(
                        player_count=player_count,
                        player_details_html=player_details_html,
                        series_details=series_details,
                    )
                )
            corresponding_game['players'] = players
    else:
        # For games that have not yet been played, the player details for a
        # game are selected from the 'line up' section of the page, which
        # shows all the upcoming players
        players_lineup_html = series_details_html.select('div.lineup')
        players = []
        for team_num, player_lineup_html in enumerate(players_lineup_html):
            # The first 5 elements include the player details needed
            players_details_html = player_lineup_html.findAll(
                'td', {'class': 'player'}
            )[:5]
            for player_details_html in players_details_html:
                players.append(
                    parse_upcoming_game_player(
                        player_details_html=player_details_html,
                        team_num=team_num,
                        series_details=series_details
                    )
                )
        for game in series_details['games']:
            game['players'] = players

    return series_details


def scrape_series(hltv_url_paths=None):
    """Scrape series and all its games details.

    :param hltv_url_paths: List of hltv urls to scrape
    :type hltv_url_paths: list or None
    :return: Series details dictionaries and all its games
    :rtype: list
    """
    logger.info('Scraper: Series scrape started')
    if hltv_url_paths is None:
        hltv_url_paths = get_series_url_paths()

    all_series_details = []

    for hltv_url_path in hltv_url_paths:
        logger.info('Scraper: Getting series %s', hltv_url_path)
        request_content = lambda_get('{}{}'.format(
            BASE_URL,
            hltv_url_path
        ))
        if request_content is not None:
            try:
                series_details = parse_series(request_content)
                series_details['hltv_url_path'] = hltv_url_path
                series_details['hltv_external_id'] = int(hltv_url_path.split('/')[2])
                all_series_details.append(series_details)
            except ParsingException:
                # If series is unable to parse, skip it
                logger.warning('Parsing Exception: %s', hltv_url_path)
                continue

    return all_series_details
