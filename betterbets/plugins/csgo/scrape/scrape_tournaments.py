import html
import logging
from datetime import datetime, timedelta
from decimal import Decimal, InvalidOperation

import pytz
from bs4 import BeautifulSoup
from django.utils import timezone

from main.lambda_get import lambda_get

BASE_URL = 'https://www.hltv.org'


logger = logging.getLogger(__name__)


def parse_archived_tournament_list(request_content):
    """Parse hltv tournament list view to get tournament detail url paths.

    :param request_content: Tournament list view request content
    :type request_content: bytes or str
    :return: Archived hltv tournament urls
    :rtype: list
    """
    hltv_url_paths = []

    archived_tournaments_list_html = BeautifulSoup(
        request_content,
        'html.parser'
    )
    archived_tournaments_html = archived_tournaments_list_html.select(
        'div.events-page div.events-month a.a-reset'
    )

    for archived_tournament_html in archived_tournaments_html:
        hltv_url_paths.append(archived_tournament_html.get('href'))

    return hltv_url_paths


def get_archived_tournament_url_paths(start_date=None, end_date=None):
    """Get tournament url paths of games that have been played.

    :param start_date: Start date of url filter
    :type start_date: str
    :param end_date: End date of url filter
    :type end_date: str
    :return: Archived hltv tournament urls
    :rtype: list
    """
    if end_date is None:
        end_date = timezone.now().strftime('%Y-%m-%d')
    if start_date is None:
        start_date = timezone.now() - timedelta(days=30)
        start_date = start_date.strftime('%Y-%m-%d')
    logger.info('Scraper: Tournament include archive (%s-%s)', start_date, end_date)

    date_filter = 'startDate={}&endDate={}'.format(
        start_date,
        end_date
    )
    archived_tournaments_url = '{base}/events/archive?{date_filter}'.format(
        base=BASE_URL,
        date_filter=date_filter
    )

    hltv_url_paths = []

    last_page = False
    offset = 0
    # Pages use an off_set in groups of 50
    while not last_page:
        request_content = lambda_get(
            archived_tournaments_url + '&offset={}'.format(offset)
        )
        if request_content is None:
            continue
        next_url_paths = parse_archived_tournament_list(request_content)
        hltv_url_paths += next_url_paths
        if len(next_url_paths) < 50:
            last_page = True

        offset += 50
    return hltv_url_paths


def parse_tournament_url_paths(request_content):
    """Parse tournament url paths.

    :param request_content: Content of a request
    :type request_content: bytes or str
    :return; Hltv tournament url paths
    :rtype: list
    """
    hltv_url_paths = []

    tournaments_list_html = BeautifulSoup(
        request_content,
        'html.parser'
    )

    # Get ongoing tournaments url paths
    ongoing_tournaments_html = tournaments_list_html.select(
        'div.ongoing-events-holder a'
    )
    for ongoing_tournament_html in ongoing_tournaments_html:
        hltv_url_paths.append(ongoing_tournament_html.get('href'))

    # Get upcoming tournaments url paths
    upcoming_tournaments_html = tournaments_list_html.select(
        'div.events-holder div.events-month a'
    )
    for upcoming_tournament_html in upcoming_tournaments_html:
        hltv_url_paths.append(upcoming_tournament_html.get('href'))

    return hltv_url_paths


def get_tournament_url_paths(start_date=None, end_date=None, include_archived=False):
    """Get tournament names and their urls.

    Upcoming tournaments are found on https://www.hltv.org/events. This
    links straight to the tournament's page.

    Archived tournaments can be filtered with a start and end date. This is
    useful when wanting to only update a small subset of historic games.

    When a tournament is taking place, including the archived games will not
    be necessary as only the current games are of interest to keep the UI up
    to date.

    :param start_date: Filter if 'include_archived' is selected ('%Y-%m-%d')
    :type start_date: str
    :param end_date: Filter if 'include_archived' is selected ('%Y-%m-%d')
    :type end_date: str
    :param include_archived: Flag to scrape historic games
    :type include_archived: bool
    :return: Tournament names and hltv_url_paths
    :rtype: dict
    """
    logger.info('Scraper: Getting tournament urls')
    hltv_url_paths = []

    # Get archived tournaments url paths
    if include_archived:
        hltv_url_paths += get_archived_tournament_url_paths(
            start_date=start_date, end_date=end_date
        )

    # Page for all the ongoing and upcoming events
    request_content = lambda_get('{}/events'.format(BASE_URL))
    if request_content is not None:
        hltv_url_paths += parse_tournament_url_paths(request_content)

    return hltv_url_paths


def parse_tournament_dates(request_content):
    """Extract a list of sorted tournament dates from tournament details page.

    Tournament details page have either a date, or date range for a
    tournament. The span element displaying the date has an attribute with
    a unix_epoch time (with precision up to milliseconds)

    :param request_content: Tournament page request content
    :type request_content: bytes or str
    :return: Start and end date of a tournament as a datetime
    :rtype: dict
    """
    tournament_html = BeautifulSoup(
        request_content,
        'html.parser'
    )

    result = {}

    # Select the table element for the unix time stamp and convert it to a
    # Python datetime object
    unix_data_spans = tournament_html.findAll(
        'td', {'class': 'eventdate'}
    )[0].findAll('span', {'data-unix': True})
    tournament_datetimes = []
    for unix_data_span in unix_data_spans:
        try:
            tournament_datetimes.append(
                datetime.fromtimestamp(
                    int(unix_data_span['data-unix'][:-3]),  # ignore miliseconds
                    tz=pytz.utc
                )
            )
        except IndexError:
            # Don't break if there is no span
            result['is_valid'] = False

    if len(tournament_datetimes) == 1:
        # Only a start date. i.e. the whole tournament is held on one day
        result['start_date'] = result['end_date'] = tournament_datetimes[0]
    elif len(tournament_datetimes) == 2:
        # Start date and end date
        tournament_datetimes = sorted(tournament_datetimes)
        result['start_date'] = tournament_datetimes[0]
        result['end_date'] = tournament_datetimes[1]
    else:
        result['start_date'] = result['end_date'] = None
        result['is_valid'] = False

    return result


def parse_tournament(request_content):
    """Scrape tournament information from the tournament's details page.

    A tournament is only valid if it can successfully find the following
    values from the tournament details page.
        - Teams attending
        - Prize money
        - Online/Offline status

    By default this will scrape only the current games. If archived games
    are wanted, the urls must be fetched using "get_tournament_url_paths"
    with the required filters.

    :param request_content: Tournament details page request content
    :type request_content: bytes or str
    :return: Tournaments and their details
    :rtype: dict
    """
    # This gets the following details in order:
    # 1. Attending teams
    # 2. Prize money
    # 3. Online/Offline
    tournament_details = {
        'is_valid': True,
        'teams': []
    }
    details_html = BeautifulSoup(
        request_content,
        'html.parser'
    )

    tournament_details['name'] = html.unescape(
        details_html.select('div.eventname')[0].text
    )
    if not tournament_details['name']:
        tournament_details['is_valid'] = False

    teams_html = details_html.select('div.team-name')
    for team_html in teams_html:
        try:
            team_hltv_url_path = team_html.find('a').get('href')
            team_details = {
                'name': html.unescape(team_html.find('div', {'class': 'text'}).text.strip()),
                'hltv_url_path': team_hltv_url_path,
                'hltv_external_id': int(team_hltv_url_path.split('/')[2])
            }
            tournament_details['teams'].append(team_details)
        except AttributeError:
            # HLTV has Javascript widgets on their tournaments pages which
            # show the odds for up-coming games between teams. These odds
            # also have the class 'div.team-name' but are duplicates of
            # teams that are playing in the tournaments and can safely be
            # ignored.
            pass

    tournament_details.update(parse_tournament_dates(request_content))

    # Prize money is the next td after the date and should be as a
    # decimal field
    try:
        tournament_details['prize_money'] = Decimal(details_html.find(
            'span', {'data-unix': True}
        ).find_next(
            'td'
        ).text.lstrip('$').replace(',', ''))
        tournament_details['prize_money_currency'] = 'USD'
    except AttributeError:
        tournament_details['is_valid'] = False
    except InvalidOperation:
        pass

    # Online tournaments have the country element of the overview page
    # set to 'Online' if it is an online tournament, otherwise it is a
    # city and country
    try:
        tournament_details['offline'] = 'Online' not in details_html.select(
            'table.info td div.flag-align'
        )[0].text
    except AttributeError:
        tournament_details['is_valid'] = False

    return tournament_details


def scrape_tournaments(hltv_url_paths=None):
    """Scrape tournaments details.

    :param hltv_url_paths: Hltv urls to scrape
    :type hltv_url_paths: list or None
    :return: Tournament details
    :rtype: list
    """
    logger.info('Scraper: Tournament scrape started')
    if hltv_url_paths is None:
        hltv_url_paths = get_tournament_url_paths()

    tournaments_details = []
    for hltv_url_path in hltv_url_paths:
        logger.info('Scraper: Getting tournament %s', hltv_url_path)
        request_content = lambda_get('{}{}'.format(
            BASE_URL,
            hltv_url_path
        ))
        if request_content is None:
            continue
        tournament_details = parse_tournament(request_content=request_content)
        tournament_details['hltv_url_path'] = hltv_url_path
        tournament_details['hltv_external_id'] = int(hltv_url_path.split('/')[2])
        tournaments_details.append(tournament_details)

    return tournaments_details
