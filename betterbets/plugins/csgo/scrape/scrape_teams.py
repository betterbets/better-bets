import html
import logging

from bs4 import BeautifulSoup

from main.lambda_get import lambda_get

MAP_COUNT = 100
BASE_URL = 'https://www.hltv.org/stats/teams?minMapCount={}'.format(MAP_COUNT)


logger = logging.getLogger(__name__)


def parse_teams(request_content=None):
    """Parse hltv teams.

    :param request_content: Html content of teams page
    :type request_content: bytes or str
    :return: All teams and their details
    :rtype: list
    """
    teams_html = BeautifulSoup(request_content, 'html.parser')

    teams = []
    for team_html in teams_html.select('td.teamCol'):
        name = html.unescape(team_html.text.strip())
        # Construct the teams 'overview' path. The path scraped is the
        # 'stats' page for the team.
        # Example:
        # "/stats/teams/6392/All-Star%20Dragon" -> "/team/6392/All-Star%20Dragon"
        hltv_stats_url_path = team_html.findAll('a')[0].get('href')
        hltv_url_path = '/team/{}'.format(
            '/'.join(hltv_stats_url_path.split('/')[-2:])
        )
        teams.append({
            'name': name,
            'hltv_url_path': hltv_url_path,
            'hltv_external_id': int(hltv_url_path.split('/')[2])
        })
    return teams


def scrape_teams():
    """Scrape csgo team names from hltv.org.

    Note: Apostrophes are represented as "&apos;"

    :return: All teams and their details
    :rtype: list
    """
    logger.info('Scraper: Team scrape started')
    request_content = lambda_get(BASE_URL)
    if request_content is not None:
        teams = parse_teams(request_content)
    else:
        teams = []
    return teams
