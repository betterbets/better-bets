import html
import logging

from bs4 import BeautifulSoup

from main.lambda_get import lambda_get

BASE_URL = 'https://www.hltv.org'
MAP_COUNT = 100


logger = logging.getLogger(__name__)


def parse_player(request_content):
    """Parse a player's stats page.

    :param request_content: Player stats page request content
    :type request_content: bytes or str
    :return: Player details
    :rtype: dict
    """
    player_stats_html = BeautifulSoup(request_content, 'html.parser')

    player = {}
    # Player first_name, last_name and alias is presented in the html
    # encoded form:
    # Hansel &apos;BnTeT&apos; Ferdinand
    player_names = html.unescape(player_stats_html.find(
        'h1', {'class': 'statsPlayerName'}
    ).text).split(' ')
    player['first_name'] = player_names[0]
    player['alias'] = player_names[1].strip("'")
    player['last_name'] = player_names[2]

    player['country'] = html.unescape(player_stats_html.select(
        'div.divided-row img.flag'
    )[0].find_next('span').text)

    return player


def get_player(player_stats_url):
    """Get a players details.

    :param player_stats_url: Hltv URL path of the players stats page
    :type player_stats_url: str
    :return: Player details
    :rtype: dict
    """
    logger.info('Scraper: Getting player %s', player_stats_url)
    player_details = {
        'hltv_url_path': player_stats_url,
        'hltv_external_id': player_stats_url.split('/')[3]
    }
    # Players have two details pages, one containing more detailed
    # information about stats, denoted player_stats.
    player_stats_request = lambda_get('{}{}'.format(BASE_URL, player_stats_url))
    if player_stats_request is not None:
        player_details.update(parse_player(player_stats_request))
    return player_details


def parse_players_list(request_content):
    """Get the list of player_links.

    :param request_content: Request content of player stats list view
    :type request_content: bytes or str
    :return: Player stats links
    :rtype: list
    """
    player_list_html = BeautifulSoup(request_content, 'html.parser')

    player_url_paths = []
    player_urls = player_list_html.select('table.stats-table tbody tr')
    for player_link_html in player_urls:
        player_url_paths.append(
            player_link_html.select('td.playerCol a')[0].get('href')
        )
    return player_url_paths


def get_players(request_content):
    """Get player details from hltv player list view.

    :param request_content: Request content of the player list page
    :type request_content: bytes or str
    :return: players details
    :rtype: dict
    """
    # FIXME: This should really be a list of details
    players = []
    player_stats_urls = parse_players_list(request_content=request_content)
    for player_stats_url in player_stats_urls:
        player_details = get_player(player_stats_url)
        players.append(player_details)
    return players


def scrape_players(map_count=None):
    """Get cso player details.

    :param map_count: Filter for the number of maps a player has played
    :type map_count: int
    :return: Players and their details
    :rtype: list
    """
    logger.info('Scraper: Player scrape started')
    if map_count is None:
        map_count = MAP_COUNT
    request_content = lambda_get('{}/stats/players?minMapCount={}'.format(BASE_URL, map_count))
    if request_content is None:
        players = []
    else:
        players = get_players(request_content=request_content)
    return players
