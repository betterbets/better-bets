import itertools
import json
import logging

import numpy as np
from django.db import models
from django.utils.functional import cached_property

from main.models import BettingSite, Odds, Series, SeriesTeam
from main.risk import calculate_risk_level
from plugins.csgo.client import CsgoPandasClient
from plugins.csgo.predict import (
    PredictException, get_team_form, hot_vector, load_model_details
)
from plugins.csgo.scrape import scrape_series

from .game import CsgoGame, CsgoGamePlayerTeam, CsgoGameTeam
from .map import CsgoMap
from .team import CsgoTeam
from .tournament import CsgoTournament

logger = logging.getLogger(__name__)


class CsgoSeriesTeam(SeriesTeam):
    team = models.ForeignKey('CsgoTeam', on_delete=models.CASCADE)
    series = models.ForeignKey('CsgoSeries', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('team', 'series')

    @property
    def current_odds(self):
        """Current odds for each betting site."""
        odds = []
        for betting_site in BettingSite.objects.all():
            try:
                odds.append(
                    self.odds.order_by(
                        'save_time'
                    ).filter(
                        betting_site=betting_site
                    )[0]
                )
            except IndexError:
                continue
        return odds

    @property
    def map_win_rates(self):
        """Calculate the lifetime win rates of a team for each map.

        :return: Win rates of each map
        :rtype: list
        """
        win_rates = []
        # Default, Tuscan and Season are not played competitively
        for csgo_map in CsgoMap.objects.exclude(name__in=['Default', 'Tuscan', 'Season']):
            games = CsgoGame.objects.filter(teams=self.team, map=csgo_map)
            num_games = games.count()
            num_wins = games.filter(winner=self.team).count()
            if games:
                win_rate = round((num_wins / num_games) * 100, 2)
            else:
                win_rate = -1

            win_rates.append({'map': csgo_map.name, 'winrate': win_rate})
        return win_rates

    @property
    def players(self):
        """Players playing for a team in a given series.

        :return: Players on a team in a given series
        :rtype: list
        """
        game_players = set()
        for game in self.series.games.all():
            for game_player in CsgoGamePlayerTeam.objects.filter(game=game, team=self.team):
                game_players.add(game_player.player)
        return list(game_players)

    def calculate_probability(self):
        """Calculate probability of winning the series.

        Using the probability of winning each game, calculate the
        probability of winning the series

        :return: Probability of winning the series
        :rtype: float or None
        """
        probabilities = CsgoGameTeam.objects.filter(
            game__in=self.series.games.all(),
            team=self.team
        ).values_list('probability', flat=True)

        series_length = self.series.length
        if self.series.length % 2 == 1:
            at_least_win = int(series_length / 2 + 1)

            # generate all possible winning outcomes
            winning_outcomes = [
                outcome for outcome in itertools.product([0, 1], repeat=series_length)
                if sum(outcome) >= at_least_win
            ]

            probability = 0.
            for outcome in winning_outcomes:
                # Sum rows and multiply items
                row_prob = 0
                for game, item in enumerate(outcome):
                    if game == 0:
                        row_prob = (1 - probabilities[game]) * (1 - item) + item * probabilities[game]
                    else:
                        row_prob *= (1 - probabilities[game]) * (1 - item) + item * probabilities[game]
                probability += row_prob
        else:
            # If even series, just calculate the average
            try:
                probability = sum(probabilities) / len(probabilities)
            except ZeroDivisionError:
                # If there are no games for a series, there will be no
                # probabilities and hence ZeroDivisionError
                probability = None

        return probability

    def __str__(self):
        return '{}: {}'.format(self.series, self.team)


class CsgoSeriesManager(models.Manager):
    def create_from_details(self, details):
        """Create a CSGO Series from scraped details.

        Note: Teams have already been updated as part of update_or_create.

        :param details: Details of a CSGO Series
        :type details: dict
        :return: Details of the created series with the new ID
        :rtype: dict
        """
        try:
            tournament = CsgoTournament.objects.get(name=details['tournament_name'])
        except CsgoTournament.DoesNotExist:
            tournament = None

        winner = None
        winning_team_details = details.get('winning_team_details')
        if winning_team_details is not None:
            winner = CsgoTeam.objects.alternate_name_get(
                name=winning_team_details['name'],
                hltv_url_path=winning_team_details['hltv_url_path']
            )

        series = self.create(
            length=details['length'],
            start_time=details['start_time'],
            hltv_url_path=details['hltv_url_path'],
            hltv_external_id=details['hltv_external_id'],
            tournament=tournament,
            winner=winner
        )
        details['id'] = series.id

        for team_details in details['teams']:
            CsgoSeriesTeam.objects.create(
                series=series,
                team=CsgoTeam.objects.get(id=team_details['id'])
            )

        details['games'] = CsgoGame.process(games_details=details['games'], series=series)

        return details


class CsgoSeries(Series):
    """Csgo series as shown on hltv.

    Series can exist without tournaments. A series can also be created
    before the tournament has been scraped.
    """
    tournament = models.ForeignKey(
        'csgo.CsgoTournament',
        related_name='series',
        blank=True,
        null=True,
        on_delete=models.PROTECT
    )
    teams = models.ManyToManyField(
        'csgo.CsgoTeam',
        through=CsgoSeriesTeam,
        related_name='series'
    )
    winner = models.ForeignKey(
        'csgo.CsgoTeam',
        blank=True,
        null=True,
        on_delete=models.PROTECT
    )
    length = models.IntegerField(help_text='Number of possible games')
    hltv_url_path = models.CharField(max_length=255, blank=True, null=True)
    hltv_external_id = models.IntegerField(blank=True, null=True, unique=True)

    objects = CsgoSeriesManager()

    class Meta:
        verbose_name_plural = 'Csgo series'
        ordering = ('start_time',)

    @classmethod
    def process(cls, details=None):
        """Process CSGO Series details.

        :param details: Details of CSGO Series from scraping hltv.org
        :type details: list
        :return: Ids of the series that have been updated
        :rtype: list
        """
        logger.info('Processing Series: Started')
        series_ids = []
        created_series = []  # TODO: Used for email notification
        updated_series = []  # TODO: Used for email notification

        if details is None:
            details = scrape_series()

        for series_details in details:
            process_details, created = cls.update_or_create(details=series_details)
            if created:
                created_series.append(process_details)
            else:
                updated_series.append(process_details)
            series_ids.append(process_details['id'])
        logger.info(
            'Processing Series: %s series created, %s series updated',
            len(created_series),
            len(updated_series)
        )
        return series_ids

    @classmethod
    def update_or_create(cls, details):
        """Update or create a CSGO series.

        :param details: Details of a team
        :type details: dict
        :return: Details of a series and if it was created
        :rtype: tuple
        """
        processed_team_details = []
        for team_details in details['teams']:
            process_details, _ = CsgoTeam.update_or_create(details=team_details)
            processed_team_details.append(process_details)
        details['teams'] = processed_team_details

        try:
            series = cls.objects.get(hltv_external_id=details['hltv_external_id'])
            logger.info('Updating Series: %s', series)
            return series.update_from_details(details), False
        except CsgoSeries.DoesNotExist:
            logger.info('Creating Series')
            return cls.objects.create_from_details(details), True

    @property
    def score(self):
        return

    @property
    def title(self):
        teams = self.teams.all().order_by('name')
        title_components = []
        for team_num in range(2):
            try:
                title_components.append(teams[team_num].name)
            except IndexError:
                title_components.append('TBA')
        return ' vs '.join(title_component for title_component in title_components)

    @cached_property
    def model_winner(self):
        """Return predicted winner of a series."""
        try:
            team_1, team_2 = self.csgoseriesteam_set.all()
            team_1_percentage, team_2_percentage = team_1.probability_percentage, team_2.probability_percentage
            if team_1_percentage is not None and team_2_percentage is not None:
                if team_1_percentage != 50:
                    if team_1_percentage > 50:
                        return {'name': team_1.team.name, 'percentage': team_1_percentage}
                    return {'name': team_2.team.name, 'percentage': team_2_percentage}
        except ValueError:
            # If team does not have 2 teams, there can be no winner
            return None

    @property
    def ordered_teams(self):
        return self.teams.all()

    @property
    def ordered_seriesteams(self):
        return self.csgoseriesteam_set.order_by('team__name').prefetch_related('team')

    @property
    def teams_betting_sites(self):
        betting_sites = set()
        for team in self.csgoseriesteam_set.all():
            for odds in team.current_odds:
                betting_sites.add(odds.betting_site)
        return betting_sites

    @property
    def head_to_head_details(self):
        """Calculate series details for d3 gaphing.

         - Team map win rates
         - Head to head win rates

         Head to head means any games where the two teams have played each
         other before.

        :return: Match history details between teams of a series
        :rtype: dict
        """
        details = {}
        for i, seriesteam in enumerate(self.ordered_seriesteams.all()):
            details[f't{i + 1}_map_winrates'] = json.dumps(seriesteam.map_win_rates)
            details[f't{i + 1}_roster'] = [player.alias for player in seriesteam.players]

        try:
            t1, t2 = self.teams.all()
        except ValueError:
            return details

        previous_games = CsgoGame.objects.filter(teams=t1).filter(teams=t2).exclude(winner__isnull=True)
        num_previous_games = previous_games.count()
        try:
            details['h2h_winrate_t1'] = previous_games.filter(winner=t1).count() / num_previous_games
            details['h2h_winrate_t2'] = previous_games.filter(winner=t2).count() / num_previous_games
        except ZeroDivisionError:
            details['h2h_winrate_t1'] = details['h2h_winrate_t2'] = 0.
        details['h2h_num_games'] = num_previous_games

        return details

    @cached_property
    def risk_level(self):
        std_dev = self.games.all().aggregate(models.Avg('standard_deviation')).get(
            'standard_deviation__avg'
        )
        if std_dev is None:
            return None
        return calculate_risk_level(std_dev)

    def scrape(self):
        """Scrape CSGO series details and update record."""
        logger.info('Re-scraping: "%s"', self)
        try:
            return scrape_series(hltv_url_paths=[self.hltv_url_path])[0]
        except IndexError:
            # Scrape exception was raised, and no details could be scraped
            return {}

    def update_from_details(self, details):
        """Update a CsgoSeries from the details of a scrape.

        Note: Teams have already been updated as part of update_or_create.

        :param details: Details of a CsgoSeries
        :type details: dict
        :return: Changed fields from an updated series
        :rtype: dict
        """
        details['id'] = self.id
        details['changes'] = {}

        try:
            tournament = CsgoTournament.objects.get(name=details['tournament_name'])
        except CsgoTournament.DoesNotExist:
            tournament = None

        winner = None
        winning_team_details = details.get('winning_team_details')
        if winning_team_details is not None:
            winner = CsgoTeam.objects.get(
                hltv_external_id=winning_team_details['hltv_external_id']
            )

        changes = self.update(
            start_time=details['start_time'],
            tournament=tournament,
            winner=winner,
            length=details['length'],
            hltv_url_path=details['hltv_url_path'],
            hltv_external_id=details['hltv_external_id']
        )
        if changes:
            details['changes'] = changes

        # The latest state of teams as given by a scrape, processed in
        # update_or_create
        new_teams = CsgoTeam.objects.filter(id__in=[
            team_details['id'] for team_details in details['teams']
        ])
        details['changes'].update(
            self.update_series_teams(set(new_teams))
        )

        details['games'] = CsgoGame.process(
            games_details=details['games'],
            series=self
        )

        if not details['changes']:
            # Remove changes if there are none
            details.pop('changes')

        return details

    def update_series_teams(self, new_teams):
        """Update CSGO teams for a series.

        At this point in the processing, CsgoTeam objects have already been
        created or updated as per the scrape details in self.update_or_create
        before the CsgoSeries itself gets processed.

        All this method does is check the SeriesTeams for any changes and
        updates the difference. Deleting teams that are no longer on the
        HLTV page and adding any new ones.

        :param new_teams: Any new teams for a series
        :type new_teams: set
        :return: Changes in teams
        :rtype: dict
        """
        changes = {}
        current_teams = set(self.teams.all())
        teams_to_create = new_teams - current_teams
        teams_to_delete = current_teams - new_teams

        for team_to_create in teams_to_create:
            CsgoSeriesTeam.objects.get_or_create(
                series=self,
                team=team_to_create
            )
        if teams_to_delete:
            CsgoSeriesTeam.objects.filter(
                series=self,
                team__in=current_teams
            ).delete()

        if teams_to_create or teams_to_delete:
            # If teams are being added or removed, show the changes
            changes = {
                'teams': {
                    'old': [repr(old_team) for old_team in current_teams],
                    'new': [repr(new_team) for new_team in new_teams]
                }
            }
        return changes

    def preprocess_series(self, client, return_team_order=False, model_details=None):
        """Constructs a preprocessed batch of data from the series."""
        # Get associated game data
        game_data = client.get_predicting_series_games(self.id)

        if game_data.empty:
            # Not all upcoming series have games/teams decided
            raise PredictException(
                'Series ({}) has no games to generate a predictions for'.format(self.id)
            )

        # Get roster data and add to games
        t1_rosters = game_data[['game_id', 'team_id']].apply(
            lambda x: (client.get_roster(x['game_id'], x['team_id']),), axis=1
        ).map(lambda x: x[0])

        t2_rosters = game_data[['game_id', 'opponent_id']].apply(
            lambda x: (client.get_roster(x['game_id'], x['opponent_id']),), axis=1
        ).map(lambda x: x[0])

        game_data['t1_roster'] = [d for d in t1_rosters]
        game_data['t2_roster'] = [d for d in t2_rosters]

        # Get encoded vectors of roster
        t1_rosters_enc = np.array(
            [d for d in t1_rosters.map(lambda x: hot_vector(x, length=model_details['max_player'])).values])
        t2_rosters_enc = np.array(
            [d for d in t2_rosters.map(lambda x: hot_vector(x, length=model_details['max_player'])).values])

        # get player histories for each team
        t1form = game_data[['t1_roster', 'start_time']].apply(
            lambda x: get_team_form(x[0], x[1], client),
            axis=1
        )
        t1form = np.array([d for d in t1form.t1_roster.values]).transpose((0, 2, 3, 1))
        t2form = game_data[['t2_roster', 'start_time']].apply(
            lambda x: get_team_form(x[0], x[1], client),
            axis=1
        )
        t2form = np.array([d for d in t2form.t2_roster.values]).transpose((0, 2, 3, 1))

        # roster difference vector - rather than 2 separate vectors
        rosters_df = t1_rosters_enc - t2_rosters_enc
        # rosters_df = np.array([row for row in rosters_enc])

        # encode maps
        map_df = game_data.map_id.map(lambda x: hot_vector([x], model_details['max_map']))

        # combine features
        game_cols = ['lan', 'prize_money', 'year', 'best_of']
        predict_feats = {
            'game data': game_data.loc[:, game_cols].values,
            'map data': np.array([f for f in map_df.values]),
            'roster data': rosters_df,
            't1 player history': t1form,
            't2 player history': t2form,
        }

        # For each game in the series make a prediction with self.model
        # feed in same features that were used in self.input_feats
        # scale data using the scalars
        for feat in model_details['input_data']:
            feature = predict_feats[feat['name']]
            if feat['scale']:
                # scale the data & replace features
                # otherwise, apply no scaling
                predict_feats[feat['name']] = feat['scaler'].transform(feature)
                max_feat = np.max(predict_feats[feat['name']])
                if max_feat > 1.:
                    predict_feats[feat['name']] = predict_feats[feat['name']] / max_feat

        # Get targets
        target_batch = game_data['t1win'].values
        input_batch = [predict_feats[feat['name']] for feat in model_details['input_data']]

        if return_team_order:
            # compute team order
            team_order = []
            for g in range(len(game_data)):
                team_order.append([
                    game_data.team_id.values[g],
                    game_data.opponent_id.values[g],
                    game_data.game_id.values[g]
                ])

            return input_batch, target_batch, team_order
        return input_batch, target_batch

    def encode_and_predict(self, model_details=None):
        """Generate predictions of all games for a given series.

        :param model_details: Model details required to get a prediction
        :type model_details: dict
        :return: Probabilities for each team in each game of the series
        :rtype: dict or None
        """
        if model_details is None:
            model_details = load_model_details()

        client = CsgoPandasClient()
        # Pre-process the series data
        input_batch, _, team_order = self.preprocess_series(client, True, model_details)
        client.close()

        # Some rows may need to be ignored
        ignores = input_batch[2].max(axis=1)
        game_ids = list(set([t[2] for t in team_order]))
        games_predictions = []
        for game_id in game_ids:
            game_prediction = {
                'game_id': game_id
            }

            # Calculate prediction for each model
            team_1, team_2 = CsgoGame.objects.get(id=game_id).teams.all()
            model_predictions = {
                team_1.name: [],
                team_2.name: [],
            }
            for model in model_details['loaded_models']:
                game_predictions = model.predict(input_batch)
                for i, _ in enumerate(game_predictions):
                    if ignores[i]:
                        # Team 1 prediction
                        model_predictions[
                            CsgoTeam.objects.get(id=team_order[i][0]).name
                        ].append(game_predictions[i][0])
                        # Team 2 prediction
                        model_predictions[
                            CsgoTeam.objects.get(id=team_order[i][1]).name
                        ].append(1. - game_predictions[i][0])

            # Summarise model predictions
            game_prediction[team_1.name] = np.mean(model_predictions[team_1.name])
            game_prediction[team_2.name] = np.mean(model_predictions[team_2.name])
            game_prediction['standard_deviation'] = np.std(
                model_predictions[team_1.name]
            )
            games_predictions.append(game_prediction)

        return games_predictions

    def predict(self, model_details=None):
        """Predict outcome of a CSGO Series' game(s).

        :param model_details: Pre-loaded model details
        :type model_details: dict
        :return: Details of a predicted series
        :rtype: dict
        """
        if model_details is None:
            model_details = load_model_details()

        prediction_details = self.encode_and_predict(model_details=model_details)

        for prediction in prediction_details:
            game = self.games.get(id=prediction['game_id'])
            game.standard_deviation = prediction['standard_deviation']
            game.save()

            for game_team in CsgoGameTeam.objects.filter(game=game):
                game_team.probability = prediction[game_team.team.name]
                game_team.save()

        for series_team in self.ordered_seriesteams:
            series_team.probability = series_team.calculate_probability()
            series_team.save()

        return prediction_details

    def __str__(self):
        return '{} BO{} ({})'.format(
            self.title,
            self.length,
            self.start_time.strftime('%d-%m-%Y %H:%M')
        )


class CsgoSeriesOdds(Odds):
    betting_site = models.ForeignKey(
        BettingSite,
        on_delete=models.PROTECT
    )
    betting_link = models.URLField(blank=True, null=True)
    arbitrage = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    series_team = models.ForeignKey(
        CsgoSeriesTeam,
        related_name='odds',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = 'Csgo series odds'
        unique_together = ('betting_site', 'series_team', 'save_time')

    def __str__(self):
        return '{} ({}): ${}'.format(
            self.series_team,
            self.betting_site.name,
            self.amount
        )
