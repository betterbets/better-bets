from django.db import models

SIDES = (('1', 'CT'), ('2', 'T'),)


class CsgoWinMethod(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return '{}'.format(self.name)


class CsgoRound(models.Model):
    game = models.ForeignKey(
        'csgo.CsgoGame',
        related_name='rounds',
        on_delete=models.PROTECT
    )
    teams = models.ManyToManyField(
        'csgo.CsgoTeam',
        through='csgo.CsgoRoundTeam',
        related_name='rounds'
    )
    players = models.ManyToManyField(
        'csgo.CsgoPlayer',
        through='csgo.CsgoRoundPlayer',
        related_name='rounds'
    )
    number = models.IntegerField()
    winner = models.ForeignKey(
        'csgo.CsgoTeam',
        related_name='round_wins',
        on_delete=models.PROTECT
    )
    win_method = models.ForeignKey(
        CsgoWinMethod,
        on_delete=models.PROTECT
    )

    class Meta:
        unique_together = ('game', 'number',)

    def __str__(self):
        return '{}: Round {}'.format(self.game, self.number)


class CsgoRoundPlayer(models.Model):
    round = models.ForeignKey('csgo.CsgoRound', on_delete=models.CASCADE)
    player = models.ForeignKey('csgo.CsgoPlayer', on_delete=models.CASCADE)
    side = models.CharField(max_length=1, choices=SIDES)
    kills = models.IntegerField()
    deaths = models.IntegerField()
    assists = models.IntegerField()

    def __str__(self):
        return '{}: {}'.format(self.round, self.player)


class CsgoRoundTeam(models.Model):
    round = models.ForeignKey('csgo.CsgoRound', on_delete=models.CASCADE)
    team = models.ForeignKey('csgo.CsgoTeam', on_delete=models.CASCADE)
    side = models.CharField(max_length=1, choices=SIDES)

    class Meta:
        unique_together = ('round', 'team')

    def __str__(self):
        return '{}: {} ({})'.format(self.round, self.team, self.get_side_display())
