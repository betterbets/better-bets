import logging
from datetime import date

from django.core.exceptions import ValidationError
from django.db import models

from main.models import Player, PlayerAlias
from plugins.csgo.scrape import scrape_players

logger = logging.getLogger(__name__)


class CsgoPlayerManager(models.Manager):
    def create_from_details(self, details):
        """Create a CsgoPlayer from the details of a scrape.

        :param details: Details of a CsgoPlayer
        :type details: dict
        :return: Details of the created player with the new ID
        :rtype: dict
        """
        player = self.create(**details)
        details['id'] = player.id
        return details


class CsgoPlayer(Player):
    alias = models.CharField(max_length=128, blank=True)
    hltv_url_path = models.CharField(max_length=255, blank=True, null=True)
    hltv_external_id = models.IntegerField(blank=True, null=True, unique=True)

    objects = CsgoPlayerManager()

    @classmethod
    def process(cls, details=None):
        """Process CSGO players.

        :param details: CSGO players details
        :type details: list
        """
        logger.info('Processing Players: Started')
        created_players = []
        updated_players = []

        if details is None:
            details = scrape_players()

        for player_details in details:
            process_details, created = CsgoPlayer.update_or_create(details=player_details)
            if created:
                created_players.append(process_details)
            else:
                updated_players.append(process_details)
        logger.info('Processing Players: Finished')

    @classmethod
    def update_or_create(cls, details):
        """Update or create CSGO player.

        :param details: Details of a player
        :type details: dict
        :return: Details of a player and if it was created
        :rtype: tuple
        """
        try:
            player = cls.objects.get(hltv_external_id=details['hltv_external_id'])
            return player.update_from_details(details), False
        except cls.DoesNotExist:
            return cls.objects.create_from_details(details), True

    def update_from_details(self, details):
        """Update a CsgoPlayer from the details of a scrape.

        :param details: Details of a CsgoPlayer
        :type details: dict
        :return: Changed fields from an updated player
        :rtype: dict
        """
        update_details = self.update(**details)
        if update_details:
            details['changes'] = update_details
        details['id'] = self.id
        return details

    def clean(self):
        """Override base clean method to accommodate for "alias".

        For a CsgoPlayer only "alias" needs to be defined, first_name and
        last_name may not be present on hltv.org.
        """
        if self.alias == '':
            raise ValidationError('Field "alias" needs to be defined.')

    def save(self, *args, **kwargs):
        """Update CsgoPlayerAlias.

         Csgo player's names can change over time. Create a CsgoPlayerAlias
         entry if the player is created for the first time and update if
         the alias has changed.
        """
        self.full_clean()
        super().save(*args, **kwargs)
        try:
            latest_player_alias = CsgoPlayerAlias.objects.get(player=self, end_date__isnull=True)
        except CsgoPlayerAlias.DoesNotExist:
            latest_player_alias = None
        if latest_player_alias:
            # Existing player being updated
            if self.alias != latest_player_alias.alias:
                latest_player_alias.end_date = date.today()
                player_alias = CsgoPlayerAlias(
                    player=self,
                    alias=self.alias,
                    start_date=date.today(),
                    end_date=None
                )
                latest_player_alias.save()
                player_alias.save()
        else:
            # First time a player is created
            player_alias = CsgoPlayerAlias(
                player=self,
                alias=self.alias,
                start_date=date.today(),
                end_date=None
            )
            player_alias.save()

    def __str__(self):
        return self.alias


class CsgoPlayerAlias(PlayerAlias):
    player = models.ForeignKey(
        CsgoPlayer,
        related_name='aliases',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = 'Csgo player aliases'

    def __str__(self):
        return '{} ({} - {} {}): {} {}'.format(
            self.alias,
            self.player.alias,
            self.player.first_name,
            self.player.last_name,
            self.start_date,
            '{}'.format(self.end_date and ' - {}'.format(self.end_date) or ''))


class CsgoAlternatePlayerAlias(models.Model):
    """Player alternate aliases.

    A list of alternate aliases (spelling differences, short names etc) to
    try matching on when scraping sites. This set of names is maintained by
    humans.
    """
    player = models.ForeignKey(
        CsgoPlayer,
        related_name='alternative_names',
        on_delete=models.CASCADE
    )
    alias = models.CharField(max_length=128)

    class Meta:
        verbose_name_plural = 'Csgo alternate player aliases'

    def __str__(self):
        return '{} - {}'.format(self.player, self.alias)
