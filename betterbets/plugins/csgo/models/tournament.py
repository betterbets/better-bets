import logging

from django.db import models

from main.models import Tournament
from plugins.csgo.scrape import scrape_tournaments

from .team import CsgoTeam

logger = logging.getLogger(__name__)


class CsgoTournamentManager(models.Manager):
    def create_from_details(self, details):
        """Create a CsgoTournament from scraped details.

        :param details: Details of a CsgoTournament
        :type details: dict
        :return: Details of the created tournament with the new ID
        :rtype: dict
        """
        tournament = self.create(
            name=details['name'],
            start_date=details['start_date'],
            end_date=details['end_date'],
            prize_money=details.get('prize_money'),
            prize_money_currency=details.get('prize_money_currency'),
            offline=details['offline'],
            hltv_url_path=details['hltv_url_path'],
            hltv_external_id=details['hltv_external_id']
        )
        details['id'] = tournament.id
        details['new_teams'] = []
        for team_index, team_details in enumerate(details['teams']):
            team_processed_details, created = CsgoTeam.update_or_create(
                details=team_details
            )
            details['teams'][team_index] = team_processed_details
            tournament.teams.add(
                CsgoTeam.objects.get(id=team_processed_details['id'])
            )
            if created:
                details['new_teams'].append(team_processed_details)
        return details


class CsgoTournament(Tournament):
    teams = models.ManyToManyField(
        'csgo.CsgoTeam',
        related_name='tournaments',
        blank=True,
    )
    hltv_url_path = models.CharField(max_length=255, blank=True, null=True)
    hltv_external_id = models.IntegerField(blank=True, null=True, unique=True)

    objects = CsgoTournamentManager()

    class Meta:
        ordering = ('start_date',)

    @classmethod
    def process(cls, details=None, hltv_url_paths=None):
        """Process CSGO tournaments.

        :param details: Details of a CSGO tournament
        :type details: list
        :param hltv_url_paths: HLTV urls for specific tournament pages
        :type hltv_url_paths: list
        """
        logger.info('Processing Tournaments: Started')
        if details is None and hltv_url_paths is None:
            details = scrape_tournaments(hltv_url_paths=hltv_url_paths)

        created_tournaments = []
        updated_tournaments = []
        created_teams = []

        for tournament_details in details:
            process_details, created = CsgoTournament.update_or_create(
                details=tournament_details
            )
            if created:
                created_tournaments.append(process_details)
            else:
                updated_tournaments.append(process_details)
            created_teams.append(
                team_details for team_details in process_details['new_teams']
            )
        logger.info('Processing Tournaments: Finished')

    @classmethod
    def update_or_create(cls, details):
        """Update or create valid CSGO tournament.

        :param details: Details of a CSGO tournament
        :type details: dict
        :return: Details of a tournament and if it was created
        :rtype: tuple
        """
        if details['is_valid'] is True:
            try:
                tournament = cls.objects.get(hltv_external_id=details['hltv_external_id'])
                return tournament.update_from_details(details), False
            except cls.DoesNotExist:
                return cls.objects.create_from_details(details), True
        return None, False

    def update_from_details(self, details):
        """Update tournament from scraped details.

        1. The teams themselves need to be updated and changed if the
           information differs from what is currently in the database
        2. The teams for the tournament need to be updated in case the teams
           have changed. (maybe a new team qualified or a team has dropped
           out)
        3. The tournament's own details need to be updated (hltv_url_path,
           start_time, end_time, etc)

        :param details: Details of a CSGO tournament
        :type details: dict
        :return: Details of the updated tournament
        :rtype: dict
        """
        details['id'] = self.id
        details['new_teams'] = []
        # Update the teams based on the details in the tournament's
        # details
        for team_index, team_details in enumerate(details['teams']):
            team_processed_details, created = CsgoTeam.update_or_create(
                details=team_details
            )
            details['teams'][team_index] = team_processed_details
            if created:
                details['new_teams'].append(team_processed_details)

        # Update the tournament's details themselves
        teams = []
        for team_details in details['teams']:
            teams.append(
                CsgoTeam.objects.get(hltv_external_id=team_details['hltv_external_id'])
            )
        update_details = self.update(
            name=details['name'],
            start_date=details['start_date'],
            end_date=details['end_date'],
            prize_money=details.get('prize_money'),
            prize_money_currency=details.get('prize_money_currency'),
            offline=details['offline'],
            hltv_url_path=details['hltv_url_path'],
            hltv_external_id=details['hltv_external_id'],
            teams=teams
        )
        if update_details:
            details['changes'] = update_details
        return details

    def scrape(self):
        return scrape_tournaments(hltv_url_paths=[self.hltv_url_path])[0]

    def __str__(self):
        return '{} ({})'.format(self.name, self.start_date.year)
