from django.db import models
from django.db.models import Max
from django.utils.functional import cached_property

from main.models import BettingSite, Game, GamePlayerTeam, GameTeam, Odds
from main.risk import calculate_risk_level

from .map import CsgoMap
from .player import CsgoPlayer
from .team import CsgoTeam


class CsgoGamePlayerTeamManager(models.Manager):
    def create_from_details(self, details, game, player, team):
        """Create CSGO Game Player Team from details.

        :param details: Details of a CsgoGamePlayerTeam
        :type details: dict
        :param game: The CsgoGame
        :type game: CsgoGame
        :param player: The CsgoPlayer
        :type player: CsgoPlayer
        :param team: The CsgoTeam
        :type team: CsgoTeam
        :return: Details of the created CsgoGamePlayerTeam
        :rtype: dict
        """
        kills = details.get('kills')
        deaths = details.get('deaths')
        game_player_team = self.create(
            game=game,
            player=player,
            team=team,
            # A player's kills and deaths will not be available
            # before the game has been played
            kills=kills,
            deaths=deaths
        )
        details['id'] = game_player_team.id
        return details


class CsgoGamePlayerTeam(GamePlayerTeam):
    game = models.ForeignKey('csgo.CsgoGame', on_delete=models.CASCADE)
    player = models.ForeignKey('csgo.CsgoPlayer', on_delete=models.CASCADE)
    team = models.ForeignKey('csgo.CsgoTeam', on_delete=models.CASCADE)
    # Kills and deaths should be properties of a game's round details.
    # This will be possible once game demo files are parsed.
    kills = models.IntegerField(null=True)
    deaths = models.IntegerField(null=True)

    objects = CsgoGamePlayerTeamManager()

    class Meta:
        # FIXME: Team should be included in the constraint
        unique_together = ('game', 'player')

    @classmethod
    def update_or_create(cls, details, game, player, team):
        """Update or create a PlayerTeam for a particular CSGO game.

        Processing of players for a particular game is different than when
        processing players to update/create them.

        :param details: Details of a CsgoGamePlayerTeam
        :type details: dict
        :param game: CsgoGame that is being updated
        :type game: CsgoGame
        :param player: CsgoPlayer that is being updated
        :type player: CsgoPlayer
        :param team: CsgoTeam that is being updated
        :type team: CsgoTeam
        :return: Details of the created/updated CsgoGamePlayerTeam
        :rtype: dict
        """
        try:
            game_player_team = cls.objects.get(
                game=game,
                player=player
            )
            update_details = game_player_team.update_from_details(
                details,
                game,
                player,
                team
            )
            return update_details, False
        except cls.DoesNotExist:
            return cls.objects.create_from_details(details, game, player, team), True

    def update_from_details(self, details, game, player, team):
        kills = details.get('kills')
        deaths = details.get('deaths')
        update_details = self.update(
            game=game,
            player=player,
            team=team,
            # A player's kills and deaths will not be available
            # before the game has been played
            kills=kills,
            deaths=deaths
        )
        if update_details:
            details['changes'] = update_details
        details['id'] = self.id
        return details

    def __str__(self):
        return '{}: {}'.format(self.game, self.player)


class CsgoGameTeam(GameTeam):
    team = models.ForeignKey('csgo.CsgoTeam', on_delete=models.CASCADE)
    game = models.ForeignKey('csgo.CsgoGame', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('team', 'game')

    @property
    def ct_score(self):
        # TODO: Once rounds have been implemented properly implement this
        return

    @property
    def t_score(self):
        # TODO: Once rounds have been implemented properly implement this
        return

    @property
    def score(self):
        # TODO: Once rounds have been implemented properly implement this
        return

    def __str__(self):
        return '{}: {}'.format(self.game, self.team)


class CsgoGameManager(models.Manager):
    def create_from_details(self, details, series):
        """Create CSGO Game from scraped details.

        :param details: Details for a CSGO Game
        :type details: dict
        :param series: Series for which the games are being updated
        :type series: CsgoSeries
        :return: Details of a created game with the new ID
        :rtype: dict
        """
        game = self.create(
            series=series,
            order=details['order']
        )
        details['id'] = game.id

        map_name = details.get('map_name')
        if map_name is not None:
            csgo_map, _ = CsgoMap.objects.get_or_create(name=map_name)
        else:
            csgo_map = None
        game.map = csgo_map

        for series_team in series.teams.all():
            # Games are only ever updated during a series as a game can not
            # exist without one. This means we can assume that a series has
            # already created all teams that will be present in the games,
            # and hence not have to create/update them.
            game_team, _ = CsgoGameTeam.objects.get_or_create(
                team=series_team,
                game=game
            )
            if game_team.team.name == details.get('winning_team_details', {}).get('name'):
                game.winner = game_team.team

        details['new_players'] = []
        for player_index, player_details in enumerate(details['players']):
            player_process_details, player_created = CsgoPlayer.update_or_create(
                details={
                    'alias': player_details.get('alias'),
                    'first_name': player_details.get('first_name'),
                    'last_name': player_details.get('last_name'),
                    'hltv_url_path': player_details.get('hltv_url_path'),
                    'hltv_external_id': player_details.get('hltv_external_id')
                }
            )
            player_details.update(player_process_details)
            player = CsgoPlayer.objects.get(id=player_process_details['id'])
            if player_created:
                details['new_players'].append(
                    player_process_details
                )
            # The series should already have all the teams present and updated
            team = series.teams.get(name=player_details['team_details']['name'])
            game_player_team_process_details, _ = CsgoGamePlayerTeam.update_or_create(
                player_details,
                game,
                player,
                team
            )
            details['players'][player_index].update(game_player_team_process_details)
        game.save()
        return details


class CsgoGame(Game):
    # order starts at 0, however, to a user games start with 1
    order = models.IntegerField(blank=True)
    teams = models.ManyToManyField(
        'csgo.CsgoTeam',
        through=CsgoGameTeam,
        related_name='games'
    )
    winner = models.ForeignKey(
        'csgo.CsgoTeam',
        related_name='wins',
        blank=True,
        null=True,
        on_delete=models.PROTECT
    )
    players = models.ManyToManyField(
        'csgo.CsgoPlayer',
        through=CsgoGamePlayerTeam,
        related_name='games',
    )
    map = models.ForeignKey(
        'csgo.CsgoMap',
        null=True,
        blank=True,
        on_delete=models.PROTECT
    )
    series = models.ForeignKey(
        'csgo.CsgoSeries',
        related_name='games',
        on_delete=models.PROTECT
    )

    objects = CsgoGameManager()

    class Meta:
        unique_together = ('order', 'series')
        ordering = ('start_time', 'order')

    @classmethod
    def process(cls, games_details, series):
        """Process CSGO games for a series.

        :param games_details: Details of a series' game
        :type games_details: list
        :param series: Series for which the games are being updated
        :type series: CsgoSeries
        :return: Details of the updated/created games
        :rtype: list
        """
        updated_games_details = []
        for game_details in games_details:
            process_details, _ = cls.update_or_create(details=game_details, series=series)
            updated_games_details.append(process_details)
        return updated_games_details

    @classmethod
    def update_or_create(cls, details, series):
        """Update or create a CSGO game.

        Games are only ever updated during a series as a game can not exist
        without one. This means we can assume that a series has already
        created all teams that will be present in the games, and hence not
        have to create/update them.

        :param series: Series for which the games are being updated
        :type series: CsgoSeries
        :param details: Details of a CSGO game
        :type details: dict
        :return: Details of the created game and if it was created
        :rtype: tuple
        """
        try:
            game = cls.objects.get(series=series, order=details['order'])
            return game.update_from_details(details, series), False
        except cls.DoesNotExist:
            return cls.objects.create_from_details(details, series), True

    @property
    def tournament(self):
        return self.series.tournament

    @cached_property
    def ordered_gameteams(self):
        return self.csgogameteam_set.all().order_by('team')

    @property
    def display_order(self):
        """Game order to show in any UI.

        Order count starts at 0. It is more intuitive for users to see the
        order starting from 1.

        Example:
            Game 1, Game 2, Game 3
        vs
            Game 0, Game 1, Game 2
        :return: Order in natural numbers
        :rtype: int
        """
        return self.order + 1

    @cached_property
    def risk_level(self):
        """The risk level associated with the prediction."""
        if self.standard_deviation:
            return calculate_risk_level(self.standard_deviation)
        return None

    def update_from_details(self, details, series):
        """Update a CSGO game.

        1. Update winner
        2. Update players
        3. Update teams

        :param details: Details of a CSGO game
        :type details: dict
        :param series: Series for which the games are being updated
        :type series: CsgoSeries
        :return: Details of the updated tournament
        :rtype: dict
        """
        details['id'] = self.id

        # Get winning team
        winning_team = None
        for series_team in series.teams.all():
            if series_team.name == details.get('winning_team_details', {}).get('name'):
                winning_team = series_team

        map_name = details.get('map_name')
        if map_name is not None:
            csgo_map, _ = CsgoMap.objects.get_or_create(name=map_name)
        else:
            csgo_map = None

        changes = self.update(
            map=csgo_map,
            winner=winning_team
        )
        details['changes'] = changes

        # A series will have already updated the teams, so we know those are
        # the new teams.
        details['changes'].update(
            self.update_game_teams(set(series.teams.all()))
        )

        updated_player_details = self.update_game_players(
            details['players']
        )
        details['players'] = updated_player_details['players']

        new_players = updated_player_details.get('new_players')
        if new_players is not None:
            details['new_players'] = new_players

        changes = updated_player_details.get('changes')
        if changes is not None:
            details['changes'].update(changes)

        return details

    def update_game_players(self, players_details):
        """Update CSGO players for a game.

        - Update the players themselves for the game (kills, deaths, etc)
        - Record the new players that are being created
        - Record the difference between the old and new ('current' from the
          scrape) players

        :param players_details: Details of the players for a game
        :type players_details: list
        :returns Changes in players
        :rtype: dict
        """
        results = {
            # Create a new players list as to not mess with the original
            # one in memory (as lists are mutable), and override it in the
            # 'details' dict in the update_from_details function.
            'players': []
        }
        created_players = []
        old_players = set(self.players.all())
        new_players = set()
        for player_details in players_details:
            try:
                details = {
                    'alias': player_details['alias'],
                    'first_name': player_details.get('first_name'),
                    'last_name': player_details.get('last_name'),
                    'hltv_url_path': player_details.get('hltv_url_path'),
                    'hltv_external_id': player_details.get('hltv_external_id')
                }
            except KeyError:
                # Players are TBA
                continue
            player_process_details, player_created = CsgoPlayer.update_or_create(
                details=details
            )
            player_details.update(player_process_details)
            player = CsgoPlayer.objects.get(id=player_process_details['id'])
            if player_created:
                created_players.append(
                    player_process_details
                )

            team = CsgoTeam.objects.get(
                hltv_external_id=player_details['team_details']['hltv_external_id']
            )

            game_player_team_details, _ = CsgoGamePlayerTeam.update_or_create(
                details=player_details,
                game=self,
                player=player,
                team=team
            )
            results['players'].append(game_player_team_details)
            new_players.add(player)
        if created_players:
            # As per convention, the created objects are called 'new'
            results['new_players'] = created_players

        if old_players != new_players:
            results['changes'] = {
                'players': {
                    'old': [repr(old_player) for old_player in old_players],
                    'new': [repr(new_player) for new_player in new_players]
                }
            }

        players_to_delete = old_players - new_players
        if players_to_delete:
            CsgoGamePlayerTeam.objects.filter(
                game=self,
                player__in=players_to_delete
            ).delete()

        return results

    def update_game_teams(self, new_teams):
        """Update CSGO teams for a game.

        Update game teams. Teams on a game should be the same as on the
        series. By this point, a series will already have all the teams set
        so we simply have to check if the series teams, are the same as the
        teams on a the current game

        :param new_teams: Any new teams on the series
        :type new_teams: set
        :return: Changes in teams
        :rtype: dict
        """
        changes = {}
        current_teams = set(self.teams.all())
        teams_to_create = new_teams - current_teams
        teams_to_delete = current_teams - new_teams

        for new_team in new_teams:
            CsgoGameTeam.objects.get_or_create(
                game=self,
                team=new_team
            )
        if teams_to_delete:
            CsgoGameTeam.objects.filter(
                game=self,
                team__in=current_teams
            ).delete()
        if teams_to_create or teams_to_delete:
            # If teams are being added or removed, show the changes
            changes = {
                'teams': {
                    'old': [repr(old_team) for old_team in current_teams],
                    'new': [repr(new_team) for new_team in new_teams]
                }
            }
        return changes

    def save(self, *args, **kwargs):
        """Autoincrement order field for each game in a series."""
        if not self.order and self.order != 0:
            max_order = CsgoGame.objects.filter(
                series=self.series
            ).aggregate(Max('order'))['order__max']
            if max_order is None:
                self.order = 0
            else:
                self.order = max_order + 1
        super().save(*args, **kwargs)

    def __str__(self):
        try:
            map_name = self.map.name
        except AttributeError:
            map_name = 'Map TBD'
        return '{} - Game {}: {}'.format(
            self.series,
            self.order + 1,
            map_name,
        )


class CsgoGameOdds(Odds):
    betting_site = models.ForeignKey(BettingSite, on_delete=models.PROTECT)
    arbitrage = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    game_team = models.ForeignKey(
        'csgo.CsgoGameTeam',
        related_name='amounts',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = 'Csgo game odds'
        unique_together = ('betting_site', 'game_team', 'save_time')

    def __str__(self):
        return '{} ({}): ${}'.format(
            self.game_team,
            self.betting_site.name,
            self.amount
        )
