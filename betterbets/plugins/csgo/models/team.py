import logging
from datetime import date

from django.db import models

from main.models import Team, TeamName
from plugins.csgo.scrape import scrape_teams

logger = logging.getLogger(__name__)


class CsgoTeamManager(models.Manager):
    def create_from_details(self, details):
        """Create a CsgoTeam from the details of a scrape.

        :param details: Details of a CsgoTeam
        :type details: dict
        :return: Details of the created team with the new ID
        :rtype: dict
        """
        team = self.create(**details)
        details['id'] = team.id
        return details

    def alternate_name_get(self, name, **kwargs):
        """Allow alternate name matching.

        :param name: Name of the team to query
        :return: The team object
        :rtype: CsgoTeam
        :raises: CsgoTeam.DoesNotExist
        """
        try:
            team = self.get(name__iexact=name, **kwargs)
        except CsgoTeam.DoesNotExist:
            team = None

        if team is None:
            try:
                # If there is no match on the team's primary name, try find
                # with alternate team name
                team = CsgoAlternateTeamName.objects.get(name=name).team
            except CsgoAlternateTeamName.DoesNotExist:
                raise CsgoTeam.DoesNotExist
        return team


class CsgoTeam(Team):
    hltv_url_path = models.CharField(max_length=255, blank=True, null=True)
    hltv_external_id = models.IntegerField(blank=True, null=True, unique=True)

    objects = CsgoTeamManager()

    class Meta:
        ordering = ('name',)

    @classmethod
    def process(cls, details=None):
        """Process CSGO teams.

        :param details: CSGO teams details
        :type details: list
        """
        logger.info('Processing Teams: Started')
        created_teams = []
        updated_teams = []

        if details is None:
            details = scrape_teams()

        for team_details in details:
            process_details, created = cls.update_or_create(details=team_details)
            if created:
                created_teams.append(process_details)
            else:
                updated_teams.append(process_details)
        logger.info('Processing Teams: Finished')

    @classmethod
    def update_or_create(cls, details):
        """Update or create CSGO team.

        :param details: Details of a team
        :type details: dict
        :return: Details of a team and if it was created
        :rtype: tuple
        """
        try:
            team = cls.objects.get(hltv_external_id=details['hltv_external_id'])
            return team.update_from_details(details), False
        except cls.DoesNotExist:
            return cls.objects.create_from_details(details), True

    @property
    def names(self):
        return CsgoTeamName.objects.filter(team=self)

    @property
    def alternate_names(self):
        return CsgoAlternateTeamName.objects.filter(team=self)

    @property
    def as_dict(self):
        return {
            'pk': self.pk,
            'name': self.name,
            'hltv_url_path': self.hltv_url_path,
            'country': self.country
        }

    def update_from_details(self, details):
        """Update a CsgoTeam from the details of a scrape.

        :param details: Details of a CsgoTeam
        :type details: dict
        :return: Changed fields from an updated team
        :rtype: dict
        """
        update_details = self.update(**details)
        if update_details:
            details['changes'] = update_details
        details['id'] = self.id
        return details

    def save(self, *args, **kwargs):
        """Update CsgoTeamName table if the name has changed."""
        super().save(*args, **kwargs)

        try:
            latest_team_name = CsgoTeamName.objects.get(team=self, end_date__isnull=True)
            # Existing team being updated
            if self.name != latest_team_name.name:
                latest_team_name.end_date = date.today()
                team_name = CsgoTeamName(
                    team=self,
                    name=self.name,
                    start_date=date.today(),
                    end_date=None
                )
                latest_team_name.save()
                team_name.save()
        except CsgoTeamName.DoesNotExist:
            # First time a team is created
            CsgoTeamName.objects.create(
                team=self,
                name=self.name,
                start_date=date.today(),
                end_date=None
            )

    def __str__(self):
        return self.name


class CsgoTeamName(TeamName):
    """Store team names for a given period.

    Team names may change over time, this table allows us to keep track of a
    team's naming history
    """
    team = models.ForeignKey(
        CsgoTeam,
        related_name='team_names',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} ({}): {}{}'.format(
            self.name,
            self.team,
            self.start_date,
            '{}'.format(self.end_date and ' - {}'.format(self.end_date) or ''))


class CsgoAlternateTeamName(models.Model):
    """Team alternate names over all time.

    A list of alternate names to try matching on when scraping sites. This
    set of names is maintained by humans.
    """
    team = models.ForeignKey(
        CsgoTeam,
        related_name='alternative_names',
        on_delete=models.CASCADE
    )
    name = models.CharField(max_length=128, unique=True)
