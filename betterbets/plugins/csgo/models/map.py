from django.db import models


class CsgoMap(models.Model):
    name = models.CharField(max_length=64, unique=True)
    short_code = models.CharField(max_length=10, unique=True, blank=True, null=True, default=None)
    version = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return '{}{}'.format(
            self.name,
            self.version and ' ({})'.format(self.version) or ''
        )
