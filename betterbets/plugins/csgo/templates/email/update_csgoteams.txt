{% autoescape off %}
Started: {{ start }j
Finished: {{ end }}
Time Elapsed: {{ elapsed_time }}

----------------------------------------

  {% if created %}
    {{ no_created }}
    {% for team in created %}
        Name: {{ team.name }}
        URL: {{ team.hltv_url_path }}
        ---------------------------------------------------------------------------------------
    {% endfor %}
  {% else %}
    No teams created!
  {% endif %}
  {% if updated %}
    {{ no_updated }}
    {{ updated }}
  {% else %}
    No teams updated!
  {% endif %}
  {% if new_teams %}
    New Teams Added
    {% for team in  new_teams %}
      {{ team }}
    {% endfor %}
  {% endif %}
{% endautoescape %}
