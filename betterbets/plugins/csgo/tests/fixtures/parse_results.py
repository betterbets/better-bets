# flake8: noqa
from datetime import datetime
from decimal import Decimal

from django.utils import timezone

TEAM_RESULT = [
    {'name': "Bee's Money Crew", 'hltv_url_path': '/team/9999/Bee%27s%20Money%20Crew', 'hltv_external_id': 9999},
    {'name': 'Space Soldiers', 'hltv_url_path': '/team/5929/Space%20Soldiers', 'hltv_external_id': 5929},
    {'name': 'NiP', 'hltv_url_path': '/team/4411/NiP', 'hltv_external_id': 4411},
    {'name': 'Luminosity', 'hltv_url_path': '/team/6290/Luminosity', 'hltv_external_id': 6290},
    {'name': 'G2', 'hltv_url_path': '/team/5995/G2', 'hltv_external_id': 5995},
    {'name': 'TSM', 'hltv_url_path': '/team/5996/TSM', 'hltv_external_id': 5996},
    {'name': 'Liquid', 'hltv_url_path': '/team/5973/Liquid', 'hltv_external_id': 5973},
    {'name': 'Virtus.pro', 'hltv_url_path': '/team/5378/Virtus.pro', 'hltv_external_id': 5378},
    {'name': 'HellRaisers', 'hltv_url_path': '/team/5310/HellRaisers', 'hltv_external_id': 5310},
    {'name': 'EnVyUs', 'hltv_url_path': '/team/5991/EnVyUs', 'hltv_external_id': 5991},
    {'name': 'SK', 'hltv_url_path': '/team/6137/SK', 'hltv_external_id': 6137},
    {'name': 'fnatic', 'hltv_url_path': '/team/4991/fnatic', 'hltv_external_id': 4991},
    {'name': 'dignitas', 'hltv_url_path': '/team/5422/dignitas', 'hltv_external_id': 5422},
    {'name': 'LDLC', 'hltv_url_path': '/team/4674/LDLC', 'hltv_external_id': 4674},
    {'name': 'Natus Vincere', 'hltv_url_path': '/team/4608/Natus%20Vincere', 'hltv_external_id': 4608},
    {'name': 'Cloud9', 'hltv_url_path': '/team/5752/Cloud9', 'hltv_external_id': 5752},
    {'name': 'mousesports', 'hltv_url_path': '/team/4494/mousesports', 'hltv_external_id': 4494},
    {'name': 'FlipSid3', 'hltv_url_path': '/team/5988/FlipSid3', 'hltv_external_id': 5988},
    {'name': 'CLG', 'hltv_url_path': '/team/5974/CLG', 'hltv_external_id': 5974},
    {'name': 'PENTA', 'hltv_url_path': '/team/5395/PENTA', 'hltv_external_id': 5395},
    {'name': 'ALTERNATE aTTaX', 'hltv_url_path': '/team/4501/ALTERNATE%20aTTaX', 'hltv_external_id': 4501}
]

PLAYER_URL_LIST = [
    '/stats/players/7167/kennyS',
    '/stats/players/39/GeT_RiGhT',
    '/stats/players/7592/device',
    '/stats/players/2757/GuardiaN',
    '/stats/players/29/f0rest',
    '/stats/players/1225/shox',
    '/stats/players/695/allu',
    '/stats/players/7390/ScreaM',
    '/stats/players/7398/dupreeh',
    '/stats/players/3055/flusha',
    '/stats/players/3849/JW',
    '/stats/players/2469/cajunb',
    '/stats/players/7429/Happy',
    '/stats/players/2553/Snax',
    '/stats/players/2730/chrisJ',
    '/stats/players/7168/NBK-',
    '/stats/players/7322/apEX',
    '/stats/players/334/AdreN',
    '/stats/players/317/pashaBiceps',
    '/stats/players/5386/byali',
    '/stats/players/884/Xizt',
    '/stats/players/483/Edward',
    '/stats/players/4954/Xyp9x',
    '/stats/players/339/ANGE1',
    '/stats/players/735/Dosia',
    '/stats/players/3347/seized',
    '/stats/players/7148/friberg',
    '/stats/players/165/NEO',
    '/stats/players/161/TaZ',
    '/stats/players/338/markeloff',
    '/stats/players/7170/SmithZz',
    '/stats/players/429/karrigan',
    '/stats/players/7166/Ex6TenZ',
    '/stats/players/484/Zeus',
    '/stats/players/7156/MSL',
    '/stats/players/41/pronax'
]

PLAYER_DETAILS = {'alias': 'kennyS', 'country': 'France', 'first_name': 'Kenny', 'last_name': 'Schrub'}

PLAYER_DETAILS_AMPERSAND = {'alias': 'kenny\'S', 'country': 'France', 'first_name': 'Kenny', 'last_name': 'Schrub'}

SERIES_BEST_OF_ONE_RESULT = {
    'is_valid': True,
    'length': 1,
    'start_time': datetime(2017, 10, 1, 8, 0, tzinfo=timezone.get_default_timezone()),
    'winning_team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
    'tournament_name': 'AOC CyberGamer Premier League Spring 2017',
    'teams': [
        {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
        {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293}
    ],
    'games': [
        {
            'order': 0,
            'map_name': 'Mirage',
            'winning_team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
            'players': [
                {
                    'team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
                    'first_name': 'Jay', 'alias': 'liazz', 'last_name': 'Tregillgas',
                    'kills': 24, 'deaths': 9,
                    'hltv_url_path': '/stats/players/10588/liazz', 'hltv_external_id': 10588
                },
                {
                    'team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
                    'first_name': 'Travis', 'alias': 'wizard', 'last_name': 'Richardson',
                    'kills': 21, 'deaths': 10,
                    'hltv_url_path': '/stats/players/9105/wizard', 'hltv_external_id': 9105
                },
                {
                    'team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
                    'first_name': 'Chris', 'alias': 'emagine', 'last_name': 'Rowlands',
                    'kills': 20, 'deaths': 8,
                    'hltv_url_path': '/stats/players/8744/emagine', 'hltv_external_id': 8744
                },
                {
                    'team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
                    'first_name': 'Jordan', 'alias': 'Hatz', 'last_name': 'Bajic',
                    'kills': 13, 'deaths': 8,
                    'hltv_url_path': '/stats/players/11162/Hatz', 'hltv_external_id': 11162
                },
                {
                    'team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
                    'first_name': 'Simon', 'alias': 'Sico', 'last_name': 'Williams',
                    'kills': 10, 'deaths': 13,
                    'hltv_url_path': '/stats/players/9102/Sico', 'hltv_external_id': 9102
                },
                {
                    'team_details': {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293},
                    'first_name': 'Steve', 'alias': 'InJect', 'last_name': 'Honan',
                    'kills': 15, 'deaths': 17,
                    'hltv_url_path': '/stats/players/11496/InJect', 'hltv_external_id': 11496
                },
                {
                    'team_details': {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293},
                    'first_name': 'James', 'alias': 'James', 'last_name': 'Quinn',
                    'kills': 11, 'deaths': 19,
                    'hltv_url_path': '/stats/players/8746/James', 'hltv_external_id': 8746
                },
                {
                    'team_details': {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293},
                    'first_name': 'Alec', 'alias': 'Noisia', 'last_name': 'Gulabovski',
                    'kills': 10, 'deaths': 17,
                    'hltv_url_path': '/stats/players/11492/Noisia', 'hltv_external_id': 11492
                },
                {
                    'team_details': {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293},
                    'first_name': 'Raymond', 'alias': 'Dezibel', 'last_name': 'Odisho',
                    'kills': 6, 'deaths': 17,
                    'hltv_url_path': '/stats/players/11494/Dezibel', 'hltv_external_id': 11494
                },
                {
                    'team_details': {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293},
                    'first_name': 'Georgio', 'alias': 'sK', 'last_name': 'Poumpoulidis',
                    'kills': 6, 'deaths': 18,
                    'hltv_url_path': '/stats/players/11493/sK', 'hltv_external_id': 11493
                }
            ]
        }
    ]
}

SERIES_BEST_OF_THREE_RESULT = {
    'is_valid': True,
    'length': 3,
    'start_time': datetime(2017, 11, 15, 20, 0, tzinfo=timezone.get_default_timezone()),
    'winning_team_details': {
        'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
    },
    'tournament_name': 'Skinhub Season 2',
    'teams': [
        {'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713},
        {'name': 'Red Reserve', 'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613}
    ],
    'games': [
        {
            'order': 0,
            'map_name': 'Overpass',
            'winning_team_details': {
                'name': 'Red Reserve',
                'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
            },
            'players': [
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Frederik', 'alias': 'acoR', 'last_name': 'Gyldstrand',
                    'kills': 20, 'deaths': 18,
                    'hltv_url_path': '/stats/players/10330/acoR', 'hltv_external_id': 10330
                }, {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Dennis', 'alias': 'sycrone', 'last_name': 'Nielsen',
                    'kills': 12, 'deaths': 19,
                    'hltv_url_path': '/stats/players/9295/sycrone', 'hltv_external_id': 9295
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Daniel', 'alias': 'mertz', 'last_name': 'Mertz',
                    'kills': 10, 'deaths': 17,
                    'hltv_url_path': '/stats/players/9895/mertz', 'hltv_external_id': 9895
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Johannes', 'alias': 'b0RUP', 'last_name': 'Borup',
                    'kills': 8, 'deaths': 18,
                    'hltv_url_path': '/stats/players/9896/b0RUP', 'hltv_external_id': 9896
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Nicklas', 'alias': 'gade', 'last_name': 'Gade',
                    'kills': 9, 'deaths': 20,
                    'hltv_url_path': '/stats/players/8875/gade', 'hltv_external_id': 8875
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Daniel', 'alias': 'djL', 'last_name': 'Narancic',
                    'kills': 20, 'deaths': 10,
                    'hltv_url_path': '/stats/players/12572/djL', 'hltv_external_id': 12572
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Fredrik', 'alias': 'FREDDyFROG', 'last_name': 'Gustafsson',
                    'kills': 20, 'deaths': 7,
                    'hltv_url_path': '/stats/players/9277/FREDDyFROG', 'hltv_external_id': 9277
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Joakim', 'alias': 'Relaxa', 'last_name': 'Gustafsson',
                    'kills': 18, 'deaths': 13,
                    'hltv_url_path': '/stats/players/9279/Relaxa', 'hltv_external_id': 9279
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Niclas', 'alias': 'PlesseN', 'last_name': 'Plessen',
                    'kills': 15, 'deaths': 13,
                    'hltv_url_path': '/stats/players/9811/PlesseN', 'hltv_external_id': 9811
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Linus', 'alias': 'Bååten', 'last_name': 'Andersson',
                    'kills': 18, 'deaths': 16,
                    'hltv_url_path': '/stats/players/10463/B%C3%A5%C3%A5ten', 'hltv_external_id': 10463
                }
            ]
        },
        {
            'order': 1,
            'map_name': 'Inferno',
            'winning_team_details': {
                'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
            },
            'players': [
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Daniel', 'alias': 'mertz', 'last_name': 'Mertz',
                    'kills': 33, 'deaths': 23,
                    'hltv_url_path': '/stats/players/9895/mertz', 'hltv_external_id': 9895
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Frederik', 'alias': 'acoR', 'last_name': 'Gyldstrand',
                    'kills': 20, 'deaths': 20,
                    'hltv_url_path': '/stats/players/10330/acoR', 'hltv_external_id': 10330
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Dennis', 'alias': 'sycrone', 'last_name': 'Nielsen',
                    'kills': 20, 'deaths': 23,
                    'hltv_url_path': '/stats/players/9295/sycrone', 'hltv_external_id': 9295
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Nicklas', 'alias': 'gade', 'last_name': 'Gade',
                    'kills': 27, 'deaths': 26,
                    'hltv_url_path': '/stats/players/8875/gade', 'hltv_external_id': 8875
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Johannes', 'alias': 'b0RUP', 'last_name': 'Borup',
                    'kills': 22, 'deaths': 25,
                    'hltv_url_path': '/stats/players/9896/b0RUP', 'hltv_external_id': 9896
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Linus', 'alias': 'Bååten', 'last_name': 'Andersson',
                    'kills': 28, 'deaths': 24,
                    'hltv_url_path': '/stats/players/10463/B%C3%A5%C3%A5ten', 'hltv_external_id': 10463
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Niclas', 'alias': 'PlesseN', 'last_name': 'Plessen',
                    'kills': 25, 'deaths': 26,
                    'hltv_url_path': '/stats/players/9811/PlesseN', 'hltv_external_id': 9811
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Joakim', 'alias': 'Relaxa', 'last_name': 'Gustafsson',
                    'kills': 24, 'deaths': 24,
                    'hltv_url_path': '/stats/players/9279/Relaxa', 'hltv_external_id': 9279
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Fredrik', 'alias': 'FREDDyFROG', 'last_name': 'Gustafsson',
                    'kills': 22, 'deaths': 21,
                    'hltv_url_path': '/stats/players/9277/FREDDyFROG', 'hltv_external_id': 9277
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Daniel', 'alias': 'djL', 'last_name': 'Narancic',
                    'kills': 18, 'deaths': 27,
                    'hltv_url_path': '/stats/players/12572/djL', 'hltv_external_id': 12572
                }
            ]
        },
        {
            'order': 2,
            'map_name': 'Train',
            'winning_team_details': {
                'name': 'North Academy',
                'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
            },
            'players': [
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Nicklas', 'alias': 'gade', 'last_name': 'Gade',
                    'kills': 22, 'deaths': 10,
                    'hltv_url_path': '/stats/players/8875/gade', 'hltv_external_id': 8875
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Daniel', 'alias': 'mertz', 'last_name': 'Mertz',
                    'kills': 17, 'deaths': 12,
                    'hltv_url_path': '/stats/players/9895/mertz', 'hltv_external_id': 9895
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Dennis', 'alias': 'sycrone', 'last_name': 'Nielsen',
                    'kills': 12, 'deaths': 10,
                    'hltv_url_path': '/stats/players/9295/sycrone', 'hltv_external_id': 9295
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Frederik', 'alias': 'acoR', 'last_name': 'Gyldstrand',
                    'kills': 15, 'deaths': 13,
                    'hltv_url_path': '/stats/players/10330/acoR', 'hltv_external_id': 10330
                },
                {
                    'team_details': {
                        'name': 'North Academy',
                        'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713
                    },
                    'first_name': 'Johannes', 'alias': 'b0RUP', 'last_name': 'Borup',
                    'kills': 11, 'deaths': 15,
                    'hltv_url_path': '/stats/players/9896/b0RUP', 'hltv_external_id': 9896
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Fredrik', 'alias': 'FREDDyFROG', 'last_name': 'Gustafsson',
                    'kills': 17, 'deaths': 16,
                    'hltv_url_path': '/stats/players/9277/FREDDyFROG', 'hltv_external_id': 9277
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Daniel', 'alias': 'djL', 'last_name': 'Narancic',
                    'kills': 10, 'deaths': 12,
                    'hltv_url_path': '/stats/players/12572/djL', 'hltv_external_id': 12572
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Joakim', 'alias': 'Relaxa', 'last_name': 'Gustafsson',
                    'kills': 12, 'deaths': 14,
                    'hltv_url_path': '/stats/players/9279/Relaxa', 'hltv_external_id': 9279
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Linus', 'alias': 'Bååten', 'last_name': 'Andersson',
                    'kills': 12, 'deaths': 18,
                    'hltv_url_path': '/stats/players/10463/B%C3%A5%C3%A5ten', 'hltv_external_id': 10463
                },
                {
                    'team_details': {
                        'name': 'Red Reserve',
                        'hltv_url_path': '/team/7613/red-reserve', 'hltv_external_id': 7613
                    },
                    'first_name': 'Niclas', 'alias': 'PlesseN', 'last_name': 'Plessen',
                    'kills': 8, 'deaths': 17,
                    'hltv_url_path': '/stats/players/9811/PlesseN', 'hltv_external_id': 9811
                }
            ]
        }
    ]
}

SERIES_BEST_OF_ONE_UPCOMING = {
    'is_valid': True,
    'length': 1,
    'start_time': datetime(2017, 11, 18, 16, 15, tzinfo=timezone.get_default_timezone()),
    'tournament_name': 'Gfinity Elite Series Season 2',
    'teams': [
        {'name': 'Infused', 'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554},
        {'name': 'Reason', 'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208}
    ],
    'games': [
        {
            'order': 0,
            'map_name': None,
            'players': [
                {
                    'team_details': {
                        'name': 'Infused',
                        'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                    },
                    'first_name': 'Jake', 'alias': 'batham', 'last_name': 'Batham',
                    'hltv_url_path': '/stats/players/7291/batham', 'hltv_external_id': 7291
                },
                {
                    'team_details': {
                        'name': 'Infused',
                        'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                    },
                    'first_name': 'Travis', 'alias': 'L1NK', 'last_name': 'Mendoza',
                    'hltv_url_path': '/stats/players/14527/L1NK', 'hltv_external_id': 14527
                },
                {
                    'team_details': {
                        'name': 'Infused',
                        'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                    },
                    'first_name': 'Aaron', 'alias': 'frei', 'last_name': 'Frei',
                    'hltv_url_path': '/stats/players/11467/frei', 'hltv_external_id': 11467
                },
                {
                    'team_details': {
                        'name': 'Infused',
                        'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                    },
                    'first_name': 'George', 'alias': 'ZED', 'last_name': 'Bear',
                    'hltv_url_path': '/stats/players/7162/ZED', 'hltv_external_id': 7162
                },
                {
                    'team_details': {
                        'name': 'Infused',
                        'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                    },
                    'first_name': 'Zaki', 'alias': 'Danceyz', 'last_name': 'Dance',
                    'hltv_url_path': '/stats/players/14402/Danceyz', 'hltv_external_id': 14402
                },
                {
                    'team_details': {
                        'name': 'Reason',
                        'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                    },
                    'first_name': 'Oliver', 'alias': 'minet', 'last_name': 'Minet',
                    'hltv_url_path': '/stats/players/543/minet', 'hltv_external_id': 543
                },
                {
                    'team_details': {
                        'name': 'Reason',
                        'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                    },
                    'first_name': 'Thomas', 'alias': 'haste', 'last_name': 'Dyrensborg',
                    'hltv_url_path': '/stats/players/3545/haste', 'hltv_external_id': 3545
                },
                {
                    'team_details': {
                        'name': 'Reason',
                        'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                    },
                    'first_name': 'Kevin', 'alias': 'KS', 'last_name': 'Svenningsen',
                    'hltv_url_path': '/stats/players/7408/KS', 'hltv_external_id': 7408
                },
                {
                    'team_details': {
                        'name': 'Reason',
                        'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                    },
                    'first_name': 'Sebastian', 'alias': 'Basso', 'last_name': 'Aagaard',
                    'hltv_url_path': '/stats/players/10443/Basso', 'hltv_external_id': 10443
                },
                {
                    'team_details': {
                        'name': 'Reason',
                        'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                    },
                    'first_name': 'Rene', 'alias': 'TeSeS', 'last_name': 'Madsen',
                    'hltv_url_path': '/stats/players/12018/TeSeS', 'hltv_external_id': 12018
                }
            ]
        }
    ]
}

SERIES_BEST_OF_THREE_UPCOMING = {
    'is_valid': True,
    'length': 3,
    'start_time': datetime(2017, 11, 18, 21, 0, tzinfo=timezone.get_default_timezone()),
    'tournament_name': 'IEM Oakland 2017',
    'teams': [
        {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
        {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411}
    ],
    'games': [
        {
            'order': 0,
            'map_name': None,
            'players': [
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo',
                    'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Fernando', 'alias': 'fer', 'last_name': 'Alvarenga',
                    'hltv_url_path': '/stats/players/8564/fer', 'hltv_external_id': 8564
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Marcelo', 'alias': 'coldzera', 'last_name': 'David',
                    'hltv_url_path': '/stats/players/9216/coldzera', 'hltv_external_id': 9216
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Epitacio', 'alias': 'TACO', 'last_name': 'de Melo',
                    'hltv_url_path': '/stats/players/9217/TACO', 'hltv_external_id': 9217
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Ricardo', 'alias': 'boltz', 'last_name': 'Prass',
                    'hltv_url_path': '/stats/players/8568/boltz', 'hltv_external_id': 8568
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg',
                    'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                },
                {
                    'team_details': {
                        'name': 'NiP',
                        'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                    },
                    'first_name': 'Christopher', 'alias': 'GeT_RiGhT', 'last_name': 'Alesund',
                    'hltv_url_path': '/stats/players/39/GeT_RiGhT', 'hltv_external_id': 39
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Richard', 'alias': 'Xizt', 'last_name': 'Landström',
                    'hltv_url_path': '/stats/players/884/Xizt', 'hltv_external_id': 884
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Fredrik', 'alias': 'REZ', 'last_name': 'Sterner',
                    'hltv_url_path': '/stats/players/9278/REZ', 'hltv_external_id': 9278
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'William', 'alias': 'draken', 'last_name': 'Sundin',
                    'hltv_url_path': '/stats/players/9255/draken', 'hltv_external_id': 9255
                }
            ]
        },
        {
            'order': 1,
            'map_name': None,
            'players': [
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo',
                    'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Fernando', 'alias': 'fer', 'last_name': 'Alvarenga',
                    'hltv_url_path': '/stats/players/8564/fer', 'hltv_external_id': 8564
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Marcelo', 'alias': 'coldzera', 'last_name': 'David',
                    'hltv_url_path': '/stats/players/9216/coldzera', 'hltv_external_id': 9216
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Epitacio', 'alias': 'TACO', 'last_name': 'de Melo',
                    'hltv_url_path': '/stats/players/9217/TACO', 'hltv_external_id': 9217
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Ricardo', 'alias': 'boltz', 'last_name': 'Prass',
                    'hltv_url_path': '/stats/players/8568/boltz', 'hltv_external_id': 8568
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg',
                    'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Christopher', 'alias': 'GeT_RiGhT', 'last_name': 'Alesund',
                    'hltv_url_path': '/stats/players/39/GeT_RiGhT', 'hltv_external_id': 39
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Richard', 'alias': 'Xizt', 'last_name': 'Landström',
                    'hltv_url_path': '/stats/players/884/Xizt', 'hltv_external_id': 884
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Fredrik', 'alias': 'REZ', 'last_name': 'Sterner',
                    'hltv_url_path': '/stats/players/9278/REZ', 'hltv_external_id': 9278
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'William', 'alias': 'draken', 'last_name': 'Sundin',
                    'hltv_url_path': '/stats/players/9255/draken', 'hltv_external_id': 9255
                }
            ]
        },
        {
            'order': 2,
            'map_name': None,
            'players': [
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo',
                    'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Fernando', 'alias': 'fer', 'last_name': 'Alvarenga',
                    'hltv_url_path': '/stats/players/8564/fer', 'hltv_external_id': 8564
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Marcelo', 'alias': 'coldzera', 'last_name': 'David',
                    'hltv_url_path': '/stats/players/9216/coldzera', 'hltv_external_id': 9216
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Epitacio', 'alias': 'TACO', 'last_name': 'de Melo',
                    'hltv_url_path': '/stats/players/9217/TACO', 'hltv_external_id': 9217
                },
                {
                    'team_details': {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                    'first_name': 'Ricardo', 'alias': 'boltz', 'last_name': 'Prass',
                    'hltv_url_path': '/stats/players/8568/boltz', 'hltv_external_id': 8568
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg',
                    'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Christopher', 'alias': 'GeT_RiGhT', 'last_name': 'Alesund',
                    'hltv_url_path': '/stats/players/39/GeT_RiGhT', 'hltv_external_id': 39
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Richard', 'alias': 'Xizt', 'last_name': 'Landström',
                    'hltv_url_path': '/stats/players/884/Xizt', 'hltv_external_id': 884
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'Fredrik', 'alias': 'REZ', 'last_name': 'Sterner',
                    'hltv_url_path': '/stats/players/9278/REZ', 'hltv_external_id': 9278
                },
                {
                    'team_details': {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411},
                    'first_name': 'William', 'alias': 'draken', 'last_name': 'Sundin',
                    'hltv_url_path': '/stats/players/9255/draken', 'hltv_external_id': 9255
                }
            ]
        }
    ]
}

SERIES_UPCOMING_URL_PATHS = [
    '/matches/2317374/grayhound-vs-tainted-minds-kings-winner-zen-league-season-2-finals',
    '/matches/2317400/k1ck-vs-hexagone-moche-lpgo',
    '/matches/2317384/infused-vs-reason-gfinity-elite-series-season-2',
    '/matches/2317401/alientech-vs-k1ck-hexagone-winner-moche-lpgo',
    '/matches/2317385/endpoint-vs-prophecy-gfinity-elite-series-season-2',
    '/matches/2317386/excel-vs-method-gfinity-elite-series-season-2',
    '/matches/2317387/epsilon-vs-envyus-academy-gfinity-elite-series-season-2',
    '/matches/2317295/sk-vs-nip-iem-oakland-2017',
    '/matches/2317296/faze-vs-cloud9-iem-oakland-2017',
    '/matches/2317415/chiefs-vs-vici-wegl-2017-super-fight-invitational',
    '/matches/2317413/tyloo-vs-vega-squadron-china-top-2017-shenzhen',
    '/matches/2317414/flash-vs-ago-china-top-2017-shenzhen',
    '/matches/2317416/tainted-minds-vs-grayhound-esea-mdl-season-26-australia',
    '/matches/2317234/defusekids-vs-oserv-esl-benelux-winter-championship-2017',
    '/matches/2317391/tricked-vs-pro100-mother-russia-2',
    '/matches/2317313/havu-vs-hasselsnok-nordic-championship-season-2',
    '/matches/2317358/singularity-vs-superjymy-x-betco-invitational',
    '/matches/2317297/iem-oakland-2017-grand-final-iem-oakland-2017',
    '/matches/2317316/beacon-vs-samurai-cevo-main-season-13-north-america',
    '/matches/2317382/spirit-academy-vs-epg-vreecase-cup-2',
    '/matches/2317359/gameagents-vs-windigo-x-betco-invitational',
    '/matches/2317390/windigo-vs-nexus-mother-russia-2',
    '/matches/2317383/excel-vs-passions-vreecase-cup-2',
    '/matches/2317315/soar-vs-rise-nation-cevo-main-season-13-north-america',
    '/matches/2317129/lfao-vs-gale-force-cevo-main-season-13-north-america',
    '/matches/2317403/hyper-vs-good-people-cevo-main-season-13-north-america',
    '/matches/2317392/adaptation-vs-vulture-eoddset-power-fight-club-2',
    '/matches/2317393/main-street-vs-bbsp-eoddset-power-fight-club-2',
    '/matches/2317395/singularity-vs-envyus-academy-vreecase-cup-2',
    '/matches/2317207/nip-vs-virtuspro-ecs-season-4-europe',
    '/matches/2317208/virtuspro-vs-nip-ecs-season-4-europe',
    '/matches/2317394/hs-p-lager-vs-pyramid-gods-eoddset-power-fight-club-2',
    '/matches/2317209/fnatic-vs-virtuspro-ecs-season-4-europe',
    '/matches/2317210/virtuspro-vs-fnatic-ecs-season-4-europe',
    '/matches/2317327/renegades-vs-nrg-ecs-season-4-north-america',
    '/matches/2317328/nrg-vs-renegades-ecs-season-4-north-america',
    '/matches/2317211/astralis-vs-nip-ecs-season-4-europe',
    '/matches/2317212/nip-vs-astralis-ecs-season-4-europe',
    '/matches/2317213/astralis-vs-godsent-ecs-season-4-europe',
    '/matches/2317214/godsent-vs-astralis-ecs-season-4-europe',
    '/matches/2317215/mousesports-vs-envyus-ecs-season-4-europe',
    '/matches/2317216/envyus-vs-mousesports-ecs-season-4-europe',
    '/matches/2316765/astralis-vs-sk-blast-pro-series-copenhagen-2017',
    '/matches/2316766/faze-vs-nip-blast-pro-series-copenhagen-2017',
    '/matches/2316767/north-vs-g2-blast-pro-series-copenhagen-2017',
    '/matches/2316768/faze-vs-north-blast-pro-series-copenhagen-2017',
    '/matches/2316769/sk-vs-g2-blast-pro-series-copenhagen-2017',
    '/matches/2316770/astralis-vs-nip-blast-pro-series-copenhagen-2017',
    '/matches/2316771/astralis-vs-north-blast-pro-series-copenhagen-2017',
    '/matches/2316772/nip-vs-sk-blast-pro-series-copenhagen-2017',
    '/matches/2316773/g2-vs-faze-blast-pro-series-copenhagen-2017',
    '/matches/2316774/nip-vs-north-blast-pro-series-copenhagen-2017',
    '/matches/2316775/sk-vs-faze-blast-pro-series-copenhagen-2017',
    '/matches/2316776/astralis-vs-g2-blast-pro-series-copenhagen-2017',
    '/matches/2316777/north-vs-sk-blast-pro-series-copenhagen-2017',
    '/matches/2316778/astralis-vs-faze-blast-pro-series-copenhagen-2017',
    '/matches/2316779/g2-vs-nip-blast-pro-series-copenhagen-2017',
    '/matches/2316780/blast-pro-stand-off-1-blast-pro-series-copenhagen-2017',
    '/matches/2316781/blast-pro-stand-off-2-blast-pro-series-copenhagen-2017',
    '/matches/2316782/blast-pro-series-grand-final-blast-pro-series-copenhagen-2017'
]

SERIES_RESULT_URL_PATHS = [
    '/matches/2317402/reak-vs-set-the-rules-wesg-2017-southern-south-america',
    '/matches/2317411/luccini-vs-jaguar-omen-strike-one-2017',
    '/matches/2317410/stormborns-vs-vault-omen-strike-one-2017',
    '/matches/2317409/envyus-academy-vs-epg-legend-series-4',
    '/matches/2317381/kon-sweden-vs-kon-denmark-king-of-nordic-season-9',
    '/matches/2317408/epg-vs-spirit-legend-series-4',
    '/matches/2317405/vault-vs-jaguar-omen-strike-one-2017',
    '/matches/2317407/envyus-academy-vs-spirit-legend-series-4',
    '/matches/2317380/kon-denmark-vs-kon-norway-king-of-nordic-season-9',
    '/matches/2317406/singularity-vs-epg-legend-series-4',
    '/matches/2317404/stormborns-vs-luccini-omen-strike-one-2017',
    '/matches/2317379/kon-finland-vs-kon-norway-king-of-nordic-season-9',
    '/matches/2317233/spirit-vs-epg-legend-series-4',
    '/matches/2317363/nexus-vs-next-generation-cevo-main-season-13-europe',
    '/matches/2317232/singularity-vs-envyus-academy-legend-series-4',
    '/matches/2317372/kings-vs-mvp-pk-zen-league-season-2-finals',
    '/matches/2317371/tainted-minds-vs-grayhound-zen-league-season-2-finals',
    '/matches/2317376/vici-vs-kinguin-wegl-2017-super-fight-invitational',
    '/matches/2317370/mvp-pk-vs-grayhound-zen-league-season-2-finals',
    '/matches/2317317/french-canadians-vs-ex-naventic-cevo-main-season-13-north-america',
    '/matches/2317399/hafnet-vs-set-the-rules-wesg-2017-southern-south-america',
    '/matches/2317318/hyper-vs-gorilla-core-cevo-main-season-13-north-america',
    '/matches/2317319/good-people-vs-tempo-storm-cevo-main-season-13-north-america',
    '/matches/2317375/chiefs-vs-ardeont-academy-wegl-2017-super-fight-invitational',
    '/matches/2317366/gx-vs-ghost-academy-esea-mdl-season-26-north-america',
    '/matches/2317369/kings-vs-tainted-minds-zen-league-season-2-finals',
    '/matches/2317365/evilvice-vs-reak-wesg-2017-southern-south-america',
    '/matches/2317294/cloud9-vs-gambit-iem-oakland-2017',
    '/matches/2317350/pact-vs-conquer-cevo-main-season-13-europe',
    '/matches/2317351/nice-vs-orion-cevo-main-season-13-europe',
    '/matches/2317320/team5-vs-pride-skinhub-season-2',
    '/matches/2317398/windigo-vs-wasd-legend-series-4',
    '/matches/2317352/bpro-vs-x6tence-cevo-main-season-13-europe',
    '/matches/2317354/alternate-attax-vs-gameagents-mother-russia-2',
    '/matches/2317293/sk-vs-optic-iem-oakland-2017',
    '/matches/2317397/nexus-vs-wasd-legend-series-4',
    '/matches/2317389/windigo-vs-wasd-legend-series-4',
    '/matches/2317364/virtuspro-vs-space-soldiers-esea-mdl-season-26-europe',
    '/matches/2317312/acapella-vs-havu-nordic-championship-season-2',
    '/matches/2317378/k1ck-vs-alientech-moche-lpgo',
    '/matches/2317388/venatores-vs-nexus-legend-series-4',
    '/matches/2317231/nexus-vs-wasd-legend-series-4',
    '/matches/2317377/hexagone-cubo-vs-hexagone-moche-lpgo',
    '/matches/2317362/havu-vs-fsnus-nordic-championship-season-2',
    '/matches/2317230/venatores-vs-windigo-legend-series-4',
    '/matches/2317360/envyus-academy-vs-goodjob-mother-russia-2',
    '/matches/2317329/seed-vs-ldlc-makemybet-championship-1',
    '/matches/2317356/seal-vs-forze-x-betco-invitational',
    '/matches/2317357/alientech-vs-hexagone-moche-lpgo',
    '/matches/2317347/ground-zero-vs-masterminds-cybergamer-intermediate-season-12-finals',
    '/matches/2317355/k1ck-vs-hexagone-cubo-moche-lpgo',
    '/matches/2317367/seadoggs-vs-corvidae-esea-mdl-season-26-australia',
    '/matches/2317368/dark-sided-vs-noxide-esea-mdl-season-26-australia',
    '/matches/2317349/gx-vs-rise-nation-esea-mdl-season-26-north-america',
    '/matches/2317291/g2-vs-optic-iem-oakland-2017',
    '/matches/2317292/renegades-vs-gambit-iem-oakland-2017',
    '/matches/2317348/soar-vs-ghost-academy-esea-mdl-season-26-north-america',
    '/matches/2317290/liquid-vs-faze-iem-oakland-2017',
    '/matches/2317353/vault-vs-hadouken-wesg-2017-northern-south-america',
    '/matches/2317288/g2-vs-liquid-iem-oakland-2017',
    '/matches/2317289/optic-vs-renegades-iem-oakland-2017',
    '/matches/2317287/optic-vs-liquid-iem-oakland-2017',
    '/matches/2317286/faze-vs-gambit-iem-oakland-2017',
    '/matches/2317168/north-academy-vs-red-reserve-skinhub-season-2',
    '/matches/2317337/virtuspro-vs-ago-esea-mdl-season-26-europe',
    '/matches/2317284/envyus-vs-nip-iem-oakland-2017'
]

TOURNAMENT_UPCOMING = {
    'is_valid': True,
    'name': 'Gamers Club Masters 2018 Northern Qualifier',
    'start_date': datetime(2018, 8, 11, 10, 0, tzinfo=timezone.get_default_timezone()),
    'end_date': datetime(2018, 8, 11, 10, 0, tzinfo=timezone.get_default_timezone()),
    'offline': True,
    'teams': [
        {'hltv_external_id': 9154, 'hltv_url_path': '/team/9154/depre', 'name': 'Depre'},
        {'hltv_external_id': 9225, 'hltv_url_path': '/team/9225/vitria', 'name': 'Vitória'}
    ]
}

TOURNAMENT_ONGOING = {
    'is_valid': True,
    'name': 'BravoBet Cup',
    'offline': False,
    'start_date': datetime(2018, 7, 20, 10, 0, tzinfo=timezone.get_default_timezone()),
    'end_date': datetime(2018, 7, 20, 10, 0, tzinfo=timezone.get_default_timezone()),
    'prize_money': Decimal('8000'),
    'prize_money_currency': 'USD',
    'teams': [
        {'hltv_external_id': 7489, 'hltv_url_path': '/team/7489/epg', 'name': 'EPG'},
        {'hltv_external_id': 9193, 'hltv_url_path': '/team/9193/plink', 'name': 'PLINK'},
        {'hltv_external_id': 8020, 'hltv_url_path': '/team/8020/vexed', 'name': 'Vexed'},
        {'hltv_external_id': 9183, 'hltv_url_path': '/team/9183/winstrike', 'name': 'Winstrike'},
        {'hltv_external_id': 8135, 'hltv_url_path': '/team/8135/forze', 'name': 'forZe'},
        {'hltv_external_id': 4469, 'hltv_url_path': '/team/4469/k1ck', 'name': 'k1ck'},
        {'hltv_external_id': 7898, 'hltv_url_path': '/team/7898/pro100', 'name': 'pro100'},
        {'hltv_external_id': 8846, 'hltv_url_path': '/team/8846/x-kom', 'name': 'x-kom'}
    ]
}

TOURNAMENT_ARCHIVED = {
    'is_valid': True,
    'name': 'Europe Minor - FACEIT Major 2018',
    'start_date': datetime(2018, 7, 19, 10, 0, tzinfo=timezone.get_default_timezone()),
    'end_date': datetime(2018, 7, 22, 10, 0, tzinfo=timezone.get_default_timezone()),
    'offline': True,
    'prize_money': Decimal('50000'),
    'prize_money_currency': 'USD',
    'teams': [
        {'hltv_external_id': 4914, 'hltv_url_path': '/team/4914/3dmax', 'name': '3DMAX'},
        {'hltv_external_id': 4869, 'hltv_url_path': '/team/4869/ence', 'name': 'ENCE'},
        {'hltv_external_id': 6134, 'hltv_url_path': '/team/6134/kinguin', 'name': 'Kinguin'},
        {'hltv_external_id': 9211, 'hltv_url_path': '/team/9211/leftout', 'name': 'LeftOut'},
        {'hltv_external_id': 4411, 'hltv_url_path': '/team/4411/nip', 'name': 'NiP'},
        {'hltv_external_id': 6615, 'hltv_url_path': '/team/6615/optic', 'name': 'OpTic'},
        {'hltv_external_id': 7613, 'hltv_url_path': '/team/7613/red-reserve', 'name': 'Red Reserve'},
        {'hltv_external_id': 8637, 'hltv_url_path': '/team/8637/sprout', 'name': 'Sprout'}
    ]
}

TOURNAMENT_NO_NAME = {
    'is_valid': False,
    'teams': [],
    'name': '',
    'start_date': datetime(1999, 11, 30, 11, 0, tzinfo=timezone.get_default_timezone()),
    'end_date': datetime(1999, 11, 30, 11, 0, tzinfo=timezone.get_default_timezone()),
    'prize_money': Decimal('832'),
    'prize_money_currency': 'USD',
    'offline': True
}

TOURNAMENT_ARCHIVED_URL_PATHS = [
    '/events/3378/challenge-of-the-gods-2017',
    '/events/3295/vip-adria-league-season-1-finals',
    '/events/2998/blast-pro-series-copenhagen-2017',
    '/events/3292/china-top-2017-shenzhen',
    '/events/3363/eoddset-power-fight-club-2',
    '/events/3367/streamme-gauntlet-cis-vs-eu-15',
    '/events/3357/daddyskins-western-league-qualifier',
    '/events/3369/nations-elite-esports-cup-european-qualifier',
    '/events/3368/omen-strike-one-2017',
    '/events/3355/wegl-2017-super-fight-invitational',
    '/events/3166/zen-league-season-2-finals',
    '/events/3358/moche-lpgo',
    '/events/3364/play2live-match'
]

TOURNAMENT_URL_PATHS = [
    '/events/3114/wesg-2017-europe-cis-regional-finals',
    '/events/3228/skinhub-season-2',
    '/events/3208/makemybet-championship-1',
    '/events/3333/mother-russia-2',
    '/events/3354/legend-series-4',
    '/events/3323/cevo-main-season-13-europe',
    '/events/3320/vreecase-cup-2',
    '/events/3322/cevo-main-season-13-north-america',
    '/events/3290/x-betco-invitational',
    '/events/3201/gfinity-elite-series-season-2',
    '/events/3350/cyberfest-2017',
    '/events/3377/alienware-liga-pro-gamers-club-nov-17',
    '/events/3224/fcdb-cup-2017',
    '/events/3312/king-of-nordic-season-9',
    '/events/3115/wesg-2017-europe-cis-regional-finals-female',
    '/events/3381/esl-south-east-europe-championship-season-6',
    '/events/3349/gext-2017-sea-finals',
    '/events/3378/challenge-of-the-gods-2017',
    '/events/3185/mobagame-cup-1',
    '/events/3336/wesg-2017-tw-hk-mo',
    '/events/3382/csgonet-cup-1',
    '/events/3371/nations-elite-esports-cup-iberian-qualifier',
    '/events/3370/nations-elite-esports-cup-nordic-qualifier',
    '/events/3356/daddyskins-western-league',
    '/events/3317/esea-open-season-26-south-africa',
    '/events/3380/esl-major-league-winter-2017',
    '/events/3319/esea-open-season-26-asia-pacific',
    '/events/2574/dreamhack-open-winter-2017',
    '/events/3072/esl-pro-league-season-6-finals',
    '/events/3313/ecs-season-4-finals',
    '/events/3351/wesg-2017-malaysia-lan',
    '/events/3275/nordic-championship-season-2-finals',
    '/events/3372/nations-elite-esports-cup-balkan-qualifier',
    '/events/3379/polish-esport-league-season-2',
    '/events/3318/esea-open-season-26-brazil',
    '/events/3315/esea-main-season-26-north-america',
    '/events/3283/rog-masters-2017-grand-finals',
    '/events/3217/australian-esports-masters-season-3-finals',
    '/events/3316/esea-main-season-26-europe',
    '/events/3289/nations-elite-esports-cup',
    '/events/2876/cybergamer-premier-league-championship-2017',
    '/events/3342/esports-balkan-league-season-1-finals',
    '/events/3248/eleague-major-2018-main-qualifier',
    '/events/3247/eleague-major-2018',
    '/events/3177/wesg-2017-asia-pacific-regional-finals-female',
    '/events/3176/wesg-2017-asia-pacific-regional-finals',
    '/events/3117/wesg-2017-americas-regional-finals-female',
    '/events/3116/wesg-2017-americas-regional-finals',
    '/events/3362/esl-pro-league-season-7-north-america',
    '/events/3361/esl-pro-league-season-7-europe',
    '/events/3309/iem-katowice-2018',
    '/events/3112/wesg-2017-world-finals',
    '/events/3113/wesg-2017-world-finals-female',
    '/events/3170/copenhagen-games-2017',
    '/events/3373/esl-pro-league-season-7-finals',
    '/events/3375/esl-pro-league-season-8-north-america',
    '/events/3374/esl-pro-league-season-8-europe',
    '/events/3376/esl-pro-league-season-8-finals'
]
