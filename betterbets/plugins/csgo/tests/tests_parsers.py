from pathlib import Path

from django.test import TestCase

from plugins.csgo.scrape import (  # Teams; Players; Series; Tournaments
    parse_archived_tournament_list, parse_player, parse_players_list,
    parse_series, parse_series_result_url_paths,
    parse_teams, parse_tournament, parse_tournament_url_paths,
    parse_upcoming_series_url_paths
)

from .fixtures.parse_results import (  # Teams; Players; Series; Tournaments
    PLAYER_DETAILS, PLAYER_DETAILS_AMPERSAND, PLAYER_URL_LIST,
    SERIES_BEST_OF_ONE_RESULT, SERIES_BEST_OF_ONE_UPCOMING,
    SERIES_BEST_OF_THREE_RESULT, SERIES_BEST_OF_THREE_UPCOMING,
    SERIES_RESULT_URL_PATHS, SERIES_UPCOMING_URL_PATHS,
    TEAM_RESULT, TOURNAMENT_ARCHIVED, TOURNAMENT_ARCHIVED_URL_PATHS,
    TOURNAMENT_NO_NAME, TOURNAMENT_ONGOING, TOURNAMENT_UPCOMING,
    TOURNAMENT_URL_PATHS
)


class TestTeamParser(TestCase):
    """Test HLTV team parser."""
    def setUp(self):
        html_file_path = Path(__file__).parent / 'fixtures' / 'teams.html'
        with open(html_file_path) as html_file:
            self.html = html_file.read()

    def test_parse_team_list(self):
        """Ensure team parser returns valid list."""
        self.assertListEqual(
            TEAM_RESULT,
            parse_teams(request_content=self.html)
        )


class TestPlayerParser(TestCase):
    """Test HLTV player parsers."""
    def setUp(self):
        self.test_dir_path = Path(__file__).parent / 'fixtures'

    def test_parse_player_list(self):
        """Ensure player list is valid."""
        with open(self.test_dir_path / 'players_list.html') as html_file:
            player_list_html = html_file.read()
        self.assertListEqual(
            PLAYER_URL_LIST,
            parse_players_list(request_content=player_list_html)
        )

    def test_parse_player_stats(self):
        """Ensure player starts are parsed properly."""
        with open(self.test_dir_path / 'player_details.html') as html_file:
            player_details_html = html_file.read()
        self.assertDictEqual(
            PLAYER_DETAILS,
            parse_player(request_content=player_details_html)
        )

        with open(self.test_dir_path / 'player_details_ampersand.html') as html_file:
            player_details_ampersand_html = html_file.read()
        self.assertDictEqual(
            PLAYER_DETAILS_AMPERSAND,
            parse_player(request_content=player_details_ampersand_html)
        )


class TestTournamentParser(TestCase):
    """Test HLTV tournament parsers."""
    def setUp(self):
        self.test_dir_path = Path(__file__).parent / 'fixtures'

    def test_parse_upcoming_tournament(self):
        """Ensure upcoming tournament page parses correctly."""
        with open(self.test_dir_path / 'tournament_upcoming.html') as html_file:
            tournament_html = html_file.read()
        self.assertDictEqual(
            TOURNAMENT_UPCOMING,
            parse_tournament(tournament_html)
        )

    def test_parse_ongoing_tournament(self):
        """Ensure ongoing tournament page parses correctly."""
        with open(self.test_dir_path / 'tournament_ongoing.html') as html_file:
            tournament_html = html_file.read()
        self.assertDictEqual(
            TOURNAMENT_ONGOING,
            parse_tournament(tournament_html)
        )

    def test_parse_archived_tournament(self):
        """Ensure archived tournament page parses correctly."""
        with open(self.test_dir_path / 'tournament_archived.html') as html_file:
            tournament_html = html_file.read()
        self.assertDictEqual(
            TOURNAMENT_ARCHIVED,
            parse_tournament(tournament_html)
        )

    def test_parse_tournament_no_name(self):
        """Ensure tournament a with blank name is not valid."""
        with open(self.test_dir_path / 'tournament_no_name.html') as html_file:
            tournament_no_name_html = html_file.read()
        self.assertDictEqual(
            TOURNAMENT_NO_NAME,
            parse_tournament(tournament_no_name_html)
        )

    def test_parse_archived_tournament_list(self):
        """Ensure archived tournament list page parses correctly."""
        with open(self.test_dir_path / 'tournament_archived_list.html') as html_file:
            tournament_list_html = html_file.read()
        self.assertListEqual(
            TOURNAMENT_ARCHIVED_URL_PATHS,
            parse_archived_tournament_list(tournament_list_html)
        )

    def test_parse_tournament_list(self):
        """Ensure tournament list page parses correctly."""
        with open(self.test_dir_path / 'tournament_list.html') as html_file:
            tournament_list_html = html_file.read()
        self.assertListEqual(
            TOURNAMENT_URL_PATHS,
            parse_tournament_url_paths(tournament_list_html)
        )


class TestSeriesParser(TestCase):
    """Test HLTV series parsers."""
    def setUp(self):
        self.test_dir_path = Path(__file__).parent / 'fixtures'

    def test_parse_best_of_one_result(self):
        """Ensure best of one results page parses correctly."""
        with open(self.test_dir_path / 'series_best_of_one_result.html') as html_file:
            series_html = html_file.read()
        self.assertDictEqual(
            SERIES_BEST_OF_ONE_RESULT,
            parse_series(request_content=series_html)
        )

    def test_parse_best_of_three_result(self):
        """Ensure best of three results page parses correctly."""
        with open(self.test_dir_path / 'series_best_of_three_result.html') as html_file:
            series_html = html_file.read()
        self.assertDictEqual(
            SERIES_BEST_OF_THREE_RESULT,
            parse_series(request_content=series_html)
        )

    def test_parse_best_of_one_upcoming(self):
        """Ensure best of one upcoming series page parse correctly."""
        with open(self.test_dir_path / 'series_best_of_one_upcoming.html') as html_file:
            series_html = html_file.read()
        self.assertDictEqual(
            SERIES_BEST_OF_ONE_UPCOMING,
            parse_series(request_content=series_html)
        )

    def test_parse_best_of_three_upcoming(self):
        """Ensure best of three upcoming series page parse correctly."""
        with open(self.test_dir_path / 'series_best_of_three_upcoming.html') as html_file:
            series_html = html_file.read()
        self.assertDictEqual(
            SERIES_BEST_OF_THREE_UPCOMING,
            parse_series(request_content=series_html)
        )

    def test_one_team_decided(self):
        """Ensure that if only one team is decided page parses correctly.

        Sometimes only one team is decided, the other team has the format
        "TeamB/TeamC".
        """
        with open(self.test_dir_path / 'series_one_team_undecided.html') as html_file:
            series_html = html_file.read()
        series_details = parse_series(series_html)
        self.assertListEqual(
            [
                {'name': '5POWER', 'hltv_url_path': '/team/7092/5power', 'hltv_external_id': 7092}
            ],
            series_details['teams']
        )

    def test_parse_upcoming_url_paths(self):
        """Ensure upcoming series list page parse correctly."""
        with open(self.test_dir_path / 'series_upcoming.html') as html_file:
            upcoming_matches_html = html_file.read()
        self.assertListEqual(
            SERIES_UPCOMING_URL_PATHS,
            parse_upcoming_series_url_paths(request_content=upcoming_matches_html)
        )

    def test_parse_result_url_paths(self):
        """Ensure series result list page parses correctly."""
        with open(self.test_dir_path / 'series_results.html') as html_file:
            upcoming_matches_html = html_file.read()
        self.assertListEqual(
            SERIES_RESULT_URL_PATHS,
            parse_series_result_url_paths(request_content=upcoming_matches_html)
        )
