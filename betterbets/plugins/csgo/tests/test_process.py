from datetime import datetime
from decimal import Decimal

from django.test import TestCase
from django.utils import timezone

from plugins.csgo.models import (
    CsgoGame, CsgoGamePlayerTeam, CsgoGameTeam, CsgoMap, CsgoPlayer,
    CsgoSeries, CsgoSeriesTeam, CsgoTeam, CsgoTournament
)


class TestTeamUpdateTask(TestCase):
    """Test updating/creating of teams via the update tasks."""
    def test_process_create_team(self):
        """Ensure teams can be created."""
        team_details = {
            'name': 'Bee\'s Money Crew',
            'hltv_url_path': '/team/9999/Bee%27s%20Money%20Crew',
            'hltv_external_id': 9999
        }
        process_details, created = CsgoTeam.update_or_create(details=team_details)
        team = CsgoTeam.objects.get(name='Bee\'s Money Crew')
        self.assertDictEqual(
            process_details,
            {
                'name': 'Bee\'s Money Crew',
                'hltv_url_path': '/team/9999/Bee%27s%20Money%20Crew',
                'hltv_external_id': 9999,
                'id': team.id
            }
        )
        self.assertTrue(created)
        self.assertEqual(team.id, 7)
        self.assertEqual(team.name, 'Bee\'s Money Crew')
        self.assertEqual(
            team.hltv_url_path,
            '/team/9999/Bee%27s%20Money%20Crew'
        )
        self.assertEqual(
            team.hltv_external_id,
            9999
        )

    def test_process_update_team(self):
        """Ensure teams are updated."""
        # Ensure teams don't exist to avoid integrity errors
        CsgoTeam.objects.all().delete()

        CsgoTeam.objects.create(
            name='dignitas',
            hltv_url_path='/team/5422/dignitas',
            hltv_external_id=5422
        )
        team_details = {
            'name': 'dignitas',
            'hltv_url_path': '/team/5422/dignitas-different',
            'hltv_external_id': 5422
        }
        process_details, created = CsgoTeam.update_or_create(team_details)
        self.assertFalse(created)
        self.assertDictEqual(
            process_details,
            {
                'id': 8,
                'name': 'dignitas',
                'hltv_url_path': '/team/5422/dignitas-different',
                'hltv_external_id': 5422,
                'changes': {
                    'hltv_url_path': {
                        'new': '/team/5422/dignitas-different',
                        'old': '/team/5422/dignitas'
                    }
                },
            }
        )


class TestPlayerUpdateTask(TestCase):
    """Test updating/creating of players via the update tasks."""
    def test_process_create_player(self):
        """Ensure players can be created."""
        player_details = {
            'alias': 'kennyS',
            'first_name': 'Kenny',
            'last_name': 'Schrub',
            'country': 'France',
            'hltv_url_path': '/stats/players/7167/kennyS',
            'hltv_external_id': 7167
        }
        process_details, created = CsgoPlayer.update_or_create(details=player_details)
        player = CsgoPlayer.objects.get(
            alias=player_details['alias'],
            first_name=player_details['first_name'],
            last_name=player_details['last_name']
        )

        self.assertDictEqual(
            process_details,
            {
                'alias': 'kennyS',
                'first_name': 'Kenny',
                'last_name': 'Schrub',
                'country': 'France',
                'hltv_url_path': '/stats/players/7167/kennyS',
                'hltv_external_id': 7167,
                'id': player.id
            }
        )
        self.assertTrue(created)
        self.assertEqual(player.alias, 'kennyS')
        self.assertEqual(player.first_name, 'Kenny')
        self.assertEqual(player.last_name, 'Schrub')
        self.assertEqual(player.country, 'France')
        self.assertEqual(player.hltv_url_path, '/stats/players/7167/kennyS')
        self.assertEqual(player.hltv_external_id, 7167)

    def test_process_update_player(self):
        """Ensure players are updated."""
        # Ensure players don't exist to avoid integrity errors
        CsgoPlayer.objects.all().delete()

        CsgoPlayer.objects.create(
            alias='kennyS',
            first_name='Kenny',
            last_name='Schrub',
            country='France',
            hltv_url_path='/stats/players/7167/kennyS',
            hltv_external_id=7167
        )
        player_details = {
            'alias': 'kennyS',
            'first_name': 'Kenny',
            'last_name': 'Schrub',
            'country': 'Germany',
            'hltv_url_path': '/stats/players/7167/kennyS-different',
            'hltv_external_id': 7167
        }
        process_details, created = CsgoPlayer.update_or_create(details=player_details)
        updated_player = CsgoPlayer.objects.get(hltv_external_id=7167)
        self.assertFalse(created)
        self.assertDictEqual(
            process_details,
            {
                'id': 6,
                'first_name': 'Kenny',
                'alias': 'kennyS',
                'last_name': 'Schrub',
                'country': 'Germany',
                'hltv_url_path': '/stats/players/7167/kennyS-different',
                'hltv_external_id': 7167,
                'changes': {
                    'country': {'old': 'France', 'new': 'Germany'},
                    'hltv_url_path': {
                        'old': '/stats/players/7167/kennyS',
                        'new': '/stats/players/7167/kennyS-different'
                    }
                }
            }
        )
        self.assertEqual(updated_player.alias, 'kennyS')
        self.assertEqual(updated_player.first_name, 'Kenny')
        self.assertEqual(updated_player.last_name, 'Schrub')
        self.assertEqual(updated_player.country, 'Germany')
        self.assertEqual(updated_player.hltv_url_path, '/stats/players/7167/kennyS-different')
        self.assertEqual(updated_player.hltv_external_id, 7167)


class TestTournamentUpdateTasks(TestCase):
    def setUp(self):
        """Ensure there are no teams or tournaments."""
        self.tournament_details = {
            'hltv_url_path': '/events/2574/dreamhack-open-winter-2017',
            'hltv_external_id': 2574,
            'is_valid': True,
            'name': 'DreamHack Open Winter 2017',
            'start_date': datetime(2017, 12, 1, 11, 0, tzinfo=timezone.get_default_timezone()),
            'end_date': datetime(2017, 12, 3, 11, 0, tzinfo=timezone.get_default_timezone()),
            'offline': True,
            'prize_money': Decimal('100000'),
            'prize_money_currency': 'USD',
            'teams': [
                {'name': 'BIG', 'hltv_url_path': '/team/7532/big', 'hltv_external_id': 7532},
                {'name': 'EnVyUs', 'hltv_url_path': '/team/5991/envyus', 'hltv_external_id': 5991},
                {'name': 'Gambit', 'hltv_url_path': '/team/6651/gambit', 'hltv_external_id': 6651},
                {'name': 'Heroic', 'hltv_url_path': '/team/7175/heroic', 'hltv_external_id': 7175},
                {'name': 'mousesports', 'hltv_url_path': '/team/4494/mousesports', 'hltv_external_id': 4494},
                {'name': 'Natus Vincere', 'hltv_url_path': '/team/4608/natus-vincere', 'hltv_external_id': 4608},
                {'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713},
                {'name': 'Rise Nation', 'hltv_url_path': '/team/7924/rise-nation', 'hltv_external_id': 7924}
            ]
        }

    def test_process_create_tournament(self):
        """Ensure tournament is created with the correct teams."""
        # Ensure teams and tournaments don't exist to avoid integrity errors
        CsgoTeam.objects.all().delete()
        CsgoTournament.objects.all().delete()

        CsgoTeam.objects.create(
            name='BIG',
            hltv_url_path='/team/7532/big',
            hltv_external_id=7532
        )

        process_details, created = CsgoTournament.update_or_create(
            details=self.tournament_details
        )
        self.assertTrue(created)
        self.assertDictEqual(
            process_details,
            {
                'id': 2,
                'hltv_url_path': '/events/2574/dreamhack-open-winter-2017',
                'hltv_external_id': 2574,
                'is_valid': True,
                'name': 'DreamHack Open Winter 2017',
                'start_date': datetime(2017, 12, 1, 11, 0, tzinfo=timezone.get_default_timezone()),
                'end_date': datetime(2017, 12, 3, 11, 0, tzinfo=timezone.get_default_timezone()),
                'offline': True,
                'prize_money': Decimal('100000'),
                'prize_money_currency': 'USD',
                'teams': [
                    {'name': 'BIG', 'hltv_url_path': '/team/7532/big', 'hltv_external_id': 7532, 'id': 9},
                    {'name': 'EnVyUs', 'hltv_url_path': '/team/5991/envyus', 'hltv_external_id': 5991, 'id': 10},
                    {'name': 'Gambit', 'hltv_url_path': '/team/6651/gambit', 'hltv_external_id': 6651, 'id': 11},
                    {'name': 'Heroic', 'hltv_url_path': '/team/7175/heroic', 'hltv_external_id': 7175, 'id': 12},
                    {
                        'name': 'mousesports', 'hltv_url_path': '/team/4494/mousesports',
                        'hltv_external_id': 4494, 'id': 13
                    },
                    {
                        'name': 'Natus Vincere', 'hltv_url_path': '/team/4608/natus-vincere',
                        'hltv_external_id': 4608, 'id': 14
                    },
                    {
                        'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy',
                        'hltv_external_id': 7713, 'id': 15
                    },
                    {
                        'name': 'Rise Nation', 'hltv_url_path': '/team/7924/rise-nation',
                        'hltv_external_id': 7924, 'id': 16
                    }
                ],
                'new_teams': [
                    {'name': 'EnVyUs', 'hltv_url_path': '/team/5991/envyus', 'hltv_external_id': 5991, 'id': 10},
                    {'name': 'Gambit', 'hltv_url_path': '/team/6651/gambit', 'hltv_external_id': 6651, 'id': 11},
                    {'name': 'Heroic', 'hltv_url_path': '/team/7175/heroic', 'hltv_external_id': 7175, 'id': 12},
                    {
                        'name': 'mousesports', 'hltv_url_path': '/team/4494/mousesports',
                        'hltv_external_id': 4494, 'id': 13
                    },
                    {
                        'name': 'Natus Vincere', 'hltv_url_path': '/team/4608/natus-vincere',
                        'hltv_external_id': 4608, 'id': 14
                    },
                    {
                        'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy',
                        'hltv_external_id': 7713, 'id': 15
                    },
                    {
                        'name': 'Rise Nation', 'hltv_url_path': '/team/7924/rise-nation',
                        'hltv_external_id': 7924, 'id': 16
                    }
                ]
            }
        )
        created_tournament = CsgoTournament.objects.get(id=process_details['id'])
        teams = CsgoTeam.objects.filter(
            name__in=[team_details['name'] for team_details in self.tournament_details['teams']]
        ).order_by('name')
        self.assertQuerysetEqual(
            created_tournament.teams.all().order_by('name'),
            [repr(team) for team in teams],
        )

    def test_process_update_tournament(self):
        """Ensure the tournament is correctly updated."""
        # Ensure teams and tournaments don't exist to avoid integrity errors
        CsgoTeam.objects.all().delete()
        CsgoTournament.objects.all().delete()

        tournament_details = self.tournament_details
        # Change a team's hltv_url_path to ensure the team is updated correctly
        tournament_details['teams'][1]['hltv_url_path'] = '/team/5991/different'
        tournament = CsgoTournament.objects.create(
            hltv_url_path='/events/2574/dreamhack-open-winter-2017',
            hltv_external_id=2574,
            name='DreamHack Open Winter 2017',
            start_date=datetime(2017, 12, 10, 11, 0, tzinfo=timezone.get_default_timezone()),
            end_date=datetime(2017, 12, 15, 11, 0, tzinfo=timezone.get_default_timezone()),
            offline=False,
            prize_money=Decimal('2000'),
            prize_money_currency='USD'
        )
        # Create tournament with one team missing
        teams_details = [
            {'name': 'EnVyUs', 'hltv_url_path': '/team/5991/envyus', 'hltv_external_id': 5991, 'id': 8},
            {'name': 'Gambit', 'hltv_url_path': '/team/6651/gambit', 'hltv_external_id': 6651, 'id': 9},
            {'name': 'Heroic', 'hltv_url_path': '/team/7175/heroic', 'hltv_external_id': 7175, 'id': 10},
            {'name': 'mousesports', 'hltv_url_path': '/team/4494/mousesports', 'hltv_external_id': 4494, 'id': 11},
            {'name': 'Natus Vincere', 'hltv_url_path': '/team/4608/natus-vincere', 'hltv_external_id': 4608, 'id': 12},
            {'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy', 'hltv_external_id': 7713, 'id': 13},
            {'name': 'Rise Nation', 'hltv_url_path': '/team/7924/rise-nation', 'hltv_external_id': 7924, 'id': 14}
        ]
        for team_details in teams_details:
            tournament.teams.add(CsgoTeam.objects.create(
                name=team_details['name'],
                hltv_url_path=team_details['hltv_url_path'],
                hltv_external_id=team_details['hltv_external_id']
            ))
        process_details, created = CsgoTournament.update_or_create(
            details=self.tournament_details
        )
        self.assertFalse(created)
        # Sorting the changes in teams in order to pass in both dev and CI
        process_details['changes']['teams']['new'] = sorted(process_details['changes']['teams']['new'])
        process_details['changes']['teams']['old'] = sorted(process_details['changes']['teams']['old'])
        self.assertDictEqual(
            process_details,
            {
                'id': 3,
                'hltv_url_path': '/events/2574/dreamhack-open-winter-2017',
                'hltv_external_id': 2574,
                'is_valid': True,
                'name': 'DreamHack Open Winter 2017',
                'start_date': datetime(2017, 12, 1, 11, 0, tzinfo=timezone.get_default_timezone()),
                'end_date': datetime(2017, 12, 3, 11, 0, tzinfo=timezone.get_default_timezone()),
                'offline': True,
                'prize_money': Decimal('100000'),
                'prize_money_currency': 'USD',
                'teams': [
                    {'name': 'BIG', 'hltv_url_path': '/team/7532/big', 'hltv_external_id': 7532, 'id': 24},
                    {
                        'id': 17,
                        'name': 'EnVyUs',
                        'hltv_url_path': '/team/5991/different',
                        'hltv_external_id': 5991,
                        'changes': {'hltv_url_path': {'old': '/team/5991/envyus', 'new': '/team/5991/different'}}
                    },
                    {
                        'name': 'Gambit', 'hltv_url_path': '/team/6651/gambit',
                        'hltv_external_id': 6651, 'id': 18
                    },
                    {
                        'name': 'Heroic', 'hltv_url_path': '/team/7175/heroic',
                        'hltv_external_id': 7175, 'id': 19
                    },
                    {
                        'name': 'mousesports', 'hltv_url_path': '/team/4494/mousesports',
                        'hltv_external_id': 4494, 'id': 20
                    },
                    {
                        'name': 'Natus Vincere', 'hltv_url_path': '/team/4608/natus-vincere',
                        'hltv_external_id': 4608, 'id': 21
                    },
                    {
                        'name': 'North Academy', 'hltv_url_path': '/team/7713/north-academy',
                        'hltv_external_id': 7713, 'id': 22
                    },
                    {
                        'name': 'Rise Nation', 'hltv_url_path': '/team/7924/rise-nation',
                        'hltv_external_id': 7924, 'id': 23
                    }
                ],
                'new_teams': [
                    {
                        'name': 'BIG', 'hltv_url_path': '/team/7532/big', 'hltv_external_id': 7532, 'id': 24}
                ],
                'changes': {
                    'start_date': {'old': '2017-12-10T11:00:00+00:00', 'new': '2017-12-01T11:00:00+00:00'},
                    'end_date': {'old': '2017-12-15T11:00:00+00:00', 'new': '2017-12-03T11:00:00+00:00'},
                    'prize_money': {'old': Decimal('2000.00'), 'new': Decimal('100000')},
                    'offline': {'old': False, 'new': True},
                    'teams': {
                        'new': [
                            '<CsgoTeam: BIG>',
                            '<CsgoTeam: EnVyUs>',
                            '<CsgoTeam: Gambit>',
                            '<CsgoTeam: Heroic>',
                            '<CsgoTeam: Natus Vincere>',
                            '<CsgoTeam: North Academy>',
                            '<CsgoTeam: Rise Nation>',
                            '<CsgoTeam: mousesports>'
                        ],
                        'old': [
                            '<CsgoTeam: EnVyUs>',
                            '<CsgoTeam: Gambit>',
                            '<CsgoTeam: Heroic>',
                            '<CsgoTeam: Natus Vincere>',
                            '<CsgoTeam: North Academy>',
                            '<CsgoTeam: Rise Nation>',
                            '<CsgoTeam: mousesports>'
                        ]
                    }
                },
            }
        )

        updated_tournament = CsgoTournament.objects.get(name=tournament_details['name'])
        self.assertEqual(updated_tournament.name, 'DreamHack Open Winter 2017')
        self.assertEqual(
            updated_tournament.start_date,
            datetime(2017, 12, 1, 11, 0, tzinfo=timezone.get_default_timezone())
        )
        self.assertEqual(
            updated_tournament.end_date,
            datetime(2017, 12, 3, 11, 0, tzinfo=timezone.get_default_timezone())
        )
        self.assertEqual(
            updated_tournament.prize_money,
            Decimal('100000')
        )

        teams = CsgoTeam.objects.filter(
            name__in=[team_details['name'] for team_details in tournament_details['teams']]
        ).order_by('name')
        self.assertQuerysetEqual(
            updated_tournament.teams.all().order_by('name'),
            [repr(team) for team in teams],
        )

    def test_process_tournament_invalid(self):
        """Ensure 'invalid' tournaments are not created."""
        original_num_of_tournaments = CsgoTournament.objects.all().count()
        CsgoTournament.update_or_create(details={
            'is_valid': False,
            'teams': [],
            'name': '',
            'start_date': datetime(1999, 11, 30, 11, 0, tzinfo=timezone.get_default_timezone()),
            'end_date': datetime(1999, 11, 30, 11, 0, tzinfo=timezone.get_default_timezone()),
            'offline': True,
            'hltv_url_path': '/events/2396/-',
            'hltv_external_id': 2396
        })
        self.assertEqual(
            original_num_of_tournaments,
            CsgoTournament.objects.all().count()
        )


class TestGameUpdateTask(TestCase):
    def test_process_create_game(self):
        """Ensure CSGO Game and all sub-objects are created."""
        # Ensure teams don't exist to avoid integrity errors
        CsgoTeam.objects.all().delete()

        team_1 = CsgoTeam.objects.create(
            id=100,
            name='Kings',
            hltv_url_path='/team/8021/kings',
            hltv_external_id=8021
        )
        team_2 = CsgoTeam.objects.create(
            id=101,
            name='AVANT',
            hltv_url_path='/team/5293/avant',
            hltv_external_id=5293
        )
        series = CsgoSeries.objects.create(
            length=1,
            winner=team_1,
            hltv_url_path='/matches/2320067/cloud9-vs-liquid-iem-katowice-2018',
            hltv_external_id=2320067
        )
        CsgoSeriesTeam.objects.create(series=series, team=team_1)
        CsgoSeriesTeam.objects.create(series=series, team=team_2)

        game_details = {
            'map_name': 'Mirage',
            'order': 0,
            'winning_team_details': {'name': 'Kings', 'hltv_url_path': '/team/8021/kings', 'hltv_external_id': 8021},
            'players': [
                {
                    'team_details': {
                        'name': 'Kings', 'hltv_url_path': '/team/8021/kings-different', 'hltv_external_id': 8021
                    },
                    'first_name': 'Jay', 'alias': 'liazz', 'last_name': 'Tregillgas',
                    'kills': '24', 'deaths': '9',
                    'hltv_url_path': '/stats/players/10588/liazz', 'hltv_external_id': 10588
                },
                {
                    'team_details': {'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293},
                    'first_name': 'Georgio', 'alias': 'sK', 'last_name': 'Poumpoulidis',
                    'kills': '6', 'deaths': '18',
                    'hltv_url_path': '/stats/players/11493/sK', 'hltv_external_id': 11493
                }
            ]
        }
        process_details, created = CsgoGame.update_or_create(game_details, series)
        created_game = CsgoGame.objects.get(id=process_details['id'])
        self.assertTrue(created)
        self.assertEqual(created_game.map, CsgoMap.objects.get(name='Mirage'))
        teams = CsgoTeam.objects.filter(name__in=['Kings', 'AVANT'])
        self.assertQuerysetEqual(
            created_game.teams.all(),
            [repr(team) for team in teams],
            ordered=False
        )
        hltv_url_paths = [player.hltv_url_path for player in created_game.players.all().order_by('alias')]
        self.assertEqual(hltv_url_paths, ['/stats/players/10588/liazz', '/stats/players/11493/sK'])
        hltv_external_ids = [player.hltv_external_id for player in created_game.players.all().order_by('alias')]
        self.assertEqual(hltv_external_ids, [10588, 11493])
        self.assertDictEqual(
            process_details,
            {
                'id': 1,
                'map_name': 'Mirage',
                'order': 0,
                'winning_team_details': {
                    'name': 'Kings', 'hltv_url_path': '/team/8021/kings',
                    'hltv_external_id': 8021
                },
                'players': [
                    {
                        'team_details': {
                            'name': 'Kings', 'hltv_url_path': '/team/8021/kings-different', 'hltv_external_id': 8021
                        },
                        'first_name': 'Jay', 'alias': 'liazz', 'last_name': 'Tregillgas',
                        'kills': '24', 'deaths': '9',
                        'hltv_url_path': '/stats/players/10588/liazz', 'hltv_external_id': 10588,
                        'id': 1
                    },
                    {
                        'team_details': {
                            'name': 'AVANT', 'hltv_url_path': '/team/5293/avant', 'hltv_external_id': 5293
                        },
                        'first_name': 'Georgio', 'alias': 'sK', 'last_name': 'Poumpoulidis',
                        'kills': '6', 'deaths': '18',
                        'hltv_url_path': '/stats/players/11493/sK', 'hltv_external_id': 11493,
                        'id': 2
                    }
                ],
                'new_players': [
                    {
                        'alias': 'liazz', 'first_name': 'Jay', 'last_name': 'Tregillgas', 'id': 1,
                        'hltv_url_path': '/stats/players/10588/liazz', 'hltv_external_id': 10588
                    },
                    {
                        'alias': 'sK', 'first_name': 'Georgio', 'last_name': 'Poumpoulidis', 'id': 2,
                        'hltv_url_path': '/stats/players/11493/sK', 'hltv_external_id': 11493
                    }
                ]
            }
        )

    def test_process_update_game(self):
        """Ensure CSGO Game and all sub-objects are updated."""
        # Ensure database is clean to avoid unexpected integrity errors
        CsgoTeam.objects.all().delete()
        CsgoGame.objects.all().delete()
        CsgoPlayer.objects.all().delete()

        # Create game to update
        team_1 = CsgoTeam.objects.create(
            name='Infused',
            hltv_url_path='/team/4554/infused',
            hltv_external_id=4554
        )
        team_2 = CsgoTeam.objects.create(
            name='Reason',
            hltv_url_path='/team/5208/reason',
            hltv_external_id=5208
        )
        series = CsgoSeries.objects.create(
            length=1,
            winner=None,
            hltv_url_path='/matches/2317384/infused-vs-reason-gfinity-elite-series-season-2',
            hltv_external_id=2317384
        )
        CsgoSeriesTeam.objects.create(series=series, team=team_1)
        CsgoSeriesTeam.objects.create(series=series, team=team_2)

        player_1 = CsgoPlayer.objects.create(
            first_name='Jake',
            alias='batham',
            last_name='Batham',
            hltv_url_path='/stats/players/7291/batham',
            hltv_external_id=7291
        )
        game = CsgoGame.objects.create(
            order=0,
            map=None,
            series=series
        )
        CsgoGameTeam.objects.create(game=game, team=team_1)
        CsgoGamePlayerTeam.objects.create(game=game, player=player_1, team=team_1)

        # New game details to override the existing game
        game_details = {
            'map_name': 'train',
            'order': 0,
            'winning_team_details': {
                'name': 'Infused', 'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
            },
            'players': [
                {
                    'team_details': {
                        'name': 'Infused', 'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                    },
                    'first_name': 'Jake', 'alias': 'batham', 'last_name': 'Batham',
                    'kills': 10, 'deaths': 5,
                    'hltv_url_path': '/stats/players/7291/batham', 'hltv_external_id': 7291
                },
                {
                    'team_details': {
                        'name': 'Reason', 'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                    },
                    'first_name': 'Oliver', 'alias': 'minet', 'last_name': 'Minet',
                    'kills': 12, 'deaths': 6,
                    'hltv_url_path': '/stats/players/543/minet', 'hltv_external_id': 543
                }
            ]
        }
        process_details, created = CsgoGame.update_or_create(game_details, series)
        updated_game = CsgoGame.objects.get(id=process_details['id'])
        self.assertEqual(updated_game.order, 0)
        self.assertEqual(updated_game.winner, team_1)
        self.assertEqual(updated_game.map, CsgoMap.objects.get(name='train'))

        teams = CsgoTeam.objects.filter(
            name__in=[player_details['team_details']['name'] for player_details in game_details['players']]
        ).order_by('name')
        self.assertQuerysetEqual(
            updated_game.teams.all().order_by('name'),
            [repr(team) for team in teams],
        )

        players = CsgoPlayer.objects.filter(
            alias__in=[player_details['alias'] for player_details in game_details['players']]
        ).order_by('alias')
        self.assertQuerysetEqual(
            updated_game.players.all().order_by('alias'),
            [repr(player) for player in players],
        )

        game_player_team_1 = CsgoGamePlayerTeam.objects.get(
            game=updated_game,
            player=player_1
        )
        self.assertEqual(game_player_team_1.kills, 10)
        self.assertEqual(game_player_team_1.deaths, 5)

        game_player_team_2 = CsgoGamePlayerTeam.objects.get(
            game=updated_game,
            player=CsgoPlayer.objects.get(alias='minet')
        )
        self.assertEqual(game_player_team_2.kills, 12)
        self.assertEqual(game_player_team_2.deaths, 6)

        self.assertFalse(created)
        self.assertDictEqual(
            process_details,
            {
                'id': 2,
                'map_name': 'train',
                'order': 0,
                'winning_team_details': {
                    'name': 'Infused', 'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                },
                'players': [
                    {
                        'id': 3,
                        'team_details': {
                            'name': 'Infused', 'hltv_url_path': '/team/4554/infused', 'hltv_external_id': 4554
                        },
                        'first_name': 'Jake', 'alias': 'batham', 'last_name': 'Batham',
                        'kills': 10, 'deaths': 5,
                        'hltv_url_path': '/stats/players/7291/batham', 'hltv_external_id': 7291,
                        'changes': {
                            'kills': {'old': None, 'new': 10},
                            'deaths': {'old': None, 'new': 5}
                        }
                    },
                    {
                        'id': 4,
                        'team_details': {
                            'name': 'Reason', 'hltv_url_path': '/team/5208/reason', 'hltv_external_id': 5208
                        },
                        'first_name': 'Oliver', 'alias': 'minet', 'last_name': 'Minet',
                        'kills': 12, 'deaths': 6,
                        'hltv_url_path': '/stats/players/543/minet', 'hltv_external_id': 543
                    }
                ],
                'changes': {
                    'winner': {'old': 'None', 'new': '<CsgoTeam: Infused>'},
                    'map': {'old': 'None', 'new': '<CsgoMap: train>'},
                    'teams': {
                        'old': ['<CsgoTeam: Infused>'],
                        'new': ['<CsgoTeam: Infused>', '<CsgoTeam: Reason>']
                    },
                    'players': {
                        'old': ['<CsgoPlayer: batham>'],
                        'new': ['<CsgoPlayer: batham>', '<CsgoPlayer: minet>']
                    }
                },
                'new_players': [
                    {
                        'alias': 'minet', 'first_name': 'Oliver', 'last_name': 'Minet', 'id': 4,
                        'hltv_url_path': '/stats/players/543/minet', 'hltv_external_id': 543
                    }
                ]
            }
        )


class TestSeriesUpdateTask(TestCase):
    def setUp(self):
        self.series_details = {
            'is_valid': True,
            'length': 3,
            'start_time': datetime(2017, 11, 18, 21, 0, tzinfo=timezone.get_default_timezone()),
            'tournament_name': 'IEM Oakland 2017',
            'hltv_url_path': '/matches/2317295/sk-vs-nip-iem-oakland-2017',
            'hltv_external_id': 2317295,
            'teams': [
                {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137},
                {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411}
            ],
            'games': [
                {
                    'map_name': None,
                    'order': 0,
                    'players': [
                        {
                            'team_details': {
                                'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137
                            },
                            'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo',
                            'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                        },
                        {
                            'team_details': {
                                'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                            },
                            'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg',
                            'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                        }
                    ]
                },
                {
                    'map_name': None,
                    'order': 1,
                    'players': [
                        {
                            'team_details': {
                                'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137
                            },
                            'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo',
                            'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                        },
                        {
                            'team_details': {
                                'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                            },
                            'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg',
                            'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                        }
                    ]
                },
            ]
        }

    def test_process_create_series(self):
        """Ensure CSGO series is being created."""
        # Ensure database is empty to avoid integrity errors
        CsgoSeries.objects.all().delete()
        CsgoPlayer.objects.all().delete()
        CsgoTeam.objects.all().delete()
        CsgoGame.objects.all().delete()

        process_details, created = CsgoSeries.update_or_create(
            details=self.series_details
        )

        created_series = CsgoSeries.objects.get(id=process_details['id'])
        self.assertEqual(created_series.hltv_url_path, '/matches/2317295/sk-vs-nip-iem-oakland-2017')
        self.assertEqual(created_series.length, 3)
        self.assertEqual(
            created_series.start_time,
            datetime(2017, 11, 18, 21, 0, tzinfo=timezone.get_default_timezone())
        )
        # No tournament due to no match on existing tournament
        self.assertEqual(created_series.tournament, None)

        # As games and teams are all deleted at the start of this test, so
        # the only games and teams left will be the ones created while
        # processing the series
        games = [repr(game) for game in CsgoGame.objects.all().order_by('id')]
        self.assertQuerysetEqual(
            created_series.games.all().order_by('id'),
            games
        )
        teams = [repr(team) for team in CsgoTeam.objects.all().order_by('id')]
        self.assertQuerysetEqual(
            created_series.teams.all().order_by('id'),
            teams
        )
        players = [repr(player) for player in CsgoPlayer.objects.all().order_by('id')]
        self.assertQuerysetEqual(
            created_series.games.all()[0].players.all().order_by('id'),
            players
        )
        player_1 = CsgoPlayer.objects.get(id=7)
        self.assertEqual(player_1.hltv_url_path, '/stats/players/2023/FalleN')
        self.assertEqual(player_1.hltv_external_id, 2023)
        player_2 = CsgoPlayer.objects.get(id=8)
        self.assertEqual(player_2.hltv_url_path, '/stats/players/29/f0rest')
        self.assertEqual(player_2.hltv_external_id, 29)

        self.assertTrue(created)
        self.assertDictEqual(
            process_details,
            {
                'id': 3,
                'is_valid': True,
                'length': 3,
                'start_time': datetime(2017, 11, 18, 21, 0, tzinfo=timezone.get_default_timezone()),
                'tournament_name': 'IEM Oakland 2017',
                'hltv_url_path': '/matches/2317295/sk-vs-nip-iem-oakland-2017',
                'hltv_external_id': 2317295,
                'teams': [
                    {'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137, 'id': 3},
                    {'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411, 'id': 4}
                ],
                'games': [
                    {
                        'map_name': None,
                        'order': 0,
                        'players': [
                            {
                                'team_details': {
                                    'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137
                                },
                                'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo', 'id': 5,
                                'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                            },
                            {
                                'team_details': {
                                    'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                                },
                                'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg', 'id': 6,
                                'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                            }
                        ],
                        'id': 3,
                        'new_players': [
                            {
                                'alias': 'FalleN', 'first_name': 'Gabriel', 'last_name': 'Toledo', 'id': 7,
                                'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                            },
                            {
                                'alias': 'f0rest', 'first_name': 'Patrik', 'last_name': 'Lindberg', 'id': 8,
                                'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                            }
                        ]
                    },
                    {
                        'map_name': None,
                        'order': 1,
                        'players': [
                            {
                                'team_details': {
                                    'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137
                                },
                                'first_name': 'Gabriel', 'alias': 'FalleN', 'last_name': 'Toledo', 'id': 7,
                                'hltv_url_path': '/stats/players/2023/FalleN', 'hltv_external_id': 2023
                            },
                            {
                                'team_details': {
                                    'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                                },
                                'first_name': 'Patrik', 'alias': 'f0rest', 'last_name': 'Lindberg', 'id': 8,
                                'hltv_url_path': '/stats/players/29/f0rest', 'hltv_external_id': 29
                            }
                        ],
                        'id': 4,
                        'new_players': []
                    }
                ]
            }
        )

    def test_process_update_series(self):
        """Ensure CSGO series is being updated"""
        # Ensure series doesn't exist to avoid integrity errors
        CsgoSeries.objects.all().delete()
        CsgoTeam.objects.all().delete()

        # Ensure teams and tournament exist
        teams = []
        for team_details in self.series_details['teams']:
            team, _ = CsgoTeam.objects.get_or_create(
                name=team_details['name'],
                hltv_external_id=team_details['hltv_external_id']
            )
            teams.append(team)
        tournament = CsgoTournament.objects.create(
            name=self.series_details['tournament_name'],
            start_date=self.series_details['start_time'],
            end_date=self.series_details['start_time'],
            hltv_url_path='/events/3378/challenge-of-the-gods-2017',
            hltv_external_id=3378,
            offline=False
        )

        series = CsgoSeries.objects.create(
            start_time=self.series_details['start_time'],
            hltv_url_path=self.series_details['hltv_url_path'],
            hltv_external_id=self.series_details['hltv_external_id'],
            tournament=tournament,
            winner=teams[1],
            length=2
        )
        CsgoSeriesTeam.objects.create(series=series, team=teams[1])

        process_details, created = CsgoSeries.update_or_create(details=self.series_details)
        updated_series = CsgoSeries.objects.get(id=process_details['id'])
        self.assertEqual(updated_series.hltv_url_path, '/matches/2317295/sk-vs-nip-iem-oakland-2017')
        self.assertEqual(updated_series.length, 3)
        self.assertEqual(
            updated_series.start_time,
            datetime(2017, 11, 18, 21, 0, tzinfo=timezone.get_default_timezone())
        )
        # No tournament due to no match on existing tournament
        self.assertEqual(updated_series.tournament, tournament)

        # As games and teams are all deleted at the start of this test, the
        # only games and teams left will be the ones created while
        # processing the series
        games = [repr(game) for game in CsgoGame.objects.all().order_by('id')]
        self.assertQuerysetEqual(
            updated_series.games.all().order_by('id'),
            games
        )
        teams = [repr(team) for team in CsgoTeam.objects.all().order_by('id')]
        self.assertQuerysetEqual(
            updated_series.teams.all().order_by('id'),
            teams
        )
        self.assertFalse(created)
        self.assertDictEqual(
            process_details,
            {
                'is_valid': True,
                'length': 3,
                'start_time': datetime(2017, 11, 18, 21, 0, tzinfo=timezone.get_default_timezone()),
                'tournament_name': 'IEM Oakland 2017',
                'hltv_url_path': '/matches/2317295/sk-vs-nip-iem-oakland-2017',
                'hltv_external_id': 2317295,
                'teams': [
                    {
                        'name': 'SK',
                        'hltv_url_path': '/team/6137/sk',
                        'hltv_external_id': 6137,
                        'changes': {
                            'hltv_url_path': {'old': None, 'new': '/team/6137/sk'}
                        },
                        'id': 5
                    },
                    {
                        'name': 'NiP',
                        'hltv_url_path': '/team/4411/nip',
                        'hltv_external_id': 4411,
                        'changes': {
                            'hltv_url_path': {'old': None, 'new': '/team/4411/nip'}
                        },
                        'id': 6
                    }
                ],
                'games': [
                    {
                        'map_name': None,
                        'order': 0,
                        'players': [
                            {
                                'team_details': {
                                    'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137
                                },
                                'first_name': 'Gabriel',
                                'alias': 'FalleN',
                                'last_name': 'Toledo',
                                'id': 9,
                                'hltv_external_id': 2023,
                                'hltv_url_path': '/stats/players/2023/FalleN'
                            },
                            {
                                'team_details': {
                                    'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                                },
                                'first_name': 'Patrik',
                                'alias': 'f0rest',
                                'last_name': 'Lindberg',
                                'id': 10,
                                'hltv_external_id': 29,
                                'hltv_url_path': '/stats/players/29/f0rest'
                            }
                        ],
                        'id': 5,
                        'new_players': [
                            {
                                'alias': 'FalleN',
                                'first_name': 'Gabriel',
                                'last_name': 'Toledo',
                                'id': 9,
                                'hltv_external_id': 2023,
                                'hltv_url_path': '/stats/players/2023/FalleN'
                            },
                            {
                                'alias': 'f0rest',
                                'first_name': 'Patrik',
                                'last_name': 'Lindberg',
                                'id': 10,
                                'hltv_external_id': 29,
                                'hltv_url_path': '/stats/players/29/f0rest'
                            }
                        ]
                    },
                    {
                        'map_name': None,
                        'order': 1,
                        'players': [
                            {
                                'team_details': {
                                    'name': 'SK', 'hltv_url_path': '/team/6137/sk', 'hltv_external_id': 6137
                                },
                                'first_name': 'Gabriel',
                                'alias': 'FalleN',
                                'last_name': 'Toledo',
                                'id': 11,
                                'hltv_external_id': 2023,
                                'hltv_url_path': '/stats/players/2023/FalleN'
                            },
                            {
                                'team_details': {
                                    'name': 'NiP', 'hltv_url_path': '/team/4411/nip', 'hltv_external_id': 4411
                                },
                                'first_name': 'Patrik',
                                'alias': 'f0rest',
                                'last_name': 'Lindberg',
                                'id': 12,
                                'hltv_external_id': 29,
                                'hltv_url_path': '/stats/players/29/f0rest'
                            }
                        ],
                        'id': 6,
                        'new_players': []
                    }
                ],
                'id': 4,
                'changes': {
                    'winner': {'old': '<CsgoTeam: NiP>', 'new': 'None'},
                    'length': {'old': 2, 'new': 3},
                    'teams': {'old': ['<CsgoTeam: NiP>'], 'new': ['<CsgoTeam: SK>', '<CsgoTeam: NiP>']}
                }
            }
        )
