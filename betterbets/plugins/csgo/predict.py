import glob
import logging
import pickle
from pathlib import Path

import numpy as np
import tensorflow as tf

logger = logging.getLogger(__name__)


MODELS_PATH = Path('/') / 'models' / 'csgo'


class PredictException(Exception):
    pass


def get_team_form(player_ids, start_time, client, max_seq_len=50):
    """Get historical game data for a team at a given date."""
    team_history = []
    feat_dimension = 9

    # ensure that we only using 5 players at most
    for player in player_ids[:5]:
        ply_form = client.get_player_game_history(
            player, start_time, num_games=max_seq_len
        ).values

        # make conformable arrays:
        form_length = len(ply_form)
        if form_length < max_seq_len:
            # add a numpy array (N, D)
            zero_arr = np.zeros((max_seq_len - form_length, feat_dimension))
            if form_length > 0:
                ply_form = np.concatenate((zero_arr, ply_form))
            else:
                ply_form = zero_arr

        team_history.append(ply_form)

    # check on length of arrays
    if len(team_history) == 5:
        # good to go
        return np.array(team_history),
    elif len(player_ids) == 0:
        # just a zero array of each player
        return np.zeros((5, max_seq_len, feat_dimension)),
    # append np zeros to the array
    missing_players = 5 - len(player_ids)
    for missing in range(missing_players):
        team_history.append(np.zeros((max_seq_len, feat_dimension)))
    return np.array(team_history),


def hot_vector(inp_vec, length):
    """Convert 1D numpy array to k-hot array."""
    out_vec = np.zeros(length + 1)
    try:
        for i in inp_vec:
            # one-hot this
            out_vec[i] = 1
        return out_vec
    except IndexError:
        return out_vec


def load_model_details(model_paths=None, cache_path=None):
    """Load pre-trained CSGO models.

    :param model_paths: List of hdf5 model paths
    :type model_paths: list
    :param cache_path: Path for the pickle model file
    :type cache_path: str
    :return: Model and its details
    :rtype: dict
    """
    tf.reset_default_graph()
    tf.keras.backend.clear_session()

    if model_paths is None:
        model_paths = [
            file_name for file_name in glob.glob('{}/*.h5'.format(MODELS_PATH))
        ]
    if cache_path is None:
        cache_path = MODELS_PATH / 'meta.pickle'

    loaded_models = []
    for model_path in model_paths:
        loaded_models.append(tf.keras.models.load_model(model_path, custom_objects={"split_tensor": split_tensor}))

    with open(cache_path, 'rb') as f:
        cache = pickle.load(f)
    return {
        'loaded_models': loaded_models,
        'max_player': cache['max_player'],
        'max_map': cache['max_map'],
        'input_data': cache['inp_data']
    }


def split_tensor(tensor, idx):
    """Splits a tensorflow tensor at a given index."""
    return tensor[:, :, :, idx]
