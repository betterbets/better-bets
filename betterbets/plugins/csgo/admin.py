from django.contrib import admin

from . import models


@admin.register(models.CsgoGameTeam)
class CsgoGameTeamAdmin(admin.ModelAdmin):
    list_display = ('series', 'team', 'csgo_map', 'tournament')
    list_filter = ('game__map__name',)
    raw_id_fields = ('game',)
    search_fields = ('team__name', 'game__map__name')

    def tournament(self, obj):
        return obj.game.series.tournament

    def series(self, obj):
        return '{} BO{}'.format(
            obj.game.series.title,
            obj.game.series.length,
        )

    def csgo_map(self, obj):
        return obj.game.map

    tournament.short_description = 'Tournament'
    series.short_description = 'Series'
    csgo_map.short_description = 'Map'


@admin.register(models.CsgoGameOdds)
class CsgoGameOddsAdmin(admin.ModelAdmin):
    list_display = ('game_team', 'amount', 'betting_site')
    list_filter = ('betting_site',)
    list_select_related = ('game_team', 'betting_site')
    raw_id_fields = ('game_team', 'arbitrage')
    search_fields = ('game_team__team', 'amount')


class CsgoGamePlayerTeamInline(admin.TabularInline):
    model = models.CsgoGamePlayerTeam
    extra = 2
    max_num = 10
    ordering = ('team__name', 'player__alias')
    raw_id_fields = ('player', 'team')


class CsgoGameTeamInline(admin.TabularInline):
    model = models.CsgoGameTeam
    extra = 2
    max_num = 2
    ordering = ('team__name',)


@admin.register(models.CsgoGame)
class CsgoGameAdmin(admin.ModelAdmin):
    inlines = (CsgoGameTeamInline, CsgoGamePlayerTeamInline)
    list_display = ('series', 'start_time', 'winner', 'map')
    list_filter = ('series__start_time',)
    list_per_page = 25
    raw_id_fields = ('series', 'winner')


class CsgoPlayerAliasInline(admin.TabularInline):
    model = models.CsgoPlayerAlias
    extra = 1
    ordering = ('start_date',)


class CsgoPlayerAlternateAliasInline(admin.TabularInline):
    model = models.CsgoAlternatePlayerAlias
    extra = 1
    ordering = ('alias',)


@admin.register(models.CsgoPlayer)
class CsgoPlayerAdmin(admin.ModelAdmin):
    list_display = ('alias', 'first_name', 'last_name')
    inlines = (
        CsgoPlayerAliasInline,
        CsgoPlayerAlternateAliasInline
    )
    search_fields = ('alias', 'first_name', 'last_name')


class CsgoRoundPlayerInline(admin.TabularInline):
    model = models.CsgoRoundPlayer
    extra = 5
    ordering = ('player',)


class CsgoRoundTeamInline(admin.TabularInline):
    model = models.CsgoRoundTeam
    extra = 2
    ordering = ('team',)


@admin.register(models.CsgoRound)
class CsgoRoundAdmin(admin.ModelAdmin):
    inlines = (CsgoRoundTeamInline, CsgoRoundPlayerInline)
    extra = 2


@admin.register(models.CsgoSeriesTeam)
class CsgoSeriesTeamAdmin(admin.ModelAdmin):
    list_display = ('series', 'team', 'tournament')

    def tournament(self, obj):
        return obj.series.tournament

    tournament.short_description = 'Tournament'


@admin.register(models.CsgoSeriesOdds)
class CsgoSeriesOdds(admin.ModelAdmin):
    pass


class CsgoSeriesGameInline(admin.TabularInline):
    model = models.CsgoGame
    extra = 1
    raw_id_fields = ('winner',)


class CsgoSeriesTeamInline(admin.TabularInline):
    model = models.CsgoSeriesTeam
    extra = 2
    max_num = 2
    ordering = ('team',)
    raw_id_fields = ('team',)


@admin.register(models.CsgoSeries)
class CsgoSeriesAdmin(admin.ModelAdmin):
    list_display = ('title', 'tournament', 'start_time', 'best_of')
    inlines = (CsgoSeriesTeamInline, CsgoSeriesGameInline)
    search_fields = ('tournament__name', 'teams__name')
    raw_id_fields = ('tournament',)

    def best_of(self, obj):
        return 'BO{}'.format(obj.length)

    best_of.short_description = 'Best Of'


class CsgoTeamNameInline(admin.TabularInline):
    model = models.CsgoTeamName
    extra = 1


class CsgoTeamAlternateNameInline(admin.TabularInline):
    model = models.CsgoAlternateTeamName
    extra = 1
    ordering = ('name',)


@admin.register(models.CsgoTeam)
class CsgoTeamAdmin(admin.ModelAdmin):
    inlines = (CsgoTeamNameInline, CsgoTeamAlternateNameInline)
    search_fields = ('name',)


@admin.register(models.CsgoTournament)
class CsgoTournamentAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'start_date', 'end_date',
        'offline', 'prize_money', 'prize_money_currency'
    )
    list_filter = ('start_date',)
    filter_horizontal = ('teams',)
    ordering = ('-start_date', 'name')
    prepopulated_fields = {'slug': ('name',), }
    search_fields = ('name', 'teams__name')


@admin.register(models.CsgoMap)
class CsgoMapAdmin(admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(models.CsgoWinMethod)
class CsgoWinMethodAdmin(admin.ModelAdmin):
    pass
