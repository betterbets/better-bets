import logging
from datetime import timedelta

from dateutil.relativedelta import relativedelta
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.http import Http404
from django.utils import timezone
from django.views.generic import DetailView

from main.views import SeriesListView
from plugins.csgo.models import CsgoSeries

WEEKDAYS = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
]


logger = logging.getLogger(__name__)


class CsgoSeriesListView(LoginRequiredMixin, SeriesListView):
    """List the series for a particular CsgoTournament."""
    model = CsgoSeries
    paginate_by = 10
    _errors = {'invalid_date_filter': 'Invalid date filter'}

    def generate_filters(self):
        """Create filters for the template."""
        date_filter = self.request.GET.get('date')
        today = timezone.now().date()
        tomorrow = today + timedelta(days=1)

        filters = []
        for days_ahead in range(0, 5):
            day = today + timedelta(days=days_ahead)
            if day == today:
                display_name = 'Today'
            elif day == tomorrow:
                display_name = 'Tomorrow'
            else:
                display_name = WEEKDAYS[day.weekday()]
            day_string = day.strftime('%Y-%m-%d')
            filters.append(
                {
                    'display_name': display_name,
                    'value': day_string,
                    'is_active': date_filter == day_string
                }
            )
        return filters

    def get_queryset(self):
        """Filter Series based on tournaments and dates."""
        prefetch_related = [
            'csgoseriesteam_set',
            'csgoseriesteam_set__team',
            'tournament',
            'teams'
        ]
        date_filter = self.request.GET.get('date')
        if date_filter is not None:
            try:
                self.validate_date_filter()
                return self.model.objects.filter(
                    start_time__date=date_filter
                ).prefetch_related(*prefetch_related)
            except ValidationError:
                raise Http404('Invalid date filter')
        return self.model.objects.filter(
            start_time__gte=timezone.now()
        ).prefetch_related(*prefetch_related)

    def get_context_data(self, **kwargs):
        """Add filters to context."""
        context = super().get_context_data(**kwargs)
        context['filter_active'] = self.request.GET.get('date', False)
        context.update({'date_filters': self.generate_filters()})
        return context


class CsgoSeriesDetailView(LoginRequiredMixin, DetailView):
    """Show games being played along with their predictions."""

    model = CsgoSeries

    def get_context_data(self, **kwargs):
        """Add head to head team details."""
        context = super().get_context_data(**kwargs)
        context.update(self.object.head_to_head_details)
        return context


class CsgoSeriesResultListView(SeriesListView):
    model = CsgoSeries
    paginate_by = 50
    template_name = 'csgo/performance.html'

    def generate_filters(self):
        """Create filters for the template."""
        date_filter = self.request.GET.get('date')
        today = timezone.now().date()
        filters = []
        for weeks in range(4, 13, 2):
            day = today - relativedelta(days=weeks * 7)
            day_string = day.strftime('%Y-%m-%d')
            filters.append({
                'display_name': '{}w'.format(weeks),
                'value': day_string,
                'is_active': date_filter == day_string
            })
        return filters

    def get_queryset(self):
        date_filter = self.request.GET.get('date')
        try:
            if date_filter is not None:
                self.validate_date_filter()
            else:
                date_filter = timezone.now().date() - timedelta(days=14)
            series = self.model.objects.filter(
                start_time__lte=timezone.now().date(),
                start_time__gte=date_filter
            ).exclude(
                winner=None
            ).order_by('-start_time').prefetch_related(
                'csgoseriesteam_set',
                'csgoseriesteam_set__team',
                'winner',
                'teams'
            )
            filtered_series = [
                s for s in series
                if s.model_winner is not None
                and s.winner.name is not None
            ]
            self.queryset = filtered_series
            return filtered_series
        except ValidationError:
            raise Http404('Invalid date filter')

    def get_context_data(self, **kwargs):
        """Add filters to context."""
        context = super().get_context_data(**kwargs)

        series = self.queryset
        num_correct_series = len([s for s in series if s.winner.name == s.model_winner['name']])
        correct_percent = (num_correct_series / len(series)) * 100
        context['num_series'] = len(series)
        context['num_correct_series'] = num_correct_series
        context['correct_percent'] = round(correct_percent, 1)

        context['filter_active'] = self.request.GET.get('date', False)
        context.update({'date_filters': self.generate_filters()})
        return context
