import logging
from datetime import timedelta

import tensorflow as tf
from django.utils import timezone

from betterbets.celery import app

from .models import CsgoPlayer, CsgoSeries, CsgoTeam, CsgoTournament
from .predict import PredictException, load_model_details
from .scrape import (
    scrape_players, scrape_series, scrape_teams, scrape_tournaments
)

logger = logging.getLogger(__name__)


@app.task
def update_series_predictions(series_ids):
    """Update predictions for series.

    :param: series_ids: Series that have recently been scraped
    :type series_ids: list
    """
    logger.info('Generating Predictions: Started')
    predictions_calculated = 0
    num_series = len(series_ids)
    model_details = load_model_details()
    for series in CsgoSeries.objects.filter(id__in=series_ids):
        try:
            series.predict(model_details=model_details)
            predictions_calculated += 1
            logger.info(
                'Generating Predictions: %s/%s',
                predictions_calculated,
                num_series
            )
        except PredictException as e:
            logger.info('Predict Error: %s', e)
            num_series -= 1
    tf.keras.backend.clear_session()
    logger.info(
        'Generating Predictions: %s predictions generated',
        predictions_calculated
    )


@app.task
def update_teams():
    """Update all CSGO teams."""
    details = scrape_teams()
    CsgoTeam.process(details=details)


@app.task
def update_players():
    """Update all players with 100 or more maps played."""
    details = scrape_players()
    CsgoPlayer.process(details=details)


@app.task
def update_tournaments():
    """Update all upcoming tournaments."""
    details = scrape_tournaments()
    CsgoTournament.process(details=details)


@app.task
def update_series(hltv_url_paths=None):
    """Update all upcoming series.

    :param hltv_url_paths: Url paths of series to update
    :type hltv_url_paths: list or QuerySet
    """
    details = scrape_series(hltv_url_paths=hltv_url_paths)
    # The IDs of the series that were updated or created
    series_ids = CsgoSeries.process(details=details)
    update_series_predictions.delay(series_ids)


@app.task
def update_series_starting_soon():
    """Update series that are starting in the next 6 hours.

    In order to keep match/game details up to date, series need to be
    updated regularly. This task simply finds the series/games that are
    being played today and runs an update_series for those hltv_url_paths.
    """
    series_hltv_url_paths = CsgoSeries.objects.filter(
        start_time__gte=timezone.now(),
        start_time__lte=timezone.now() + timedelta(hours=6)
    ).values_list('hltv_url_path', flat=True)
    update_series(hltv_url_paths=series_hltv_url_paths)


@app.task
def update_past_series(start_date=None, end_date=None):
    """Update past series.

    As series are finished, their match details are updated on HLTV, we want
    to capture the results of the match to display model performance stats.
    """
    logger.info('Updating Past Series: Started')
    today = timezone.now().date()
    if start_date is None:
        start_date = today - timedelta(days=7)
    if end_date is None:
        end_date = today
    past_series = CsgoSeries.objects.filter(
        winner=None,
        start_time__lte=end_date,
        start_time__gte=start_date
    )
    for series in past_series:
        details = series.scrape()
        if details:
            # If a parsing exception is raised, no details are returned
            series.update_or_create(details)
    logger.info('Updating Past Series: Finished')
