# Provision Development Machine

In order to provision your local machine with Docker an Docker compose, run the following playbook while in the
`playbooks` directory

Install Docker and Docker Compose. This can be done manually or, if you are using Ubuntu 16.04, an Ansible playbook
has been provided.

First, ensure the latest version of Ansible is installed. You can do this by using the following commands on
Ubuntu:

```bash
sudo apt-get update
```

```bash
sudo apt-get install software-properties-common
```

```bash
sudo apt-add-repository ppa:ansible/ansible
```

```bash
sudo apt-get update
```

```bash
sudo apt-get install ansible
```

While in the root of this repository, you can use the following command:

```bash
sudo ansible-playbook playbook/docker.yml
```

__Note:__ This playbook will add you to the Docker group. You will need to log out and back in again in order for
these changes to take affect, otherwise you will need to run Docker commands with `sudo`.

While in the root of this repository, you can use the following command:

```bash
sudo ansible-playbook docker.yml
```

__Note:__ This playbook will add you to the Docker group. You will need to log out and back in again in order for
these changes to take affect.
