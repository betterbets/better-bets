#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

until curl -f http://app:8080/
do
  echo "Waiting for app..."
  sleep 5
done

# Must be in the project directory
cd /home/betterbets/betterbets && celery -n csgo_series@%%h -Q csgo_series --app=betterbets --no-color worker --concurrency=1 --pidfile=
