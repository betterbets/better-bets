#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

until curl -f http://app:8080/
do
  echo "Waiting for app..."
  sleep 5
done

# Must be in the project directory
cd /home/betterbets/betterbets && celery -A betterbets beat -l INFO --pidfile="/tmp/celery-beat.pid"
