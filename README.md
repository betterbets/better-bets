# Better Bets
Better Bets Django Application.

Please refer to the [Wiki](https://gitlab.com/betterbets/better-bets/wikis/home) for documentation


## Prerequisites

Development is done via Docker and Docker Compose. Refer to the
[Wiki](https://gitlab.com/betterbets/better-bets/wikis/Development/getting-started`) for more detail.

Ensure you have downloaded and installed [Docker](https://www.docker.com/community-edition#/download)
and [Docker Compose](https://docs.docker.com/compose/install/).

Optionally, if you are on a Ubuntu system, you can use the Ansible playbook provided in the
[playbooks](https://gitlab.com/betterbets/better-bets/tree/master/playbooks) directory.


## Development Environment:

1.  Create an environment file using the `env.example` template called `.env` in the root of this repository.

    This contains all the environment variables and secrets to run the app. By convention, the `.env` file gets read by
    Docker Compose.
   
    The environment file can be found in LastPass

2.  Run Docker Compose in the root directory of this repository.

    ```bash
    docker-compose up
    ```
    
    This will start the app, database and cache containers. By default this will run the Django app using
    `manage.py runserver`.

    When `docker-compose up` is run for the first time a few things will happen:
     - Latest production database will be downloaded and restored
     - CSGO Model files will be downloaded

3.  You can now visit the app via `localhost:8080`


## Production-like

To use the "production-like" configuration run the next command. This will run the Django app using uwsgi as well
as all the Celery process. An Nginx container will be used to serve up the static files.

```bash
docker-compose -f docker-compose.yml -f docker-compose.full.yml up
```

The app is accessed with `localhost:8000`


## Jupyter Notebook

To use a Jupyter Notebook in the betterbets Django environment use the following steps

1.  Run Jupyter Notebook docker-compose configuration

    ```bash
    docker-compose -f docker-compose.yml -f docker-compose.jupyter.yml up
    ```

2.  Run the Jupyter Notebook entrypoint

    ```bash
    docker exec -it betterbets bash entrypoint.sh notebook
    ```

3.  Open the link in your browser, setting the domain to `localhost`. For example

    ```bash
    # Original:
    http://5aa6e8a18f3b:8888/?token=cebe97b5fe70b7afb3e1fe6479627f0225c4de754a050c0d&token=cebe97b5fe70b7afb3e1fe6479627f0225c4de754a050c0d

    # Final
    http://localhost:8888/?token=cebe97b5fe70b7afb3e1fe6479627f0225c4de754a050c0d&token=cebe97b5fe70b7afb3e1fe6479627f0225c4de754a050c0d
    ```

4.  When opening a new kernel, select `Django Shell-Plus`


## Cleaning up/Refreshing the environment

### Docker

Clean the Docker resources by stopping Docker Compose and removing any dangling resources. In the root directory of
this repository, run the following command

```bash
docker-compose stop
```

```bash
docker container prune -f && docker volume prune -f && docker network prune -f && docker image prune -f
```

### Database

Removing the `data` directory will delete the current database. Upon using `docker-compose up` again, a new database
will be downloaded. Inside the root directory of this repository, run the following command

```bash
sudo rm -rf data
```

### Model

If the models are deleted, they will be re-downloaded the next time `docker-compose up` is run. To remove the models
run the following command from the root directory of this repository

```bash
sudo rm -rf models
```


## Building the Container

To rebuild the docker image, run the following command in the root directory of this repository

```bash
docker-compose build
```


## Tests:

Linting and unit tests can be run from the root directory of this repository.

 -  Pylint

    ```bash
    docker-compose run --rm app lint
    ```

 -  Unit Tests

    ```bash
    docker-compose run --rm app test
    ```


## Useful Commands

 -  Bash shell
    ```bash
    docker exec -u betterbets -it betterbets bash
    ```

 -  Django shell
    ```bash
    docker exec -u betterbets -it betterbets python /home/betterbets/betterbets/manage.py shell_plus
    ```

 -  Django migrations

    ```bash
    docker exec -u betterbets -it betterbets python /home/betterbets/betterbets/manage.py makemigrations main csgo
    ```

    ```bash
    docker exec -u betterbets -it betterbets python /home/betterbets/betterbets/manage.py migrate
    ```

 -  Postgres shell

    ```bash
    docker exec -it postgres psql -U betterbets betterbets
    ```

 -  Dump Postgres database

    ```bash
    docker exec -t postgres pg_dump -c -U betterbets betterbets > dump_`date +%Y%m%d"_"%H%M%S`.sql
    ```

 -  Restore Postgres database

    ```bash
    cat dump.sql | docker exec -i postgres psql -U betterbets betterbets
    ```
