#!/bin/bash
set -e

# Setting Group Permissions
DOCKER_GROUP_CURRENT_ID=`id -g $DOCKER_GROUP`

if [ $DOCKER_GROUP_CURRENT_ID -eq $HOST_GROUP_ID ]; then
  echo "Group $DOCKER_GROUP is already mapped to $DOCKER_GROUP_CURRENT_ID."
else
  DOCKER_GROUP_OLD=`getent group $HOST_GROUP_ID | cut -d: -f1`

  if [ -z "$DOCKER_GROUP_OLD" ]; then
    echo "Changing the ID of $DOCKER_GROUP group to $HOST_GROUP_ID"
    groupmod -o -g $HOST_GROUP_ID $DOCKER_GROUP || true
  else
    exit 1
  fi
fi

# Setting User Permissions
DOCKER_USER_CURRENT_ID=`id -u $DOCKER_USER`

if [ $DOCKER_USER_CURRENT_ID -eq $HOST_USER_ID ]; then
  echo "User $DOCKER_USER is already mapped to $DOCKER_USER_CURRENT_ID."
else
  DOCKER_USER_OLD=`getent passwd $HOST_USER_ID | cut -d: -f1`

  if [ -z "$DOCKER_USER_OLD" ]; then
    echo "Changing the ID of $DOCKER_USER user to $HOST_USER_ID"
    usermod -o -u $HOST_USER_ID $DOCKER_USER || true
  else
    exit 1
  fi
fi
