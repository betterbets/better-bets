FROM python:3.6

ARG BETTERBETS_HOME=/home/betterbets
ARG REQUIREMENTS_PATH=/requirements.txt

RUN useradd -ms /bin/bash betterbets && mkdir /static
RUN apt-get update \
 && apt-get -y install postgresql-client \
 && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt
COPY ./requirements /requirements
RUN pip install --upgrade pip --no-cache-dir -r $REQUIREMENTS_PATH

COPY permissions.sh /permissions.sh
COPY entrypoint.sh /entrypoint.sh
COPY --chown=betterbets:betterbets ./.coveragerc ./.isort.cfg ./.pylintrc $BETTERBETS_HOME/
COPY --chown=betterbets:betterbets ./configs $BETTERBETS_HOME/configs
COPY --chown=betterbets:betterbets configure.py $BETTERBETS_HOME/configure.py

COPY --chown=betterbets:betterbets betterbets /home/betterbets/betterbets

VOLUME /home/betterbets/betterbets

HEALTHCHECK CMD curl -f http://localhost:8080/ || exit 1
ENTRYPOINT ["bash", "/entrypoint.sh"]
CMD ["dev"]
